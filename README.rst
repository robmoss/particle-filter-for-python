Particle filters for Python
===========================

|version| |joss| |docs| |tests| |coverage| |downloads|

Description
-----------

This package implements several particle filter methods that can be used for recursive Bayesian estimation and forecasting.
See the `online documentation <https://pypfilt.readthedocs.io/>`_ for tutorials, how-to guides, and API documentation.

License
-------

The code is distributed under the terms of the `BSD 3-Clause license <https://opensource.org/licenses/BSD-3-Clause>`_ (see
``LICENSE``), and the documentation is distributed under the terms of the
`Creative Commons BY-SA 4.0 license
<http://creativecommons.org/licenses/by-sa/4.0/>`_.

Installation
------------

To install the latest release::

    pip install pypfilt

To install the latest release with plotting support (requires `matplotlib <http://matplotlib.org/>`_)::

    pip install pypfilt[plot]

To install the latest development version, clone this repository and run::

    pip install .

Citation
--------

If you use ``pypfilt``, please cite our `JOSS article <https://doi.org/10.21105/joss.06276>`_ and an archived release of the software (see ``CITATION.cff``).

.. code-block:: bibtex

   @article{pypfilt,
     author = {Moss, Robert},
     title = {pypfilt: a particle filter for {Python}},
     journal = {Journal of Open Source Software},
     volume = {9},
     issue = {96},
     pages = {6276},
     year = {2024},
     doi = {10.21105/joss.06276},
     note = {Please cite this article and an archived release (see CITATION.cff)},
   }

.. |version| image:: https://badge.fury.io/py/pypfilt.svg
   :alt: Latest version
   :target: https://pypi.org/project/pypfilt/

.. |joss| image:: https://joss.theoj.org/papers/10.21105/joss.06276/status.svg
   :alt: Journal of Open Source Software
   :target: https://doi.org/10.21105/joss.06276

.. |docs| image::  https://readthedocs.org/projects/pypfilt/badge/
   :alt: Documentation
   :target: https://pypfilt.readthedocs.io/

.. |tests| image:: https://gitlab.unimelb.edu.au/rgmoss/particle-filter-for-python/badges/master/pipeline.svg
   :alt: Test cases
   :target: https://gitlab.unimelb.edu.au/rgmoss/particle-filter-for-python

.. |coverage| image:: https://gitlab.unimelb.edu.au/rgmoss/particle-filter-for-python/badges/master/coverage.svg
   :alt: Test coverage
   :target: https://gitlab.unimelb.edu.au/rgmoss/particle-filter-for-python

.. |downloads| image:: https://static.pepy.tech/badge/pypfilt
   :alt: PyPI downloads
   :target: https://pepy.tech/projects/pypfilt
