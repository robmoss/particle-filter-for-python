import logging
import numpy as np
import pypfilt
import pytest
import scipy.stats
from pypfilt.examples.predation import predation_instance


def add_dependent_parameters(instance):
    """
    Define dependent parameters `kappa` and `omega`.
    """
    for name in ['kappa', 'omega']:
        instance.settings['prior'][name] = {'shape': 1, 'dependent': True}

    def dist_fn(indep_values, dep_params):
        """Define kappa = alpha + 1 and omega ~ N(alpha, 1)."""
        logger = logging.getLogger(__name__)
        logger.info('called dist_fn()')
        alpha = indep_values['alpha']
        return {
            'kappa': {
                'ppf': lambda qtls: alpha + 1,
            },
            'omega': {'distribution': scipy.stats.norm(loc=alpha, scale=1)},
        }

    if 'sampler' not in instance.settings:
        instance.settings['sampler'] = {}
    instance.settings['sampler']['dependent_distributions_function'] = dist_fn


def test_lhs_sampler_predation_scalar_with_dependent_params(caplog):
    # NOTE: The caplog fixture captures logging; see the pytest documentation:
    # https://docs.pytest.org/en/stable/logging.html
    caplog.set_level(logging.INFO)

    instance = predation_instance('predation.toml')
    add_dependent_parameters(instance)

    # Build the simulation context and check that we obtained the expected
    # values for the dependent parameters kappa and omega.
    context = instance.build_context()
    assert 'called dist_fn()' in caplog.text
    assert 'kappa' in context.data['prior']
    assert 'omega' in context.data['prior']
    # Check that kappa = alpha + 1.
    assert np.allclose(
        context.data['prior']['kappa'], context.data['prior']['alpha'] + 1
    )
    # Check that alpha and omega have similar mean values.
    mean_alpha = np.mean(context.data['prior']['alpha'])
    mean_omega = np.mean(context.data['prior']['omega'])
    assert abs(mean_alpha - mean_omega) < 0.01

    fs_times = [1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0]
    # Check for warning messages about unused parameters (alpha, omega).
    # NOTE: the records returned by pytest.warn include all warnings, not only
    # those that match the provided pattern.
    warn_msg = 'Unused prior samples'
    with pytest.warns(UserWarning, match=warn_msg) as records:
        results = pypfilt.forecast(context, fs_times, filename=None)
    prior_warnings = [r for r in records if warn_msg in str(r.message)]
    assert len(prior_warnings) == 2

    assert results.estimation is not None
    for t in fs_times:
        assert t in results.forecasts


def test_lhs_sampler_list_arguments():
    instance = predation_instance('predation.toml')
    instance.settings['prior']['epsilon'] = {
        'name': 'norm',
        'args': {
            'loc': [[1, 100]],
            'scale': [[0.1], [10]],
        },
        'shape': [2, 2],
    }

    # Build the simulation context and check that we obtained the expected
    # values for epsilon.
    context = instance.build_context()
    assert 'epsilon' in context.data['prior']
    epsilon = context.data['prior']['epsilon']
    assert epsilon.shape == (1000, 2, 2)
    # Check that the sample means are close to the true means.
    assert abs(1 - np.mean(epsilon[:, 0, 0])) < 0.01
    assert abs(1 - np.mean(epsilon[:, 1, 0])) < 0.01
    assert abs(100 - np.mean(epsilon[:, 0, 1])) < 0.01
    assert abs(100 - np.mean(epsilon[:, 1, 1])) < 0.01
    # Check that the sample standard deviations are close to the truth.
    assert abs(0.1 - np.std(epsilon[:, 0, 0])) < 0.01
    assert abs(0.1 - np.std(epsilon[:, 0, 1])) < 0.01
    assert abs(10 - np.std(epsilon[:, 1, 0])) < 0.1
    assert abs(10 - np.std(epsilon[:, 1, 1])) < 0.1
