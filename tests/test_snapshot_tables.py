import h5py
import numpy as np
import os
import os.path
import pypfilt
from pypfilt.examples.predation import predation_instance


def test_snapshot_tables():
    snapshot_file = 'test_forecast_snapshot.hdf5'
    try:
        os.remove(snapshot_file)
    except FileNotFoundError:
        pass

    instance = predation_instance('predation.toml')
    instance.settings['summary']['tables']['snapshot_all'] = {
        'component': 'pypfilt.summary.ForecastSnapshot',
        'each_summary_time': True,
    }
    instance.settings['summary']['tables']['snapshot_one'] = {
        'component': 'pypfilt.summary.ForecastSnapshot',
        'each_summary_time': False,
    }
    instance.settings['summary']['tables']['snapshot_ens'] = {
        'component': 'pypfilt.summary.EnsembleSnapshot',
    }
    context = instance.build_context()
    fs_times = [5.0]
    pypfilt.forecast(context, fs_times, filename=snapshot_file)

    assert os.path.isfile(snapshot_file)
    with h5py.File(snapshot_file, 'r') as f:
        snapshot_one = f['/tables/snapshot_one'][()]
        snapshot_all = f['/tables/snapshot_all'][()]
        snapshot_ens = f['/tables/snapshot_ens'][()]

    # Check that the tables have expected dimensions, and that the snapshot
    # for the forecast time is the same in both tables.
    particles = context.settings['filter']['particles']
    assert snapshot_one.shape == (particles,)
    assert snapshot_all.shape == (11 * particles,)
    assert np.array_equal(snapshot_one, snapshot_all[:particles])
    # NOTE: the ensemble snapshot table should contain 16 snapshots (0..16)
    # but the snapshot for t = 5 should be recorded in both the estimation and
    # forecasting passes.
    # Also note that we cannot directly compare the ensemble snapshot to the
    # forecast snapshots, because the ForecastSnapshot table resamples the
    # particles.
    assert snapshot_ens.shape == (17 * particles,)

    # Now use the initial snapshot to define the model prior samples.
    instance = predation_instance('predation.toml')
    prior = instance.settings['prior']
    param_names = list(prior.keys())
    for name in param_names:
        prior[name] = {
            'external': True,
            'hdf5': snapshot_file,
            'dataset': '/tables/snapshot_one',
            'column': name,
        }

    # Check that the resulting simulation context contains the expected model
    # prior samples.
    context = instance.build_context()
    for name in param_names:
        assert np.array_equal(snapshot_one[name], context.data['prior'][name])

    # Clean up.
    os.remove(snapshot_file)
