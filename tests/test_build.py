"""
Test the construction and behaviour of simulation contexts.
"""

import copy
import logging
import numpy as np
import pytest

from pypfilt.build import Context, validate_ensemble_partitions


def test_build_get_setting():
    """
    Ensure that ``Context.get_setting()`` behaves as expected.
    """
    ctx = Context(
        scenario_id='test_build',
        descriptor='get_setting',
        source=None,
        settings={'a': 1, 'b': {'c': 2}},
        data={},
        component={},
        event_handler={},
        all_observations=[],
    )
    # Retrieve a top-level parameter.
    # The choice of default value should have no effect.
    assert ctx.get_setting(['a']) == 1
    assert ctx.get_setting(['a'], default=0) == 1
    # Retrieve a nested parameter.
    # The choice of default value should have no effect.
    assert ctx.get_setting(['b', 'c']) == 2
    assert ctx.get_setting(['b', 'c'], default=0) == 2
    # Retrieve a missing parameter.
    # The choice of default value should be evident.
    assert ctx.get_setting(['b', 'd']) is None
    assert ctx.get_setting(['b', 'd'], 'default') == 'default'


def test_build_partitions():
    """
    Ensure that valid and invalid partitions are detected by ``Context``.
    """
    valid_partitions = [
        {
            'weight': 0.2,
            'particles': 200,
        },
        {
            'weight': 0.3,
            'particles': 200,
        },
        {
            'weight': 0.5,
            'particles': 600,
        },
    ]

    settings = {
        'filter': {
            'particles': 1000,
        }
    }

    # Create an array for checking partition slice indexing.
    ensemble = np.zeros(settings['filter']['particles'])

    # Ensure that with no partitions defined, a single partition is returned.
    validate_ensemble_partitions(settings)
    parts = settings['filter']['partition']
    assert len(parts) == 1
    part = parts[0]
    assert part['weight'] == 1
    assert ensemble[part['slice']].shape == ensemble.shape
    assert part['slice'].start == 0 or part['slice'].start is None
    assert part['slice'].stop == 1000
    assert part['slice'].step == 1 or part['slice'].step is None

    # Ensure that the valid partitions (above) are accepted as valid.
    settings['filter']['partition'] = copy.deepcopy(valid_partitions)
    validate_ensemble_partitions(settings)

    # Inspect the ``filter.partitions`` setting.
    parts = settings['filter']['partition']
    # Check that the particle numbers are consistent.
    assert len(parts) == len(valid_partitions)
    for ix, part in enumerate(parts):
        assert part['weight'] == valid_partitions[ix]['weight']
        assert part['particles'] == valid_partitions[ix]['particles']
        assert ensemble[part['slice']].shape == (part['particles'],)

    # Check that the first partition is the first 200 particles.
    assert ensemble[parts[0]['slice']].shape == (200,)
    assert parts[0]['slice'].start == 0
    assert parts[0]['slice'].stop == 200
    assert parts[0]['slice'].step == 1 or parts[0]['slice'].step is None

    # Check that the second partition is the next 200 particles.
    assert ensemble[parts[1]['slice']].shape == (200,)
    assert parts[1]['slice'].start == 200
    assert parts[1]['slice'].stop == 400
    assert parts[1]['slice'].step == 1 or parts[1]['slice'].step is None

    # Check that the third partition is the final 600 particles.
    assert ensemble[parts[2]['slice']].shape == (600,)
    assert parts[2]['slice'].start == 400
    assert parts[2]['slice'].stop == 1000
    assert parts[2]['slice'].step == 1 or parts[2]['slice'].step is None

    # Identify the particles that belong to each partition in a sequence of
    # mask arrays.
    masks = []
    for p in parts:
        mask = np.zeros(ensemble.shape, dtype=np.bool_)
        mask[p['slice']] = True
        masks.append(mask)
    # Check that the partitions do not overlap.
    assert not np.any(np.logical_and.reduce(masks))
    # Check that the partitions cover all particles.
    assert np.all(np.logical_or.reduce(masks))

    # With no partitions, particles and weights sum to zero.
    settings['filter']['partition'] = []
    with pytest.raises(ValueError, match='particles sum to'):
        validate_ensemble_partitions(settings)

    # Remove a 'weight' key.
    settings['filter']['partition'] = copy.deepcopy(valid_partitions)
    del settings['filter']['partition'][0]['weight']
    with pytest.raises(ValueError, match='has no field "weight"'):
        validate_ensemble_partitions(settings)

    # Remove a 'particles' key.
    settings['filter']['partition'] = copy.deepcopy(valid_partitions)
    del settings['filter']['partition'][-1]['particles']
    with pytest.raises(ValueError, match='has no field "particles"'):
        validate_ensemble_partitions(settings)

    # Make weights sum to something other than unity.
    settings['filter']['partition'] = copy.deepcopy(valid_partitions)
    settings['filter']['partition'][-1]['weight'] = 0.25
    with pytest.raises(ValueError, match='weights sum to'):
        validate_ensemble_partitions(settings)

    # Make particles sum to something other than total particles.
    settings['filter']['partition'] = copy.deepcopy(valid_partitions)
    settings['filter']['partition'][0]['particles'] = 100
    with pytest.raises(ValueError, match='particles sum to'):
        validate_ensemble_partitions(settings)


def test_build_partitions_invalid_weight():
    """
    Ensure that invalid weights are detected, even if they sum to unity.
    """
    settings = {
        'filter': {
            'particles': 1000,
            'partition': [
                {
                    'weight': -0.1,
                    'particles': 500,
                },
                {
                    'weight': 1.1,
                    'particles': 500,
                },
            ],
        }
    }

    with pytest.raises(ValueError, match='invalid weight'):
        validate_ensemble_partitions(settings)


def test_build_partitions_invalid_particles():
    """
    Ensure that invalid particles are detected, even if they sum to the total.
    """
    settings = {
        'filter': {
            'particles': 1000,
            'partition': [
                {
                    'weight': 0.1,
                    'particles': -500,
                },
                {
                    'weight': 0.9,
                    'particles': 1500,
                },
            ],
        }
    }

    with pytest.raises(ValueError, match='invalid number of particles'):
        validate_ensemble_partitions(settings)


def test_build_partitions_invalid_reservoir_partition_number():
    """
    Ensure that invalid reservoir partition numbers are detected.
    """
    settings = {
        'filter': {
            'particles': 1000,
            'partition': [
                {
                    'weight': 0.0,
                    'particles': 200,
                    'reservoir': True,
                },
                {
                    'weight': 1.0,
                    'particles': 800,
                    'reservoir_fraction': 0.1,
                },
            ],
        }
    }

    for reservoir in [-2, -1, 0, 2, 3]:
        settings['filter']['partition'][1]['reservoir_partition'] = reservoir
        with pytest.raises(ValueError, match='invalid reservoir #'):
            validate_ensemble_partitions(settings)

    settings['filter']['partition'][0]['reservoir'] = False
    settings['filter']['partition'][1]['reservoir_partition'] = 1
    with pytest.raises(ValueError, match='invalid reservoir #'):
        validate_ensemble_partitions(settings)


def test_build_partitions_invalid_reservoir_fraction():
    """
    Ensure that invalid reservoir fractions are detected.
    """
    settings = {
        'filter': {
            'particles': 1000,
            'partition': [
                {
                    'weight': 0.0,
                    'particles': 200,
                    'reservoir': True,
                },
                {
                    'weight': 1.0,
                    'particles': 800,
                    'reservoir_partition': 1,
                },
            ],
        }
    }

    for frac in [-0.1, 0.0, 1.0, 1.1]:
        settings['filter']['partition'][1]['reservoir_fraction'] = frac
        with pytest.raises(ValueError, match='invalid reservoir fraction'):
            validate_ensemble_partitions(settings)


def test_build_reservoir_valid_unused_1(caplog):
    """
    Ensure that ``Context`` accepts valid reservoir partitions.
    """
    caplog.set_level(logging.WARNING)
    valid_partitions = [
        {
            'weight': 0.0,
            'particles': 100,
            'reservoir': True,
        },
        {
            'weight': 0.1,
            'particles': 100,
            'reservoir': True,
        },
        {
            'weight': 0.9,
            'particles': 800,
        },
    ]

    settings = {
        'filter': {
            'particles': 1000,
            'partition': valid_partitions,
        }
    }

    # Ensure that the reservoir partitions are accepted.
    validate_ensemble_partitions(settings)
    parts = settings['filter']['partition']
    assert len(parts) == 3
    assert parts[0]['reservoir']
    assert parts[1]['reservoir']
    assert not parts[2]['reservoir']

    # Ensure there was a warning about an unused zero-weight partition.
    assert 'Unused reservoir #1' in caplog.text


def test_build_reservoir_valid_unused_2(caplog):
    """
    Ensure that ``Context`` accepts valid reservoir partitions.
    """
    caplog.set_level(logging.WARNING)
    valid_partitions = [
        {
            'weight': 0.0,
            'particles': 100,
            'reservoir': True,
        },
        {
            'weight': 0.1,
            'particles': 100,
            'reservoir': True,
        },
        {
            'weight': 0.9,
            'particles': 800,
            'reservoir_partition': 1,
            'reservoir_fraction': 0.0005,
        },
    ]

    settings = {
        'filter': {
            'particles': 1000,
            'partition': valid_partitions,
        }
    }

    # Ensure that the reservoir partitions are accepted.
    validate_ensemble_partitions(settings)
    parts = settings['filter']['partition']
    assert len(parts) == 3
    assert parts[0]['reservoir']
    assert parts[1]['reservoir']
    assert not parts[2]['reservoir']

    # Ensure there was a warning about sampling no particles from a reservoir.
    msg = 'Partition #3 samples no particles from its reservoir'
    assert msg in caplog.text

    # Ensure there was a warning about an unused zero-weight partition.
    msg = 'Unused reservoir #1'
    assert msg in caplog.text


def test_build_reservoir_valid_used(caplog):
    """
    Ensure that ``Context`` accepts valid reservoir partitions.
    """
    caplog.set_level(logging.WARNING)
    valid_partitions = [
        {
            'weight': 0.0,
            'particles': 100,
            'reservoir': True,
        },
        {
            'weight': 0.1,
            'particles': 100,
            'reservoir': True,
        },
        {
            'weight': 0.9,
            'particles': 800,
            'reservoir_partition': 1,
            'reservoir_fraction': 0.1,
        },
    ]

    settings = {
        'filter': {
            'particles': 1000,
            'partition': valid_partitions,
        }
    }

    # Ensure that the reservoir partitions are accepted.
    validate_ensemble_partitions(settings)
    parts = settings['filter']['partition']
    assert len(parts) == 3
    assert parts[0]['reservoir']
    assert parts[1]['reservoir']
    assert not parts[2]['reservoir']
    assert parts[2]['reservoir_ix'] == 0

    # Ensure there was no warning about an unused zero-weight partition.
    assert 'Unused reservoir #1' not in caplog.text
