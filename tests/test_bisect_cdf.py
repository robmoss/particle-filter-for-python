"""Test cases for the CDF bisection method."""

import numpy as np
import pypfilt
import pypfilt.state
import pytest
import scipy.stats


class FixedNormal(pypfilt.obs.Univariate):
    def distribution(self, ctx, snapshot):
        return scipy.stats.norm(loc=1, scale=1.5)


def create_dummy_snapshot(weights):
    """
    Create a history matrix snapshot for testing the CDF bisection method.

    :param weights: An array of particle weights, which will be used to define
        the ensemble size.
    """
    history_dtype = [
        ('weight', np.float64),
        ('prev_ix', np.float64),
        ('resampled', np.bool_),
        ('state_vec', [('x', np.float64), ('y', np.float64)]),
    ]
    history_matrix = np.zeros(shape=(1, len(weights)), dtype=history_dtype)
    history_matrix['weight'] = weights
    return pypfilt.state.Snapshot(
        time=1.0,
        steps_per_unit=1,
        matrix=history_matrix,
        hist_ix=0,
    )


def test_bisect_cdf_zero_weights():
    """
    When given a subset of the ensemble which has zero probability mass, the
    original implementation entered an infinite loop. This should now be
    caught and an error should be raised.
    """
    obs_model = FixedNormal(obs_unit='arbitrary', settings={})
    ctx = None
    weights = np.zeros(100)
    snapshot = create_dummy_snapshot(weights)
    probs = [0.025, 0.05, 0.25, 0.50, 0.75, 0.95, 0.975]
    with pytest.raises(ValueError, match='Net weight is not positive'):
        obs_model.quantiles(ctx, snapshot, probs)


def test_bisect_cdf_negative_weights():
    """
    Ensure that an error is raised when one or more particles have negative
    weight.
    """
    obs_model = FixedNormal(obs_unit='arbitrary', settings={})
    ctx = None
    weights = np.zeros(100)
    weights[42] = -1
    snapshot = create_dummy_snapshot(weights)
    probs = [0.025, 0.05, 0.25, 0.50, 0.75, 0.95, 0.975]
    with pytest.raises(ValueError, match='Weights contain negative values'):
        obs_model.quantiles(ctx, snapshot, probs)


def test_bisect_cdf_nan_weights():
    """
    Ensure that an error is raised when one or more particles have non-finite
    weight.
    """
    obs_model = FixedNormal(obs_unit='arbitrary', settings={})
    ctx = None
    weights = np.zeros(100)
    weights[42] = np.nan
    snapshot = create_dummy_snapshot(weights)
    probs = [0.025, 0.05, 0.25, 0.50, 0.75, 0.95, 0.975]
    with pytest.raises(ValueError, match='Weights contain non-finite values'):
        obs_model.quantiles(ctx, snapshot, probs)


def test_bisect_cdf_inf_weights():
    """
    Ensure that an error is raised when one or more particles have non-finite
    weight.
    """
    obs_model = FixedNormal(obs_unit='arbitrary', settings={})
    ctx = None
    weights = np.zeros(100)
    weights[42] = np.inf
    snapshot = create_dummy_snapshot(weights)
    probs = [0.025, 0.05, 0.25, 0.50, 0.75, 0.95, 0.975]
    with pytest.raises(ValueError, match='Weights contain non-finite values'):
        obs_model.quantiles(ctx, snapshot, probs)
