"""Test the construction of backcast history matrices."""

import numpy as np
import pypfilt


def minimal_context(particles):
    """
    Construct a minimal simulation context for testing backcast construction.

    :param particles: The number of particles.
    """

    class MinimalModel:
        def field_types(self, ctx):
            return [('x', np.float64), ('y', np.float64)]

    return pypfilt.build.Context(
        scenario_id='test_history_backcast',
        descriptor='',
        source=None,
        settings={
            'filter': {
                'particles': particles,
            }
        },
        data={},
        component={
            'model': MinimalModel(),
        },
        event_handler={},
        all_observations=[],
    )


def test_history_backcast_simple():
    """
    Construct a small history matrix and check that the backcast has the
    expected structure and content.

    The history matrix has the following structure:

        Time:      0    1    2    3

        Particles: 0    1    0    0
                   1    2    1    1
                   2    4    2    2
                   3    4    3    1
                   4    0    4    3

        Weights:   0.2  0.2  var  0.2

    There are resampling events at times t=1 and t=3, and particle weights are
    non-uniform at time t=2.

    The final surviving particles are: [1, 2, 4, 2, 4].

    The particle state vectors [x, y] record the current time in x(t) and the
    original particle index in y(t).
    """
    num_px = 5
    num_steps = 3
    ctx = minimal_context(num_px)

    # Construct an empty history matrix.
    svec_shape = (num_steps + 1, num_px)
    svec_dtype = ctx.component['model'].field_types(ctx)
    matrix = np.zeros(
        svec_shape,
        dtype=[
            ('weight', np.float64),
            ('prev_ix', np.int64),
            ('resampled', np.bool_),
            ('state_vec', svec_dtype),
        ],
    )
    matrix['weight'][:] = 0.2
    matrix['prev_ix'][:] = np.arange(num_px)

    # Set x(t) = t and y(t) = initial particle index.
    for t in range(num_steps + 1):
        matrix['state_vec']['x'][t] = t
        matrix['state_vec']['y'][t] = np.arange(num_px)

    # Apply resampling at time t = 1.
    ixs_t1 = np.array([1, 2, 4, 4, 0])
    matrix[1:] = matrix[1:, ixs_t1]
    matrix[1]['resampled'] = True
    matrix[2:]['prev_ix'] = np.arange(num_px)

    # Apply non-uniform weights at time t = 2.
    matrix[2]['weight'] = np.array([0.2, 0.5, 0.1, 0.1, 0.1])

    # Apply resampling at time t = 3.
    ixs_t3 = np.array([0, 1, 2, 1, 3])
    matrix[3]['resampled'] = True
    matrix[3:] = matrix[3:, ixs_t3]
    # NOTE: no future 'prev_ix' columns to update.

    # Construct the simulation history and update the index so that it is
    # consistent with having performed ``num_steps`` time-steps.
    history = pypfilt.state.History(
        ctx, matrix, offset=0, times=list(range(num_steps + 1))
    )
    history.index = num_steps

    # Construct the backcast and inspect its contents.
    backcast = history.create_backcast(ctx)
    bmat = backcast.matrix

    # Check that the final particle weights are preserved.
    assert np.allclose(bmat['weight'], matrix['weight'][-1])
    # Check that all parent indices are 0:N-1 (i.e., resampling events have
    # been accounted for).
    np.array_equiv(bmat['prev_ix'], np.arange(num_px))
    assert not np.any(bmat['resampled'])

    # Check that x(t) = t.
    for t in range(num_steps + 1):
        assert np.all(bmat['state_vec']['x'][t] == t)

    # Check that y(t) = original particle index (0:N-1).
    expected_order = np.array([1, 2, 4, 2, 4])
    np.array_equiv(bmat['state_vec']['y'], expected_order)

    # Check that the time-step index is preserved.
    assert backcast.index == num_steps
