"""Test the backcast summary components."""

import numpy as np
import pypfilt
from pypfilt.examples.predation import predation_instance


def test_backcast_predictive_cis():
    """
    Test the BackcastPredictiveCIs summary table.
    """

    # Add a BackcastPredictiveCIs table to the predation scenario.
    instance = predation_instance('predation.toml')
    instance.settings['summary']['monitors'] = {
        'backcast_monitor': {'component': 'pypfilt.summary.BackcastMonitor'}
    }
    instance.settings['summary']['tables']['backcasts'] = {
        'component': 'pypfilt.summary.BackcastPredictiveCIs',
        'backcast_monitor': 'backcast_monitor',
    }

    # Adjust the time-step size and summary statistic frequency.
    steps_per_unit = 6
    summaries_per_unit = 3
    instance.settings['time']['steps_per_unit'] = steps_per_unit
    instance.settings['time']['summaries_per_unit'] = summaries_per_unit

    # Run a single forecasting simulation.
    fs_time = 9.0
    context = instance.build_context()
    results = pypfilt.forecast(context, [fs_time], filename=None)

    # Inspect the backcast.
    backcast = results.forecasts[fs_time].tables['backcasts']
    expected_cints = [0, 50, 60, 70, 80, 90, 95]
    num_expected_times = 1 + int(fs_time) * summaries_per_unit
    expected_times = np.linspace(0, fs_time, num_expected_times)

    for obs_unit in ['x', 'y']:
        obs_mask = backcast['unit'] == obs_unit
        obs_rows = np.sum(obs_mask)
        assert obs_rows == len(expected_cints) * num_expected_times
        assert np.allclose(expected_times, np.unique(backcast['time']))

        for time in np.unique(backcast['time']):
            time_mask = obs_mask & (backcast['time'] == time)
            time_rows = np.sum(time_mask)
            assert time_rows == len(expected_cints)

            for cint in expected_cints:
                cint_mask = time_mask & (backcast['prob'] == cint)
                cint_rows = np.sum(cint_mask)
                assert cint_rows == 1
