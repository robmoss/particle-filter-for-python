"""Test the pypfilt.cache module."""

import datetime
import numpy as np
import os
import pypfilt
import pypfilt.cache
import pypfilt.examples
import pypfilt.examples.predation
import pytest
import warnings


from test_predation import simulate_from_model, predation_instance


def test_cache_scalar():
    toml_file = 'predation.toml'
    cache_file = 'test_cache_scalar.hdf5'
    t0 = 0.0
    t1 = 15.0

    try:
        os.remove(cache_file)
    except FileNotFoundError:
        pass

    # Simulate observations from a single particle.
    obs_tables = simulate_from_model(toml_file)

    # Extract the observations from the summary table.
    x_tbl = obs_tables['x']
    y_tbl = obs_tables['y']

    # Change one of the observations.
    change_time = t0 + 4
    y_new = y_tbl.copy()
    orig_value = y_new['value'][y_new['time'] == change_time]
    y_new['value'][y_new['time'] == change_time] = orig_value + 0.1

    # Run several forecasts to populate the cache, and then check that the
    # cache retrieves the correct state for various contexts.
    # The forecasts are, in chronological order:
    #
    # A: with original obs table and lookup table
    # B: change obs with time between A and B
    # C: original obs and lookup table
    # D: original obs and different lookup table

    # Forecast A: with original observation and lookup tables
    instance = predation_instance(toml_file)
    instance.settings['files']['delete_cache_file_before_forecast'] = False
    instance.settings['files']['delete_cache_file_after_forecast'] = False
    context = instance.build_context(obs_tables={'x': x_tbl, 'y': y_tbl})
    context.data['lookup']['test'] = x_tbl.copy()
    context.settings['files']['cache_file'] = cache_file
    fs_time_A = t0 + 3
    pypfilt.forecast(context, [fs_time_A], filename=None)
    assert os.path.isfile(cache_file)

    # Forecast B: change obs with time between A and B
    fs_time_B = t0 + 5
    context = instance.build_context(obs_tables={'x': x_tbl, 'y': y_new})
    context.data['lookup']['test'] = x_tbl.copy()
    context.settings['files']['cache_file'] = cache_file
    pypfilt.forecast(context, [fs_time_B], filename=None)

    # Forecast C: original obs and lookup table
    fs_time_C = t0 + 7
    context = instance.build_context(obs_tables={'x': x_tbl, 'y': y_tbl})
    context.data['lookup']['test'] = x_tbl.copy()
    context.settings['files']['cache_file'] = cache_file
    pypfilt.forecast(context, [fs_time_C], filename=None)

    # Forecast D: original obs and different lookup table
    fs_time_D = t0 + 9
    context.data['lookup']['test'] = y_tbl.copy()
    pypfilt.forecast(context, [fs_time_D], filename=None)

    # Create a context object that is consistent with forecasts A and C.
    instance = predation_instance(toml_file)
    instance.settings['files']['delete_cache_file_before_forecast'] = False
    instance.settings['files']['delete_cache_file_after_forecast'] = False
    ctx_AC = instance.build_context(obs_tables={'x': x_tbl, 'y': y_tbl})
    ctx_AC.data['lookup']['test'] = x_tbl.copy()
    ctx_AC.settings['files']['cache_file'] = cache_file
    ctx_AC.settings['time'] = {'start': t0, 'until': t1}

    # Create a context object that is consistent with forecast B.
    ctx_B = instance.build_context(obs_tables={'x': x_tbl, 'y': y_new})
    ctx_B.data['lookup']['test'] = x_tbl.copy()
    ctx_B.settings['files']['cache_file'] = cache_file
    ctx_B.settings['time'] = {'start': t0, 'until': t1}

    # Create a context object that is consistent with forecast D.
    ctx_D = instance.build_context(obs_tables={'x': x_tbl, 'y': y_tbl})
    ctx_D.data['lookup']['test'] = y_tbl.copy()
    ctx_D.settings['files']['cache_file'] = cache_file
    ctx_D.settings['time'] = {'start': t0, 'until': t1}

    # Check the cache results when using the same observations and lookup
    # tables as forecasts A and C.
    result = pypfilt.cache.load_state(cache_file, ctx_AC, [fs_time_A])
    assert result is not None
    assert result['start'] == fs_time_A

    result = pypfilt.cache.load_state(cache_file, ctx_AC, [fs_time_B])
    assert result is not None
    assert result['start'] == fs_time_A

    result = pypfilt.cache.load_state(cache_file, ctx_AC, [fs_time_C])
    assert result is not None
    assert result['start'] == fs_time_C

    result = pypfilt.cache.load_state(cache_file, ctx_AC, [fs_time_D])
    assert result is not None
    assert result['start'] == fs_time_C

    result = pypfilt.cache.load_state(cache_file, ctx_AC, [t0])
    assert result is None

    result = pypfilt.cache.load_state(cache_file, ctx_AC, [t1])
    assert result is not None
    assert result['start'] == fs_time_C

    # Check the cache results when using the same observations and lookup
    # tables as forecast B.
    result = pypfilt.cache.load_state(cache_file, ctx_B, [fs_time_A])
    assert result is not None
    assert result['start'] == fs_time_A

    result = pypfilt.cache.load_state(cache_file, ctx_B, [fs_time_B])
    assert result is not None
    assert result['start'] == fs_time_B

    result = pypfilt.cache.load_state(cache_file, ctx_B, [fs_time_C])
    assert result is not None
    assert result['start'] == fs_time_B

    result = pypfilt.cache.load_state(cache_file, ctx_B, [fs_time_D])
    assert result is not None
    assert result['start'] == fs_time_B

    result = pypfilt.cache.load_state(cache_file, ctx_B, [t0])
    assert result is None

    result = pypfilt.cache.load_state(cache_file, ctx_B, [t1])
    assert result is not None
    assert result['start'] == fs_time_B

    # Check the cache results when using the same observations and lookup
    # tables as forecast D.
    result = pypfilt.cache.load_state(cache_file, ctx_D, [fs_time_A])
    assert result is None

    result = pypfilt.cache.load_state(cache_file, ctx_D, [fs_time_B])
    assert result is None

    result = pypfilt.cache.load_state(cache_file, ctx_D, [fs_time_C])
    assert result is None

    result = pypfilt.cache.load_state(cache_file, ctx_D, [fs_time_D])
    assert result is not None
    assert result['start'] == fs_time_D

    result = pypfilt.cache.load_state(cache_file, ctx_D, [t0])
    assert result is None

    result = pypfilt.cache.load_state(cache_file, ctx_D, [t1])
    assert result is not None
    assert result['start'] == fs_time_D

    pypfilt.examples.predation.remove_example_files()
    os.remove(cache_file)


def test_cache_datetime():
    toml_file = 'predation-datetime.toml'
    cache_file = 'test_cache_datetime.hdf5'
    t0 = datetime.datetime(2017, 5, 1)
    t1 = t0 + datetime.timedelta(days=15)

    try:
        os.remove(cache_file)
    except FileNotFoundError:
        pass

    # Simulate observations from a single particle.
    obs_tables = simulate_from_model(toml_file)

    # Extract the observations from the summary table.
    x_tbl = obs_tables['x']
    y_tbl = obs_tables['y']

    # Change one of the observations.
    change_time = t0 + datetime.timedelta(days=4)
    y_new = y_tbl.copy()
    orig_value = y_new['value'][y_new['time'] == change_time]
    y_new['value'][y_new['time'] == change_time] = orig_value + 0.1

    # Run several forecasts to populate the cache, and then check that the
    # cache retrieves the correct state for various contexts.
    # The forecasts are, in chronological order:
    #
    # A: with original obs table and lookup table
    # B: change obs with time between A and B
    # C: original obs and lookup table
    # D: original obs and different lookup table

    # Forecast A: with original observation and lookup tables
    instance = predation_instance(toml_file)
    instance.settings['files']['delete_cache_file_before_forecast'] = False
    instance.settings['files']['delete_cache_file_after_forecast'] = False
    context = instance.build_context(obs_tables={'x': x_tbl, 'y': y_tbl})
    context.data['lookup']['test'] = x_tbl.copy()
    context.settings['files']['cache_file'] = cache_file
    fs_time_A = t0 + datetime.timedelta(days=3)
    pypfilt.forecast(context, [fs_time_A], filename=None)
    assert os.path.isfile(cache_file)

    # Forecast B: change obs with time between A and B
    fs_time_B = t0 + datetime.timedelta(days=5)
    context = instance.build_context(obs_tables={'x': x_tbl, 'y': y_new})
    context.data['lookup']['test'] = x_tbl.copy()
    context.settings['files']['cache_file'] = cache_file
    pypfilt.forecast(context, [fs_time_B], filename=None)

    # Forecast C: original obs and lookup table
    fs_time_C = t0 + datetime.timedelta(days=7)
    context = instance.build_context(obs_tables={'x': x_tbl, 'y': y_tbl})
    context.data['lookup']['test'] = x_tbl.copy()
    context.settings['files']['cache_file'] = cache_file
    pypfilt.forecast(context, [fs_time_C], filename=None)

    # Forecast D: original obs and different lookup table
    fs_time_D = t0 + datetime.timedelta(days=9)
    context.data['lookup']['test'] = y_tbl.copy()
    pypfilt.forecast(context, [fs_time_D], filename=None)

    # Create a context object that is consistent with forecasts A and C.
    instance = predation_instance(toml_file)
    instance.settings['files']['delete_cache_file_before_forecast'] = False
    instance.settings['files']['delete_cache_file_after_forecast'] = False
    ctx_AC = instance.build_context(obs_tables={'x': x_tbl, 'y': y_tbl})
    ctx_AC.data['lookup']['test'] = x_tbl.copy()
    ctx_AC.settings['files']['cache_file'] = cache_file
    ctx_AC.settings['time'] = {'start': t0, 'until': t1}

    # Create a context object that is consistent with forecast B.
    ctx_B = instance.build_context(obs_tables={'x': x_tbl, 'y': y_new})
    ctx_B.data['lookup']['test'] = x_tbl.copy()
    ctx_B.settings['files']['cache_file'] = cache_file
    ctx_B.settings['time'] = {'start': t0, 'until': t1}

    # Create a context object that is consistent with forecast D.
    ctx_D = instance.build_context(obs_tables={'x': x_tbl, 'y': y_tbl})
    ctx_D.data['lookup']['test'] = y_tbl.copy()
    ctx_D.settings['files']['cache_file'] = cache_file
    ctx_D.settings['time'] = {'start': t0, 'until': t1}

    # Check the cache results when using the same observations and lookup
    # tables as forecasts A and C.
    result = pypfilt.cache.load_state(cache_file, ctx_AC, [fs_time_A])
    assert result is not None
    assert result['start'] == fs_time_A

    result = pypfilt.cache.load_state(cache_file, ctx_AC, [fs_time_B])
    assert result is not None
    assert result['start'] == fs_time_A

    result = pypfilt.cache.load_state(cache_file, ctx_AC, [fs_time_C])
    assert result is not None
    assert result['start'] == fs_time_C

    result = pypfilt.cache.load_state(cache_file, ctx_AC, [fs_time_D])
    assert result is not None
    assert result['start'] == fs_time_C

    result = pypfilt.cache.load_state(cache_file, ctx_AC, [t0])
    assert result is None

    result = pypfilt.cache.load_state(cache_file, ctx_AC, [t1])
    assert result is not None
    assert result['start'] == fs_time_C

    # Check the cache results when using the same observations and lookup
    # tables as forecast B.
    result = pypfilt.cache.load_state(cache_file, ctx_B, [fs_time_A])
    assert result is not None
    assert result['start'] == fs_time_A

    result = pypfilt.cache.load_state(cache_file, ctx_B, [fs_time_B])
    assert result is not None
    assert result['start'] == fs_time_B

    result = pypfilt.cache.load_state(cache_file, ctx_B, [fs_time_C])
    assert result is not None
    assert result['start'] == fs_time_B

    result = pypfilt.cache.load_state(cache_file, ctx_B, [fs_time_D])
    assert result is not None
    assert result['start'] == fs_time_B

    result = pypfilt.cache.load_state(cache_file, ctx_B, [t0])
    assert result is None

    result = pypfilt.cache.load_state(cache_file, ctx_B, [t1])
    assert result is not None
    assert result['start'] == fs_time_B

    # Check the cache results when using the same observations and lookup
    # tables as forecast D.
    result = pypfilt.cache.load_state(cache_file, ctx_D, [fs_time_A])
    assert result is None

    result = pypfilt.cache.load_state(cache_file, ctx_D, [fs_time_B])
    assert result is None

    result = pypfilt.cache.load_state(cache_file, ctx_D, [fs_time_C])
    assert result is None

    result = pypfilt.cache.load_state(cache_file, ctx_D, [fs_time_D])
    assert result is not None
    assert result['start'] == fs_time_D

    result = pypfilt.cache.load_state(cache_file, ctx_D, [t0])
    assert result is None

    result = pypfilt.cache.load_state(cache_file, ctx_D, [t1])
    assert result is not None
    assert result['start'] == fs_time_D

    pypfilt.examples.predation.remove_example_files()
    os.remove(cache_file)


def load_cached_state(cache_file, instance, x, y, lookup, fs_time):
    """
    Return the most recent cached state for a new forecast.
    """
    context = instance.build_context(obs_tables={'x': x, 'y': y})
    context.data['lookup']['test'] = lookup.copy()
    context.settings['files']['cache_file'] = cache_file
    return pypfilt.cache.load_state(cache_file, context, [fs_time])


def test_cache_non_finite_values_in_context():
    """
    Test that non-finite values result in warnings, when trying to compare
    cached states to a simulation context.
    """

    toml_file = 'predation.toml'
    cache_file = 'test_cache_non_finite_values_in_context.hdf5'
    t0 = 0.0

    try:
        os.remove(cache_file)
    except FileNotFoundError:
        pass

    # Simulate observations from a single particle.
    obs_tables = simulate_from_model(toml_file)

    # Extract the observations from the summary table.
    x_tbl = obs_tables['x']
    y_tbl = obs_tables['y']

    # Change one of the observations.
    change_time = t0 + 4
    y_nonfin = y_tbl.copy()
    y_nonfin['value'][y_nonfin['time'] == change_time] = np.nan

    # Run a forecast to record a cached state without any non-finite values.
    instance = predation_instance(toml_file)
    instance.settings['files']['delete_cache_file_before_forecast'] = False
    instance.settings['files']['delete_cache_file_after_forecast'] = False
    context = instance.build_context(obs_tables={'x': x_tbl, 'y': y_tbl})
    context.data['lookup']['test'] = x_tbl.copy()
    context.settings['files']['cache_file'] = cache_file
    fs_cache = t0 + 3
    pypfilt.forecast(context, [fs_cache], filename=None)
    assert os.path.isfile(cache_file)

    with warnings.catch_warnings():
        warnings.simplefilter('error')
        fs_time = t0 + 5
        update = load_cached_state(
            cache_file, instance, x_tbl, y_tbl, x_tbl, fs_time
        )
        assert update is not None
        assert update['start'] == fs_cache

    # Detect warnings about non-finite observations, but a valid cached state
    # should still be returned.
    warn_msg = f'/{fs_cache}/data/obs/y: non-finite values in context'
    with pytest.warns(UserWarning, match=warn_msg):
        fs_time = t0 + 5
        update = load_cached_state(
            cache_file, instance, x_tbl, y_nonfin, x_tbl, fs_time
        )
        assert update is not None
        assert update['start'] == fs_cache

    # Detect warnings about non-finite lookup tables, no cached state should
    # be returned.
    warn_msg = f'/{fs_cache}/data/lookup/test: non-finite values in context'
    with pytest.warns(UserWarning, match=warn_msg):
        fs_time = t0 + 5
        update = load_cached_state(
            cache_file, instance, x_tbl, y_tbl, y_nonfin, fs_time
        )
        assert update is None

    pypfilt.examples.predation.remove_example_files()
    os.remove(cache_file)


def test_cache_non_finite_values_in_cached_future_obs():
    """
    Test that non-finite values result in warnings, when trying to compare
    cached states to a simulation context.
    """

    toml_file = 'predation.toml'
    cache_file = 'test_cache_non_finite_values_in_cached_future_obs.hdf5'
    t0 = 0.0

    try:
        os.remove(cache_file)
    except FileNotFoundError:
        pass

    # Simulate observations from a single particle.
    obs_tables = simulate_from_model(toml_file)

    # Extract the observations from the summary table.
    x_tbl = obs_tables['x']
    y_tbl = obs_tables['y']

    # Change one of the *future* observations.
    change_time = t0 + 4
    y_nonfin = y_tbl.copy()
    y_nonfin['value'][y_nonfin['time'] == change_time] = np.nan

    # Run a forecast to record a cached state with a non-finite observation.
    instance = predation_instance(toml_file)
    instance.settings['files']['delete_cache_file_before_forecast'] = False
    instance.settings['files']['delete_cache_file_after_forecast'] = False
    context = instance.build_context(obs_tables={'x': x_tbl, 'y': y_nonfin})
    context.data['lookup']['test'] = x_tbl.copy()
    context.settings['files']['cache_file'] = cache_file
    fs_cache = t0 + 3
    pypfilt.forecast(context, [fs_cache], filename=None)
    assert os.path.isfile(cache_file)

    # Detect warnings about non-finite observations, but a valid cached state
    # should still be returned.
    warn_msg = f'/{fs_cache}/data/obs/y: non-finite values in cache'
    with pytest.warns(UserWarning, match=warn_msg):
        fs_time = t0 + 5
        update = load_cached_state(
            cache_file, instance, x_tbl, y_tbl, x_tbl, fs_time
        )
        assert update is not None
        assert update['start'] == fs_cache

    pypfilt.examples.predation.remove_example_files()
    os.remove(cache_file)


def test_cache_non_finite_values_in_cached_past_obs():
    """
    Test that non-finite values result in warnings, when trying to compare
    cached states to a simulation context.
    """

    toml_file = 'predation.toml'
    cache_file = 'test_cache_non_finite_values_in_cached_past_obs.hdf5'
    t0 = 0.0

    try:
        os.remove(cache_file)
    except FileNotFoundError:
        pass

    # Simulate observations from a single particle.
    obs_tables = simulate_from_model(toml_file)

    # Extract the observations from the summary table.
    x_tbl = obs_tables['x']
    y_tbl = obs_tables['y']

    # Change one of the *past* observations.
    change_time = t0 + 2
    y_nonfin = y_tbl.copy()
    y_nonfin['value'][y_nonfin['time'] == change_time] = np.nan

    # Run a forecast to record a cached state with a non-finite observation,
    # but the particle filter should instead fail and raise an exception.
    instance = predation_instance(toml_file)
    instance.settings['files']['delete_cache_file_before_forecast'] = False
    instance.settings['files']['delete_cache_file_after_forecast'] = False
    context = instance.build_context(obs_tables={'x': x_tbl, 'y': y_nonfin})
    context.data['lookup']['test'] = x_tbl.copy()
    context.settings['files']['cache_file'] = cache_file
    fs_time_cached = t0 + 3
    with pytest.raises(ValueError, match='NaN weights'):
        pypfilt.forecast(context, [fs_time_cached], filename=None)

    # The cache file should not have been created.
    assert not os.path.isfile(cache_file)

    pypfilt.examples.predation.remove_example_files()


def test_cache_non_finite_values_in_cached_lookup():
    """
    Test that non-finite values result in warnings, when trying to compare
    cached states to a simulation context.
    """

    toml_file = 'predation.toml'
    cache_file = 'test_cache_non_finite_values_in_cached_lookup.hdf5'
    t0 = 0.0

    try:
        os.remove(cache_file)
    except FileNotFoundError:
        pass

    # Simulate observations from a single particle.
    obs_tables = simulate_from_model(toml_file)

    # Extract the observations from the summary table.
    x_tbl = obs_tables['x']
    y_tbl = obs_tables['y']

    # Change one of the lookup values.
    change_time = t0 + 4
    y_nonfin = y_tbl.copy()
    y_nonfin['value'][y_nonfin['time'] == change_time] = np.nan

    # Run a forecast to record a cached state without any non-finite values.
    instance = predation_instance(toml_file)
    instance.settings['files']['delete_cache_file_before_forecast'] = False
    instance.settings['files']['delete_cache_file_after_forecast'] = False
    context = instance.build_context(obs_tables={'x': x_tbl, 'y': y_tbl})
    context.data['lookup']['test'] = y_nonfin.copy()
    context.settings['files']['cache_file'] = cache_file
    fs_cache = t0 + 3
    pypfilt.forecast(context, [fs_cache], filename=None)
    assert os.path.isfile(cache_file)

    # Detect warnings about non-finite lookup values, no cached state should
    # be returned.
    warn_msg = f'/{fs_cache}/data/lookup/test: non-finite values in cache'
    with pytest.warns(UserWarning, match=warn_msg):
        fs_time = t0 + 5
        update = load_cached_state(
            cache_file, instance, x_tbl, y_tbl, x_tbl, fs_time
        )
        assert update is None

    pypfilt.examples.predation.remove_example_files()
    os.remove(cache_file)
