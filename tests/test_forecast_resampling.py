"""
Ensure that setting "filter.resample.before_forecasting" to ``True`` and to
 ``False`` produces the intended behaviours.
"""

import logging
import pypfilt
import pypfilt.examples.simple

from test_cached_prng_states import simulate_from_model


def test_forecast_resampling_enabled(caplog):
    """Ensure resampling occurs before forecasting."""
    caplog.set_level(logging.DEBUG)
    run_forecast(resample=True, fs_time=5.0)
    message = 'Resampling before forecast'
    records = [r for r in caplog.records if message in r.message]
    assert len(records) == 1


def test_forecast_resampling_enabled_forecast_at_start(caplog):
    """
    Ensure resampling does not occur before forecasting, when the forecast
    begins at the start of the simulation period.
    """
    caplog.set_level(logging.DEBUG)
    run_forecast(resample=True, fs_time=0.0)
    message = 'Resampling before forecast'
    records = [r for r in caplog.records if message in r.message]
    assert len(records) == 0


def test_forecast_resampling_disabled(caplog):
    """Ensure resampling does not occur before forecasting."""
    caplog.set_level(logging.DEBUG)
    run_forecast(resample=False, fs_time=5.0)
    message = 'Resampling before forecast'
    records = [r for r in caplog.records if message in r.message]
    assert len(records) == 0


def run_forecast(resample, fs_time):
    time_scale = pypfilt.Scalar()
    start = 0.0
    until = 10.0
    obs_tables = simulate_from_model(time_scale, start, fs_time)

    inst = pypfilt.examples.simple.gaussian_walk_instance()

    inst.settings['time']['start'] = start
    inst.settings['time']['until'] = until
    inst.settings['filter']['resample']['before_forecasting'] = resample
    inst.settings['files']['delete_cache_file_before_forecast'] = True
    inst.settings['files']['delete_cache_file_after_forecast'] = True

    context = inst.build_context(obs_tables=obs_tables)
    return pypfilt.forecast(context, [fs_time], filename=None)
