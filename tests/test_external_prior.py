import h5py
import io
import numpy as np
import os
import pypfilt


def test_external_prior():
    toml_io = io.StringIO(config_string())

    instances = list(pypfilt.load_instances(toml_io))
    assert len(instances) == 1
    instance = instances[0]

    # Write sampled values for y to an external file.
    y_samples_file = 'y-samples.ssv'
    with open(y_samples_file, 'w') as f:
        f.write('x y\n')
        f.write('1 2\n')
        f.write('3 4\n')
        f.write('5 6\n')
        f.write('7 8\n')
        f.write('9 10\n')

    z_samples_file = 'z-samples.hdf5'
    z_samples_dataset = 'values/z'
    z_samples = np.array(
        [
            (10.0, 12.0, 43.0),
            (20.0, 22.0, 53.0),
            (30.0, 32.0, 63.0),
            (40.0, 42.0, 73.0),
            (50.0, 52.0, 83.0),
        ],
        dtype=[('a', np.float64), ('b', np.float64), ('z', np.float64)],
    )
    with h5py.File(z_samples_file, 'w') as f:
        f.create_dataset(
            z_samples_dataset, data=z_samples, dtype=z_samples.dtype
        )
    context = instance.build_context()

    # Ensure we have prior samples for x, y, and z.
    assert len(context.data['prior']) == 3
    assert 'x' in context.data['prior']
    assert 'y' in context.data['prior']
    assert 'x' in context.data['prior']

    # Check that the x samples reflect the specified distribution.
    (x_min, x_max) = (10, 20)
    assert not np.array_equal(
        context.data['prior']['x'], np.array([1, 3, 4, 7, 9])
    )
    assert np.all(context.data['prior']['x'] >= x_min)
    assert np.all(context.data['prior']['x'] <= x_max)

    # Check that the y samples reflect the external samples file contents.
    assert np.array_equal(
        context.data['prior']['y'], np.array([2, 4, 6, 8, 10])
    )

    # Check that the z samples reflect the external samples file contents.
    assert np.array_equal(
        context.data['prior']['z'], np.array([43, 53, 63, 73, 83])
    )

    # Clean up.
    os.remove(y_samples_file)
    os.remove(z_samples_file)


def config_string():
    return """
    [components]
    model = "pypfilt.examples.simple.GaussianWalk"
    time = "pypfilt.Scalar"
    sampler = "pypfilt.sampler.LatinHypercube"
    summary = "pypfilt.summary.HDF5"

    [time]
    start = 0.0
    until = 30.0
    steps_per_unit = 1

    [prior]
    x = { name = "uniform", args.loc = 10.0, args.scale = 10.0 }
    y = { external = true, table = "y-samples.ssv", column = "y" }
    z.external = true
    z.hdf5 = "z-samples.hdf5"
    z.dataset = "values/z"
    z.column = "z"

    [observations.x]
    model = "pypfilt.examples.simple.GaussianObs"
    observation_period = 0
    # file = "simple-x-obs.ssv"
    parameters.sdev = 0.2

    [summary.tables]
    model_cints.model = "pypfilt.summary.ModelCIs"
    model_cints.credible_intervals = [ 0, 50, 95 ]
    sim_obs.model = "pypfilt.summary.SimulatedObs"
    sim_obs.observation_unit = "x"

    [filter]
    particles = 5
    prng_seed = 42
    history_window = -1
    resample.threshold = 0.25
    regularisation.enabled = true
    filter.regularisation.bounds.x = { min = 0, max = 20 }

    [files]
    input_directory = "."
    output_directory = "."
    delete_cache_file_before_forecast = true
    delete_cache_file_after_forecast = true

    [scenario.example]
    name = "Example Scenario"
    """
