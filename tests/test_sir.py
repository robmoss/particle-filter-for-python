"""Test cases for the models in ``pypfilt.examples.sir``."""

import io
import logging
import numpy as np
import os
import pypfilt
from pypfilt.scenario import override_dict


def apply_sir_ground_truth(instance):
    """
    Fix the SIR model parameters at known values.
    """
    R0 = 2.5
    gamma = 1.0
    instance.settings['prior'] = {
        'R0': {
            'name': 'constant',
            'args': {'value': R0},
        },
        'gamma': {
            'name': 'constant',
            'args': {'value': gamma},
        },
    }


def test_sir_simulate(caplog):
    """
    Simulate observations from each SIR model for the same ground truth.
    """
    caplog.set_level(logging.DEBUG)
    toml_data = pypfilt.examples.sir.sir_toml_data()
    toml_file = 'sir.toml'
    with open(toml_file, 'w') as f:
        f.write(toml_data)
    instances = list(pypfilt.load_instances(toml_file))
    assert len(instances) == 5

    tables = {}
    for instance in instances:
        scen_id = instance.scenario_id
        apply_sir_ground_truth(instance)
        tables[scen_id] = pypfilt.simulate_from_model(instance, particles=1)

    # Retrieve the simulated case counts for each scenario.
    time_series = {k: v['cases']['value'] for (k, v) in tables.items()}

    # Check that the two ODE models yielded identical observations.
    assert np.array_equal(time_series['ODE_Euler'], time_series['ODE_RK'])

    for _scen_id, cases in time_series.items():
        # Check that every scenario yielded at least 50 cases.
        assert cases.sum() >= 50
        # Check that every scenario yielded cases on at least 5 days.
        assert sum(cases > 0) >= 5
        # Check that every scenario yielded no cases on at least 10 days.
        assert sum(cases == 0) >= 10
        # Check that all cases occurred in the first 10 days.
        assert all(cases[10:] == 0)

    os.remove(toml_file)


def test_sir_ctmc(caplog):
    """
    Run an estimation pass and simulate observations for the continuous-time
    Markov chain SIR model.
    """
    run_scenario('CTMC')


def test_sir_dtmc(caplog):
    """
    Run an estimation pass and simulate observations for the discrete-time
    Markov chain SIR model.
    """
    run_scenario('DTMC')


def test_sir_ode(caplog):
    """
    Run an estimation pass and simulate observations for the two ordinary
    differential equation SIR models.
    """
    results_1 = run_scenario('ODE_Euler')
    hist_1 = results_1.estimation.history.matrix
    # NOTE: there are 100 time-steps per unit time, extract the particle state
    # vectors at each time unit.
    svec_1 = hist_1['state_vec'][::100].view((float, 5))

    results_2 = run_scenario('ODE_RK')
    hist_2 = results_2.estimation.history.matrix
    svec_2 = hist_2['state_vec'].view((float, 5))

    assert svec_1.shape == svec_2.shape

    # Check that the SirOdeEuler and SirOdeRK model states are similar.
    diff = svec_1 - svec_2
    assert np.abs(diff).max() < 1
    assert np.abs(diff).mean() < 0.1


def test_sir_sde(caplog):
    """
    Run an estimation pass and simulate observations for the stochastic
    differential equation SIR model.
    """
    run_scenario('SDE')


def run_scenario(scenario_id):
    """
    Run an SIR model scenario and perform some basic checks.
    """
    toml_io = io.StringIO(pypfilt.examples.sir.sir_toml_data())
    instances = [
        inst
        for inst in pypfilt.load_instances(toml_io)
        if inst.scenario_id == scenario_id
    ]
    assert len(instances) == 1
    instance = instances[0]

    # Ensure that pypfilt returns the particle history matrices.
    override_dict(
        instance.settings, {'filter': {'results': {'save_history': True}}}
    )

    context = instance.build_context()
    results = pypfilt.fit(context, filename=None)

    # Sum the simulated observations.
    sim_obs = results.estimation.tables['sim_obs']
    net_obs = sim_obs['value'].sum()

    # Sum the infections.
    history = results.estimation.history.matrix
    net_infs = np.sum(
        history[-1]['state_vec']['I'] + history[-1]['state_vec']['R']
    )

    # Check that there were a reasonable number of infections and cases.
    assert net_infs > 250
    assert net_obs > 200

    # The observed fraction of infections should be *roughly* similar to the
    # observation probability.
    obs_frac = net_obs / net_infs
    obs_prob = context.settings['observations']['cases']['parameters']['p']
    assert abs(obs_prob - obs_frac) < 0.04

    model_name = instance.settings['components']['model'].split('.')[-1]
    logger = logging.getLogger(__name__)
    logger.debug(
        '{}: observed {:0.2f}% of {:.0f} infections'.format(
            model_name, 100 * obs_frac, net_infs
        )
    )

    return results
