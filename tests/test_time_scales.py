"""
Test the behaviour of the pypfilt time scales.
"""

import datetime
import pytest
from pypfilt.time import Datetime, Scalar, step_subset_modulos


def test_datetime_steps():
    """
    Ensure that Datetime.step_of() returns the expected values.
    """
    scale = Datetime()
    start = datetime.datetime(2022, 1, 1)
    end = start + datetime.timedelta(days=3)
    steps_per_unit_values = [1, 5, 10, 12, 20, 100]
    for steps_per_unit in steps_per_unit_values:
        scale.set_period(start, end, steps_per_unit)
        prev_time = start
        for step_number, time_step in scale.steps():
            assert time_step.end > prev_time
            prev_time = time_step.end
            assert scale.step_of(time_step.end) == step_number


def test_datetime_steps_wht_dates():
    """
    Ensure that Datetime.step_of() returns the expected values when the
    simulation period is defined by dates.
    """
    scale = Datetime()
    start = datetime.date(2022, 1, 1)
    end = start + datetime.timedelta(days=3)
    steps_per_unit_values = [1, 5, 10, 12, 20, 100]
    for steps_per_unit in steps_per_unit_values:
        scale.set_period(start, end, steps_per_unit)
        prev_time = scale.parse(start)
        assert isinstance(prev_time, datetime.datetime)
        for step_number, time_step in scale.steps():
            assert time_step.end > prev_time
            prev_time = time_step.end
            assert scale.step_of(time_step.end) == step_number


def test_scalar_steps():
    """
    Ensure that Scalar.step_of() returns the expected values.
    """
    scale = Scalar()
    start = 10.0
    end = start + 3.0
    steps_per_unit_values = [1, 5, 10, 12, 20, 100]
    for steps_per_unit in steps_per_unit_values:
        scale.set_period(start, end, steps_per_unit)
        prev_time = start
        for step_number, time_step in scale.steps():
            assert time_step.end > prev_time
            prev_time = time_step.end
            assert scale.step_of(time_step.end) == step_number


def test_step_subset_modulos():
    """
    Ensure that pypfilt.time.step_subset_modulus() returns expected values.
    """
    steps_per_unit = 12

    assert set(step_subset_modulos(steps_per_unit, 1)) == {0}
    assert set(step_subset_modulos(steps_per_unit, 2)) == {0, 6}
    assert set(step_subset_modulos(steps_per_unit, 3)) == {0, 4, 8}
    assert set(step_subset_modulos(steps_per_unit, 4)) == {0, 3, 6, 9}
    assert set(step_subset_modulos(steps_per_unit, 6)) == {0, 2, 4, 6, 8, 10}
    assert set(step_subset_modulos(steps_per_unit, 12)) == set(range(12))

    for samples in [-1, 0, 5, 7, 8, 9, 10, 11, 13, 14]:
        with pytest.raises(ValueError, match='Cannot take .* samples'):
            step_subset_modulos(steps_per_unit, samples)
