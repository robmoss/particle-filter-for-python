"""Test cases for the models in ``pypfilt.examples.lorenz``."""

from contextlib import contextmanager
import matplotlib
import numpy as np
import os
from pathlib import Path
import pypfilt
import pypfilt.crps
import pypfilt.examples.lorenz


@contextmanager
def temp_wd(path):
    """A context manager for temporarily changing the working directory."""
    orig_wd = Path.cwd()
    try:
        os.chdir(path)
        yield
    finally:
        os.chdir(orig_wd)


def plot_lorenz63_forecast(results, obs_tables, plot_file):
    """
    Plot credible intervals for x(t), y(t), and z(t) against observations.
    """
    backcast_time = 10
    forecast_time = 20

    # Collect credible intervals for the recent backcast and the forecast.
    fit = results.estimation.tables['forecasts']
    forecast = results.forecasts[forecast_time].tables['forecasts']
    credible_intervals = np.concatenate(
        (fit[fit['time'] >= backcast_time], forecast)
    )

    # Collect the simulated observations that were used to fit the model.
    backcast_obs = {
        unit: table[
            np.logical_and(
                table['time'] >= backcast_time,
                table['time'] <= forecast_time,
            )
        ]
        for (unit, table) in obs_tables.items()
    }

    # Collect the simulated observations over the forecast horizon.
    future_obs = {
        unit: table[table['time'] > forecast_time]
        for (unit, table) in obs_tables.items()
    }

    # Plot the backcast and forecast against the simulated observations.
    matplotlib.use('Agg')
    with pypfilt.plot.apply_style():
        plot = pypfilt.plot.Wrap(
            credible_intervals,
            'Time',
            '',
            ('unit', lambda s: '{}(t)'.format(s)),
            nc=1,
        )

        plot.expand_x_lims('time')
        plot.expand_y_lims('ymax')
        plot.fig.subplots_adjust(hspace=0.5)

        obs_size = 25.0

        for ax, df in plot.subplots():
            # Plot each forecast credible interval.
            hs = pypfilt.plot.cred_ints(ax, df, 'time', 'prob')

            # Plot the observations up to the forecasting time.
            hs.extend(
                pypfilt.plot.observations(
                    ax,
                    backcast_obs[df['unit'][0]],
                    label='Past observations',
                    s=obs_size,
                )
            )

            # Plot the observations after the forecasting time.
            hs.extend(
                pypfilt.plot.observations(
                    ax,
                    future_obs[df['unit'][0]],
                    future=True,
                    label='Future observations',
                    s=obs_size,
                )
            )

            plot.add_to_legend(hs)

            # Add a vertical line to indicate the forecast time.
            ax.axvline(
                x=forecast_time, linestyle='--', color='#7f7f7f', zorder=0
            )

            # Adjust the axis limits and the number of ticks.
            x_range = df['time'].max() - df['time'].min()
            x_expand = x_range * 0.01
            ax.set_xlim(
                left=df['time'].min() - x_expand,
                right=df['time'].max() + x_expand,
            )
            ax.locator_params(axis='x', nbins=6)
            ax.set_ylim(auto=True)
            ax.locator_params(axis='y', nbins=6)

        plot.legend(loc='upper center', ncol=4, borderaxespad=0)

        # NOTE: do not save the matplotlib version in the image metadata.
        # This ensures the images are reproducible across matplotlib versions.
        plot.save(
            plot_file,
            format='png',
            width=10,
            height=5,
            metadata={'Software': None},
        )


def check_lorenz63_instances():
    """Ensure that the example Lorenz-63 file defines a single scenario."""
    scenario_file = 'lorenz63_simulate.toml'
    instances = list(pypfilt.load_instances(scenario_file))
    assert len(instances) == 1


def simulate_lorenz63_observations():
    scenario_file = 'lorenz63_simulate.toml'
    instances = list(pypfilt.load_instances(scenario_file))
    instance = instances[0]

    # Simulate observations for x(t), y(t), and z(t).
    obs_tables = pypfilt.simulate_from_model(instance)

    # Save the observations to plain-text files.
    for obs_unit, obs_table in obs_tables.items():
        out_file = f'lorenz63-{obs_unit}.ssv'
        pypfilt.io.write_table(out_file, obs_table)

    return obs_tables


def run_lorenz63_forecast(filename=None):
    scenario_file = 'lorenz63_forecast.toml'
    instances = list(pypfilt.load_instances(scenario_file))
    instance = instances[0]

    # Run a forecast from t = 20.
    forecast_time = 20
    context = instance.build_context()
    return pypfilt.forecast(context, [forecast_time], filename=filename)


def run_lorenz63_forecast_regularised(filename=None):
    scenario_file = 'lorenz63_forecast_regularised.toml'
    instances = list(pypfilt.load_instances(scenario_file))
    instance = instances[0]

    # Run a forecast from t = 20.
    forecast_time = 20
    context = instance.build_context()
    return pypfilt.forecast(context, [forecast_time], filename=filename)


def run_all_lorenz63_scenarios():
    """Run all of the Lorenz-63 scenarios."""
    scenario_file = 'lorenz63_all.toml'

    # Collect the scenario instances in a dictionary.
    instances = {
        instance.scenario_id: instance
        for instance in pypfilt.load_instances(scenario_file)
    }
    assert len(instances) == 3

    # Simulate observations from the 'simulate' scenario.
    obs_tables = pypfilt.simulate_from_model(instances['simulate'])

    # Run forecasts from t = 20.
    forecast_time = 20
    forecast_results = pypfilt.forecast(
        instances['forecast'].build_context(), [forecast_time], filename=None
    )
    regularised_results = pypfilt.forecast(
        instances['forecast_regularised'].build_context(),
        [forecast_time],
        filename=None,
    )

    return (obs_tables, forecast_results, regularised_results)


def plot_crps_comparison(crps_fs, crps_reg, png_file):
    """
    Plot the CRPS values for the original and regularised forecasts.
    """
    import matplotlib.pyplot as plt
    import pypfilt.plot

    matplotlib.use('Agg')

    # Calculate the skill score for the regularised forecasts, relative to the
    # original forecasts.
    mean_fs = np.mean(crps_fs['score'])
    mean_reg = np.mean(crps_reg['score'])
    skill = (mean_fs - mean_reg) / mean_fs

    with pypfilt.plot.apply_style():
        fig, ax = plt.subplots(layout='constrained')
        ax.set_xlabel('Time')
        ax.set_ylabel('CRPS (lower is better)')
        ax.plot('time', 'score', data=crps_fs, label='Original forecast')
        ax.plot(
            'time',
            'score',
            data=crps_reg,
            label=f'With regularisation\n(skill score = {skill:0.3f})',
        )
        ax.legend(loc='upper left')
        fig.set_figwidth(6)
        fig.set_figheight(3)
        fig.savefig(
            png_file,
            format='png',
            dpi=300,
            metadata={'Software': None},
        )


def score_lorenz63_forecasts():
    """Calculate CRPS values for the simulated `z(t)` observations."""
    # Load the true observations that occur after the forecasting time.
    columns = [('time', float), ('value', float)]
    z_true = pypfilt.io.read_table('lorenz63-z.ssv', columns)
    z_true = z_true[z_true['time'] > 20]

    # Run the original forecasts.
    fs_file = 'lorenz63_forecast.hdf5'
    fs = run_lorenz63_forecast(filename=fs_file)

    # Run the forecasts with regularisation.
    reg_file = 'lorenz63_regularised.hdf5'
    fs_reg = run_lorenz63_forecast_regularised(filename=reg_file)

    # Extract the simulated z(t) observations for each forecast.
    time = pypfilt.Scalar()
    z_table = '/tables/sim_z'
    z_fs = pypfilt.io.load_summary_table(time, fs_file, z_table)
    z_reg = pypfilt.io.load_summary_table(time, reg_file, z_table)

    # Calculate CRPS values for each forecast.
    crps_fs = pypfilt.crps.simulated_obs_crps(z_true, z_fs)
    crps_reg = pypfilt.crps.simulated_obs_crps(z_true, z_reg)

    # Check that regularisation improved the forecast performance.
    assert np.mean(crps_reg['score']) < np.mean(crps_fs['score'])

    # Compare the CRPS values for each forecast.
    plot_crps_comparison(crps_fs, crps_reg, 'lorenz63_crps_comparison.png')

    return (fs, fs_reg)


def test_lorenz63():
    output_dir = Path('doc').resolve() / 'getting-started'

    with temp_wd(output_dir):
        pypfilt.examples.lorenz.save_lorenz63_scenario_files()

        obs_tables = simulate_lorenz63_observations()
        fs, fs_reg = score_lorenz63_forecasts()
        plot_lorenz63_forecast(fs, obs_tables, 'lorenz63_forecast.png')
        plot_lorenz63_forecast(
            fs_reg, obs_tables, 'lorenz63_forecast_regularised.png'
        )

        # Remove the output HDF5 files.
        for hdf5_file in Path().glob('lorenz63_*.hdf5'):
            hdf5_file.unlink()

        # Check that the all-scenarios file produces identical results.
        (new_obs_tables, new_fs, new_fs_reg) = run_all_lorenz63_scenarios()

        # Check that we obtain identical simulated observations.
        assert set(obs_tables.keys()) == set(new_obs_tables.keys())
        for key, table in obs_tables.items():
            assert np.array_equal(table, new_obs_tables[key])

        # Check that we obtain identical forecast tables.
        forecast_time = 20
        assert np.array_equal(
            fs.forecasts[forecast_time].tables['forecasts'],
            new_fs.forecasts[forecast_time].tables['forecasts'],
        )
        assert np.array_equal(
            fs_reg.forecasts[forecast_time].tables['forecasts'],
            new_fs_reg.forecasts[forecast_time].tables['forecasts'],
        )
