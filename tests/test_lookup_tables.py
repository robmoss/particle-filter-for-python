"""Test that lookup tables are loaded and behave as intended."""

# TODO: define a scenario that uses lookup tables
# and check that the model can lookup the values.

import io
import numpy as np
from pathlib import Path
import pypfilt
import pytest
import scipy.stats


def test_lookup_tables():
    source = io.StringIO(lookup_scenario())
    instances = list(pypfilt.load_instances(source))
    assert len(instances) == 1
    instance = instances[0]

    y_file = Path('y.ssv')
    z_file = Path('z.ssv')

    with open(y_file, 'w') as f:
        f.write('time value1 value2 value3 value4\n')
        f.write('2    1      2      3      4\n')
        f.write('4    1      2      2      5\n')
        f.write('6    1      2      1      6\n')
        f.write('8    1      2      0      7\n')

    with open(z_file, 'w') as f:
        f.write('time value1 value2\n')
        f.write('2    1.000  0.001\n')
        f.write('4    0.100  0.010\n')
        f.write('6    0.010  0.100\n')
        f.write('8    0.001  1.000\n')

    forecast_time = 0
    context = instance.build_context()

    # Inspect the lookup tables.
    assert 'y' in context.component['lookup']
    assert 'z' in context.component['lookup']
    y_tbl = context.component['lookup']['y']
    z_tbl = context.component['lookup']['z']
    assert y_tbl.value_count() == 4
    assert z_tbl.value_count() == 2
    assert y_tbl.start() == 2
    assert y_tbl.end() == 8
    assert z_tbl.start() == 2
    assert z_tbl.end() == 8
    assert set(y_tbl.times()) == {2, 4, 6, 8}
    assert set(z_tbl.times()) == {2, 4, 6, 8}
    assert np.allclose(y_tbl.lookup(0), np.array([1, 2, 3, 4]))
    assert np.allclose(y_tbl.lookup(5), np.array([1, 2, 2, 5]))
    assert np.allclose(y_tbl.lookup(10), np.array([1, 2, 0, 7]))
    assert np.allclose(z_tbl.lookup(0), np.array([1.0, 0.001]))
    assert np.allclose(z_tbl.lookup(5), np.array([0.1, 0.01]))
    assert np.allclose(z_tbl.lookup(10), np.array([0.001, 1.0]))

    results = pypfilt.forecast(context, [forecast_time], filename=None)

    y_file.unlink()
    z_file.unlink()

    # Check that we cannot build a simulation context when the lookup files do
    # not exist.
    with pytest.raises(FileNotFoundError):
        instance.build_context()

    # Make sure the lookup tables were used appropriately.
    history = results.forecasts[0.0].history.matrix
    x_hist = history['state_vec']['x']
    sim_obs = results.forecasts[0.0].tables['sim_obs']

    # Verify that the particles used the correct lookup columns.
    # Particle 1: x(t) = x(t-1) + 1
    assert np.allclose(1, np.diff(x_hist[:, 0]))
    # Particle 1: x(t) = x(t-1) + 2
    assert np.allclose(2, np.diff(x_hist[:, 1]))
    # Particle 3: x(t) = x(t-1) + {3, 2, 1, 0}
    assert np.allclose(
        np.array([3, 3, 3, 3, 2, 2, 1, 1, 0, 0]), np.diff(x_hist[:, 2])
    )
    # Particle 4: x(t) = x(t-1) + {4, 5, 6, 7}
    assert np.allclose(
        np.array([4, 4, 4, 4, 5, 5, 6, 6, 7, 7]), np.diff(x_hist[:, 3])
    )

    # Verify that the observation model used the correct lookup columns.
    x_flat = x_hist.reshape(-1)
    x_sim = sim_obs['value']
    assert x_flat.shape == x_sim.shape
    z_ixs = history['lookup']['z'].reshape(-1)
    assert z_ixs.min() == 0
    assert z_ixs.max() == 1

    # Identify when the observation error should be maximal.
    expect_large_error = np.logical_or(
        (z_ixs == 0) & (sim_obs['time'] < 4),
        (z_ixs == 1) & (sim_obs['time'] >= 8),
    )
    # Identify when the observation error should be minimal.
    expect_small_error = np.logical_or(
        (z_ixs == 0) & (sim_obs['time'] >= 8),
        (z_ixs == 1) & (sim_obs['time'] < 4),
    )

    # Check that we have large and small observation errors.
    obs_error = abs(x_flat - x_sim)
    assert obs_error[expect_large_error].mean() > 0.1
    assert obs_error[expect_small_error].mean() < 0.001


class LookupModel(pypfilt.Model):
    def field_types(self, ctx):
        return [('x', float)]

    def update(self, ctx, time_step, is_fs, prev, curr):
        y = ctx.component['lookup']['y'].lookup(time_step.start)
        assert len(y) == 4
        curr['x'] = prev['x'] + y


class LookupObs(pypfilt.obs.Univariate):
    def distribution(self, ctx, snapshot):
        values = ctx.component['lookup']['z'].lookup(snapshot.time)
        assert len(values) == 2
        ixs = snapshot.vec['lookup']['z']
        scale = values[ixs]
        assert len(scale) == 4
        loc = snapshot.state_vec['x'] - 0.5 * scale
        return scipy.stats.uniform(loc=loc, scale=scale)


def lookup_scenario():
    return """
    [components]
    time = "pypfilt.Scalar"
    sampler = "pypfilt.sampler.LatinHypercube"
    summary = "pypfilt.summary.HDF5"
    model = "test_lookup_tables.LookupModel"

    [time]
    start = 0
    until = 10
    steps_per_unit = 1

    [lookup_tables]
    y = "y.ssv"
    z = { file = "z.ssv", sample_values = true }

    [observations.x]
    model = "test_lookup_tables.LookupObs"

    [summary.tables]
    sim_obs.component = "pypfilt.summary.SimulatedObs"
    sim_obs.observation_unit = "x"

    [prior]
    x = { name = "uniform", args.loc = 0, args.scale = 1 }

    [files]
    input_directory = "."
    output_directory = "."
    temp_directory = "."
    delete_cache_file_before_forecast = true
    delete_cache_file_after_forecast = true

    [filter]
    particles = 4
    prng_seed = 1
    history_window = -1
    resample.threshold = 0.25
    results.save_history = true

    [scenario.example]
    name = "example"
    """
