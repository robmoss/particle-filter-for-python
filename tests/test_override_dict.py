"""Test that `pypfilt.scenario.override_dict()` behaves as expected."""

from pypfilt.scenario import override_dict, DELETE_KEY


def test_override_dict_flat():
    """
    Ensure that overriding values in a flat dictionary works as expected.
    """
    start = {
        'a': 1,
        'b': 2,
        'c': 3,
    }
    overrides = {
        'b': 3,
        'c': 4,
        'd': 5,
    }
    expect = {
        'a': 1,
        'b': 3,
        'c': 4,
        'd': 5,
    }
    override_dict(start, overrides)
    assert start == expect


def test_override_dict_flat_with_nested():
    """
    Ensure that overriding values in a flat dictionary works as expected.
    """
    start = {
        'a': 1,
        'b': 2,
        'c': 3,
    }
    overrides = {
        'b': {
            'x': 10,
            'y': 100,
        },
        'c': 4,
        'd': 5,
    }
    expect = {
        'a': 1,
        'b': {
            'x': 10,
            'y': 100,
        },
        'c': 4,
        'd': 5,
    }
    override_dict(start, overrides)
    assert start == expect


def test_override_dict_nested_with_flat():
    """
    Ensure that overriding values in a nested dictionary works as expected.
    """
    start = {
        'a': 1,
        'b': {
            'x': 10,
            'y': 100,
        },
        'c': 3,
    }
    overrides = {
        'b': 2,
        'c': 4,
        'd': 5,
    }
    expect = {
        'a': 1,
        'b': 2,
        'c': 4,
        'd': 5,
    }
    override_dict(start, overrides)
    assert start == expect


def test_override_dict_nested_with_nested():
    """
    Ensure that overriding values in a nested dictionary works as expected.
    """
    start = {
        'a': 1,
        'b': {
            'x': 10,
            'y': 100,
            'z': {
                'alpha': -1,
                'beta': -2,
            },
        },
        'c': 3,
    }
    overrides = {
        'b': {
            'x': {
                'omega': -10,
            },
            'y': 50,
            'z': {
                'beta': -4,
                'gamma': -8,
            },
        },
        'c': 4,
        'd': 5,
    }
    expect = {
        'a': 1,
        'b': {
            'x': {
                'omega': -10,
            },
            'y': 50,
            'z': {
                'alpha': -1,
                'beta': -4,
                'gamma': -8,
            },
        },
        'c': 4,
        'd': 5,
    }
    override_dict(start, overrides)
    assert start == expect


def test_override_dict_nested_with_deletes():
    """
    Ensure that deleting keys in a nested dictionary works as expected.

    Deleting keys that don't exist should have no effect.
    """
    start = {
        'a': 1,
        'b': {
            'x': 10,
            'y': 100,
            'z': {
                'alpha': -1,
                'beta': -2,
            },
        },
        'c': 3,
    }
    overrides = {
        'b': {
            'x': {
                'omega': -10,
            },
            'y': 50,
            'z': DELETE_KEY,
            # NOTE: this key does not exist.
            'zzz': DELETE_KEY,
        },
        'c': DELETE_KEY,
        'd': 5,
        # NOTE: this key does not exist.
        'e': DELETE_KEY,
    }
    expect = {
        'a': 1,
        'b': {
            'x': {
                'omega': -10,
            },
            'y': 50,
        },
        'd': 5,
    }
    override_dict(start, overrides)
    assert start == expect
