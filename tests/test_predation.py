"""Test the pypfilt.examples.predation module."""

import datetime
import io
import numpy as np
import os
import os.path
import pypfilt
import pypfilt.examples
import pypfilt.examples.predation
from pathlib import Path
from pypfilt.examples.predation import (
    apply_ground_truth_prior,
    predation_instance,
    remove_example_files,
)
import pypfilt.scenario


def run_scalar_forecast(save_history=False):
    """
    Run predation forecasts using the scalar time scale.
    """
    instance = predation_instance('predation.toml')
    if save_history:
        pypfilt.scenario.override_dict(
            instance.settings, {'filter': {'results': {'save_history': True}}}
        )
    context = instance.build_context()
    fs_times = [1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0]
    results = pypfilt.forecast(context, fs_times, filename=None)
    remove_example_files()
    return results


def run_datetime_forecast(save_history=False):
    """
    Run predation forecasts using the datetime time scale.
    """
    instance = predation_instance('predation-datetime.toml')
    if save_history:
        pypfilt.scenario.override_dict(
            instance.settings, {'filter': {'results': {'save_history': True}}}
        )
    context = instance.build_context()
    t0 = datetime.datetime(2017, 5, 1)
    fs_times = [
        t0 + datetime.timedelta(days=t)
        for t in [1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0]
    ]
    results = pypfilt.forecast(context, fs_times, filename=None)
    remove_example_files()
    return results


def test_predation_compare_time_scales():
    """
    Run predation model forecasts using both scalar and datetime time scales.
    The outputs should be identical, except for the time columns.

    This test triggers the summarise bug that was fixed by commit 1c4af76.
    This required modifying how pypfilt.cache.__data_match compares data keys,
    because no lookup tables are defined.
    """
    result_sc = run_scalar_forecast(save_history=True)
    result_dt = run_datetime_forecast(save_history=True)

    # Compare history matrices for the estimation pass and for each forecast.
    sc_times = result_sc.forecast_times()
    dt_times = result_dt.forecast_times()
    assert len(sc_times) == len(dt_times)

    assert np.array_equal(
        result_sc.estimation.history.matrix,
        result_dt.estimation.history.matrix,
    )
    for sc_time, dt_time in zip(sc_times, dt_times):
        assert np.array_equal(
            result_sc.forecasts[sc_time].history.matrix,
            result_dt.forecasts[dt_time].history.matrix,
        )

    # Verify that the observations differ, due to the different time scales.
    assert len(result_sc.obs) == len(result_dt.obs)
    assert not result_sc.obs == result_dt.obs

    # Verify that the observation values are identical.
    for sc_obs, dt_obs in zip(result_sc.obs, result_dt.obs):
        assert sc_obs.keys() == dt_obs.keys()
        for key in sc_obs:
            if key in ['time']:
                continue
            assert sc_obs[key] == dt_obs[key]

    # Compare the summary tables.
    cints_sc = result_sc.estimation.tables['model_cints']
    forecast_sc = result_sc.estimation.tables['forecasts']
    cints_dt = result_dt.estimation.tables['model_cints']
    forecast_dt = result_dt.estimation.tables['forecasts']

    cints_cols = [
        n for n in cints_sc.dtype.names if n not in ['fs_time', 'time']
    ]
    assert np.array_equal(cints_sc[cints_cols], cints_dt[cints_cols])

    forecast_cols = [
        n for n in forecast_sc.dtype.names if n not in ['fs_time', 'time']
    ]
    assert np.array_equal(
        forecast_sc[forecast_cols], forecast_dt[forecast_cols]
    )

    for sc_time, dt_time in zip(sc_times, dt_times):
        assert len(result_sc.forecasts[sc_time].tables) == 2
        assert len(result_dt.forecasts[dt_time].tables) == 2

        cints_sc = result_sc.forecasts[sc_time].tables['model_cints']
        forecast_sc = result_sc.forecasts[sc_time].tables['forecasts']

        cints_dt = result_dt.forecasts[dt_time].tables['model_cints']
        forecast_dt = result_dt.forecasts[dt_time].tables['forecasts']

        cints_cols = [
            n for n in cints_sc.dtype.names if n not in ['fs_time', 'time']
        ]
        assert np.array_equal(cints_sc[cints_cols], cints_dt[cints_cols])

        forecast_cols = [
            n for n in forecast_sc.dtype.names if n not in ['fs_time', 'time']
        ]
        assert np.array_equal(
            forecast_sc[forecast_cols], forecast_dt[forecast_cols]
        )

    # Check that the forecast summary tables span the expected interval.
    fs_times = [1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0]
    max_time = 15.0
    for fs_time in fs_times:
        for table_name in ['forecasts', 'model_cints']:
            table = result_sc.forecasts[fs_time].tables[table_name]
            assert all(table['fs_time'] == fs_time)
            assert all(table['time'] >= fs_time)
            assert all(table['time'] <= max_time)
            assert any(table['time'] == fs_time)
            assert any(table['time'] == max_time)


def test_config():
    """
    Regression test, check that model and forecast credible intervals have not
    changed.
    """
    obs_x_file = 'predation-counts-x.ssv'
    obs_y_file = 'predation-counts-y.ssv'

    toml_data = pypfilt.examples.predation.example_toml_data()
    toml_io = io.StringIO(toml_data)

    obs_x_data = pypfilt.examples.predation.example_obs_x_data()
    with open(obs_x_file, 'w') as f:
        f.write(obs_x_data)

    obs_y_data = pypfilt.examples.predation.example_obs_y_data()
    with open(obs_y_file, 'w') as f:
        f.write(obs_y_data)

    instances = pypfilt.scenario.load_instances(toml_io)
    contexts = [inst.build_context() for inst in instances]

    assert len(contexts) == 1
    context = contexts[0]

    forecast_times = [1.0, 3.0, 5.0, 7.0, 9.0]

    results = pypfilt.forecast(context, forecast_times, filename=None)

    assert results.estimation is not None
    assert len(results.obs) > 0
    for t in forecast_times:
        assert t in results.forecasts
        # Compare the credible intervals for x(t) and y(t) to the forecast
        # credible intervals; the forecast CIs should be wider because they
        # include the observation variance.
        tables = results.forecasts[t].tables
        assert 'model_cints' in tables
        assert 'forecasts' in tables
        model_cints = tables['model_cints']
        forecasts = tables['forecasts']
        probs = np.unique(forecasts['prob'])
        probs = [pr for pr in probs if pr > 0]
        for name in np.unique(forecasts['unit']):
            for pr in probs:
                m_ci = model_cints[
                    np.logical_and(
                        model_cints['name'] == name, model_cints['prob'] == pr
                    )
                ]
                m_fs = forecasts[
                    np.logical_and(
                        forecasts['unit'] == name, forecasts['prob'] == pr
                    )
                ]
                assert m_ci.shape == m_fs.shape
                assert np.all(m_ci['ymin'] > m_fs['ymin'])
                assert np.all(m_ci['ymax'] < m_fs['ymax'])

    for data_file in [obs_x_file, obs_y_file]:
        assert os.path.isfile(data_file)
        os.remove(data_file)

    # Save the state vector credible intervals and forecast credible intervals
    # as plain-text files in the tests directory, which we can version-track
    # and use for regression tests.

    tbl_cints = np.concatenate(
        tuple(
            [results.estimation.tables['model_cints']]
            + [
                results.forecasts[n].tables['model_cints']
                for n in forecast_times
            ]
        )
    )
    tbl_cints = tbl_cints[['fs_time', 'time', 'name', 'prob', 'ymin', 'ymax']]
    tbl_fs = np.concatenate(
        tuple(
            [results.estimation.tables['forecasts']]
            + [
                results.forecasts[n].tables['forecasts']
                for n in forecast_times
            ]
        )
    )
    tbl_fs = tbl_fs[['fs_time', 'time', 'unit', 'prob', 'ymin', 'ymax']]

    save_dir = os.path.abspath('tests')
    cint_file = os.path.join(save_dir, 'config_cints.ssv')
    fs_file = os.path.join(save_dir, 'config_forecasts.ssv')
    np.savetxt(
        cint_file,
        tbl_cints,
        comments='',
        header=' '.join(tbl_cints.dtype.names),
        fmt='%d %d %s %d %0.6f %0.6f',
    )
    np.savetxt(
        fs_file,
        tbl_fs,
        comments='',
        header=' '.join(tbl_fs.dtype.names),
        fmt='%d %d %s %d %0.6f %0.6f',
    )


def test_simulated_obs_forecasts():
    """
    Generate simulated observations from the predation model, and then use
    these simulated observations to generate forecasts.
    """
    obs_tables = simulate_from_model('predation.toml')

    # Run forecasts against these simulated observations.
    instance = predation_instance('predation.toml')
    for obs_unit in obs_tables.keys():
        instance.settings['summary']['tables'][obs_unit] = {
            'component': 'pypfilt.summary.SimulatedObs',
            'observation_unit': obs_unit,
            'expected_obs_monitor': 'expected_obs',
        }
    context = instance.build_context(obs_tables=obs_tables)
    fs_times = [1.0, 3.0, 5.0, 7.0, 9.0]
    t1 = context.settings['time']['until']
    results = pypfilt.forecast(context, fs_times, filename=None)
    remove_example_files()

    # Check the simulated observations for each forecast.
    for fs_time in fs_times:
        assert fs_time in results.forecasts
        tables = results.forecasts[fs_time].tables
        for obs_unit in obs_tables.keys():
            assert obs_unit in tables
            sim_obs = tables[obs_unit]
            assert all(sim_obs['fs_time'] == fs_time)
            assert all(sim_obs['time'] >= fs_time)
            num_particles = np.sum(sim_obs['time'] == fs_time)
            assert len(sim_obs) == num_particles * int(t1 - fs_time + 1)

    # Save the simulated observations as plain-text files in the tests
    # directory, which we can version-track and use for regression tests.
    for obs_unit in obs_tables.keys():
        all_sim_obs = tuple(
            results.forecasts[fs_time].tables[obs_unit]
            for fs_time in fs_times
        )
        all_sim_obs = np.concatenate(all_sim_obs)
        all_sim_obs = all_sim_obs[['fs_time', 'time', 'value']]
        # Only retain every 500th row: 2 for each observation type, each day.
        sim_obs_tbl = all_sim_obs[::500]
        save_dir = os.path.abspath('tests')
        sim_obs_base = 'simulated_obs_{}.ssv'.format(obs_unit)
        sim_obs_file = os.path.join(save_dir, sim_obs_base)
        np.savetxt(
            sim_obs_file,
            sim_obs_tbl,
            fmt='%d %d %0.6f',
            header='fs_time time value',
            comments='',
        )


def simulate_from_model(toml_file):
    """
    Simulate observations from the predation model.
    """
    instance = predation_instance(toml_file)
    apply_ground_truth_prior(instance)
    obs_tables = pypfilt.simulate_from_model(instance, particles=1)
    remove_example_files()
    return obs_tables


def test_simulate_from_scalar_model():
    """
    Simulate observations from the predation model.
    """
    obs_tables = simulate_from_model('predation.toml')

    # Check that the output appears sensible.
    t0 = 0.0
    t1 = 15.0
    num_obs_times = int(t1 - t0 + 1)
    for obs_unit in ['x', 'y']:
        assert obs_unit in obs_tables
        sim_obs = obs_tables[obs_unit]
        assert isinstance(sim_obs, np.ndarray)
        assert len(sim_obs) == num_obs_times
        assert all(sim_obs['value'] > -0.5)
        assert all(sim_obs['value'] < 2.5)
        for t in range(num_obs_times):
            obs_at_t = sim_obs[sim_obs['time'] == float(t)]
            assert len(obs_at_t) == 1

    # Save the simulated observations as plain-text files in the tests
    # directory, which we can version-track and use for regression tests.
    save_dir = os.path.abspath('tests')
    x_obs_file = os.path.join(save_dir, 'predation-counts-x.ssv')
    y_obs_file = os.path.join(save_dir, 'predation-counts-y.ssv')
    pypfilt.examples.predation.save_scalar_observations(
        obs_tables, x_obs_file, y_obs_file
    )


def test_simulate_from_datetime_model():
    """
    Simulate observations from the predation model.
    """
    obs_tables = simulate_from_model('predation-datetime.toml')

    # Check that the output appears sensible.
    num_obs_times = 16
    for obs_unit in ['x', 'y']:
        assert obs_unit in obs_tables
        sim_obs = obs_tables[obs_unit]
        times = np.unique(sim_obs['time'])
        assert len(times) == num_obs_times
        assert isinstance(sim_obs, np.ndarray)
        assert len(sim_obs) == num_obs_times
        assert all(sim_obs['value'] > -0.5)
        assert all(sim_obs['value'] < 2.5)
        for time in times:
            obs_at_t = sim_obs[sim_obs['time'] == time]
            assert len(obs_at_t) == 1

    # Save the simulated observations as plain-text files in the tests
    # directory, which we can version-track and use for regression tests.
    save_dir = os.path.abspath('tests')
    x_obs_file = os.path.join(save_dir, 'predation-counts-x-datetime.ssv')
    y_obs_file = os.path.join(save_dir, 'predation-counts-y-datetime.ssv')
    pypfilt.examples.predation.save_datetime_observations(
        obs_tables, x_obs_file, y_obs_file
    )


def test_predation_main():
    """
    Run the predation model using the provided ``main()`` function.
    """
    output_files = [
        'predation.hdf5',
        'predation_forecasts.png',
        'predation_params.png',
    ]
    for filename in output_files:
        try:
            os.remove(filename)
        except FileNotFoundError:
            pass
    pypfilt.examples.predation.main()
    remove_example_files()
    image_dir = Path('doc') / 'getting-started'
    for filename in output_files:
        assert os.path.isfile(filename)
        if filename.endswith('.png'):
            # Update documentation images.
            image = Path(filename)
            image.rename(image_dir / filename)
        else:
            # Remove all other files.
            os.remove(filename)
