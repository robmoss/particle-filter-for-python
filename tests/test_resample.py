"""Test cases for the pypfilt.resample method."""

import logging
import numpy as np
import pytest

import pypfilt.build
from pypfilt.examples.simple import gaussian_walk_instance
from pypfilt.pfilter import reweight_ensemble
from pypfilt.resample import post_regularise, resample, resample_ixs


def minimal_context(method, fields, smooth_fields):
    """
    Construct a minimal simulation context for testing ``pypfilt.resample``.
    """
    return pypfilt.build.Context(
        scenario_id='test_resample',
        descriptor='',
        source=None,
        settings={
            'filter': {
                'resample': {
                    'method': method,
                },
                'regularisation': {
                    'enabled': False,
                    'tolerance': 1e-8,
                    'bandwidth_scale': 0.5,
                    'regularise_or_fail': False,
                    'bounds': {
                        'a': {'min': -100, 'max': 100},
                        'b': {'min': -100, 'max': 100},
                    },
                },
            },
        },
        data={},
        component={
            'model': dummy_model(fields, smooth_fields),
            'random': {
                'resample': np.random.default_rng(seed=20211102),
            },
        },
        event_handler={},
        all_observations=[],
    )


@pytest.mark.parametrize('method', ['basic', 'stratified', 'deterministic'])
def test_resample(method):
    model_fields = []
    ctx = minimal_context(method, model_fields, model_fields)

    weights = np.array([0.50, 0.25, 0.1, 0.1, 0.02, 0.02, 0.01])
    ixs = np.zeros(weights.shape)
    resampled = np.zeros(weights.shape, dtype=np.bool_)
    dtype = [
        ('weight', np.float64),
        ('prev_ix', np.int64),
        ('resampled', np.bool_),
    ]
    n_tries = 10
    for _ in range(n_tries):
        x = np.array(list(zip(weights, ixs, resampled)), dtype=dtype)
        resample(ctx, x)
        prev_ix = x['prev_ix']
        if not all(x['weight'] == x['weight'][0]):
            pytest.fail('Resampled weights are not identical')
        if any(prev_ix == 0) and any(prev_ix == 1) and any(prev_ix > 1):
            # The first particle (weight 0.5) has been selected at least once,
            # as has the second particle (weight 0.25) and at least one of the
            # other particles (net weight 0.25).
            break
    else:
        pytest.fail(f'Failure after {n_tries} loops with {method} resampling')


@pytest.mark.parametrize('method', ['basic', 'stratified', 'deterministic'])
def test_resample_ixs(method):
    weights = np.array([0.50, 0.25, 0.1, 0.1, 0.02, 0.02, 0.01])
    rnd = np.random.default_rng()
    n_tries = 10
    for _ in range(n_tries):
        ixs = resample_ixs(weights, rnd, method)
        if any(ixs == 0) and any(ixs == 1) and any(ixs > 1):
            # The first particle (weight 0.5) has been selected at least once,
            # as has the second particle (weight 0.25) and at least one of the
            # other particles (net weight 0.25).
            break
    else:
        pytest.fail(f'Failure after {n_tries} loops with {method} resampling')


@pytest.mark.parametrize('method', ['stratified', 'deterministic'])
def test_resample_ixs_count_fewer(method):
    """
    Ensure that resample_ixs() can return fewer samples than the number of
    candidates.

    In selecting 4 samples from the provided weights array using stratified or
    deterministic resamples, we have the following guarantees:

    1. The first particle will be selected *exactly twice* because its
       weight spans the [0, 0.25) and [0.25, 0.50) intervals;

    2. The second particle will be selected *exactly once* because its
       weight spans the [0.50, 0.75) interval; and

    3. One of the remaining particles will be selected once, because their
       combined weights span the [0.75, 1.0) interval.
    """
    weights = np.array([0.50, 0.25, 0.1, 0.1, 0.02, 0.02, 0.01])
    count = 4
    rnd = np.random.default_rng()
    ixs = resample_ixs(weights, rnd, method, count=count)
    assert len(ixs) == count
    assert sum(ixs == 0) == 2
    assert sum(ixs == 1) == 1
    assert sum(ixs >= 2) == 1


@pytest.mark.parametrize('method', ['stratified', 'deterministic'])
def test_resample_ixs_count_more(method):
    """
    Ensure that resample_ixs() can return more samples than the number of
    candidates.

    In selecting 10 samples from the provided weights array using stratified
    or deterministic resamples, we have the following guarantees:

    1. The first particle will be selected *exactly five times*;
    2. The second particle will be selected *either 2 or 3 times*; and
    3. The third particle will be selected *either 2 or 3 times*.
    """
    weights = np.array([0.50, 0.25, 0.25])
    count = 10
    rnd = np.random.default_rng()
    ixs = resample_ixs(weights, rnd, method, count=count)
    assert len(ixs) == count
    assert sum(ixs == 0) == 5
    assert sum(ixs == 1) in [2, 3]
    assert sum(ixs >= 2) in [2, 3]


def dummy_model(fields, smooth_fields):
    """
    Construct a dummy model that only defines its field names, field types,
    and which fields can be smoothed.

    :param fields: The list of field names, all of which are treated as
        floating-point values.
    :param smooth_fields: The list of field names that can be smoothed.
    """

    class DummyModel:
        def field_names(self):
            return fields

        def field_types(self, ctx):
            return [(f, float) for f in fields]

        def can_smooth(self):
            return smooth_fields

    return DummyModel()


def test_post_regularise():
    """
    Test post-regularisation using an ensemble of 10 particles for a model of
    three variables.
    """
    model_fields = ['a', 'b', 'c']
    method = 'deterministic'
    ctx = minimal_context(method, model_fields, model_fields)
    ctx.settings['filter']['regularisation']['enabled'] = True

    # NOTE: at a minimum, we need 'weight', 'prev_ix', 'resampled', and
    # 'state_vec'.
    px = np.array(
        [
            (0.1, 0, False, (1.0, 1.0, 1.0)),
            (0.1, 0, False, (1.0, 2.0, 1.0)),
            (0.1, 0, False, (1.0, 3.0, 1.0)),
            (0.1, 0, False, (1.0, 4.0, 1.0)),
            (0.1, 0, False, (1.0, 5.0, 1.0)),
            (0.1, 1, False, (2.0, 6.0, 1.0)),
            (0.1, 1, False, (2.0, 7.0, 1.0)),
            (0.1, 1, False, (2.0, 8.0, 1.0)),
            (0.1, 1, False, (2.0, 9.0, 1.0)),
            (0.1, 1, False, (2.0, 10.0, 1.0)),
        ],
        dtype=[
            ('weight', np.float64),
            ('prev_ix', np.int64),
            ('resampled', np.bool_),
            ('state_vec', ctx.component['model'].field_types(ctx)),
        ],
    )
    new_px = np.copy(px)
    post_regularise(ctx, px, new_px)
    assert np.array_equal(px['weight'], new_px['weight'])
    assert np.array_equal(px['prev_ix'], new_px['prev_ix'])
    assert not np.array_equal(px['state_vec'], new_px['state_vec'])
    assert np.array_equal(px['state_vec']['c'], new_px['state_vec']['c'])
    abs_diff_a = np.abs(new_px['state_vec']['a'] - px['state_vec']['a'])
    assert np.mean(abs_diff_a) < 0.2
    assert np.mean(abs_diff_a) > 0.01
    abs_diff_b = np.abs(new_px['state_vec']['b'] - px['state_vec']['b'])
    assert np.mean(abs_diff_b) < 1.5
    assert np.mean(abs_diff_b) > 0.2


def test_post_regularise_matrix_failure_(caplog):
    """
    Test post-regularisation where there is perfect correlation between the
    parameters.
    """
    caplog.set_level(logging.DEBUG)

    model_fields = ['a', 'b', 'c']
    method = 'deterministic'
    ctx = minimal_context(method, model_fields, model_fields)
    ctx.settings['filter']['regularisation']['enabled'] = True
    ctx.settings['filter']['regularisation']['regularise_or_fail'] = False

    px = np.array(
        [
            (0.1, 0, False, (1.0, 1.0, 1.0)),
            (0.1, 0, False, (1.0, 1.0, 1.0)),
            (0.1, 0, False, (1.0, 1.0, 1.0)),
            (0.1, 0, False, (1.0, 1.0, 1.0)),
            (0.1, 0, False, (1.0, 1.0, 1.0)),
            (0.1, 1, False, (2.0, 2.0, 1.0)),
            (0.1, 1, False, (2.0, 2.0, 1.0)),
            (0.1, 1, False, (2.0, 2.0, 1.0)),
            (0.1, 1, False, (2.0, 2.0, 1.0)),
            (0.1, 1, False, (2.0, 2.0, 1.0)),
        ],
        dtype=[
            ('weight', np.float64),
            ('prev_ix', np.int64),
            ('resampled', np.bool_),
            ('state_vec', ctx.component['model'].field_types(ctx)),
        ],
    )
    new_px = np.copy(px)
    post_regularise(ctx, px, new_px)

    # Check that the particles were not modified.
    assert np.array_equal(px, new_px)
    assert np.array_equal(px['weight'], new_px['weight'])
    assert np.array_equal(px['prev_ix'], new_px['prev_ix'])
    assert np.array_equal(px['state_vec'], new_px['state_vec'])
    # Check that regularisation was attempted.
    assert 'Post-RPF: smoothing a, b' in caplog.messages
    # Check that appropriate warnings were logged.
    warnings = [r for r in caplog.records if r.levelname == 'WARNING']
    assert len(warnings) > 0
    assert any('Matrix is not positive definite' in r.msg for r in warnings)

    # Check that this failure mode raises an exception.
    ctx.settings['filter']['regularisation']['regularise_or_fail'] = True
    with pytest.raises(np.linalg.LinAlgError):
        post_regularise(ctx, px, new_px)


@pytest.mark.filterwarnings('ignore:Degrees of freedom <= 0')
def test_post_regularise_floating_point_failure(caplog):
    """
    Test post-regularisation where almost all of the probability mass is
    located in a single particle..
    """
    caplog.set_level(logging.DEBUG)

    model_fields = ['a', 'b', 'c']
    method = 'deterministic'
    ctx = minimal_context(method, model_fields, model_fields)
    ctx.settings['filter']['regularisation']['enabled'] = True
    ctx.settings['filter']['regularisation']['regularise_or_fail'] = False

    # NOTE: allocate a tiny non-zero weight to one other particle.
    eps = 1e-20
    px = np.array(
        [
            (1.0 - eps, 0, False, (1.0, 1.0, 1.0)),
            (0.0, 0, False, (1.0, 1.0, 1.0)),
            (0.0, 0, False, (1.0, 1.0, 1.0)),
            (0.0, 0, False, (1.0, 1.0, 1.0)),
            (0.0, 0, False, (1.0, 1.0, 1.0)),
            (eps, 1, False, (2.0, 2.0, 1.0)),
            (0.0, 1, False, (2.0, 2.0, 1.0)),
            (0.0, 1, False, (2.0, 2.0, 1.0)),
            (0.0, 1, False, (2.0, 2.0, 1.0)),
            (0.0, 1, False, (2.0, 2.0, 1.0)),
        ],
        dtype=[
            ('weight', np.float64),
            ('prev_ix', np.int64),
            ('resampled', np.bool_),
            ('state_vec', ctx.component['model'].field_types(ctx)),
        ],
    )
    new_px = np.copy(px)
    post_regularise(ctx, px, new_px)

    # Check that the particles were not modified.
    assert np.array_equal(px, new_px)
    assert np.array_equal(px['weight'], new_px['weight'])
    assert np.array_equal(px['prev_ix'], new_px['prev_ix'])
    assert np.array_equal(px['state_vec'], new_px['state_vec'])
    # Check that regularisation was attempted.
    assert 'Post-RPF: smoothing a, b' in caplog.messages
    # Check that appropriate warnings were logged.
    warnings = [r for r in caplog.records if r.levelname == 'WARNING']
    assert len(warnings) > 0
    assert any(
        'Post-RPF: cannot calculate the covariance matrix' in r.msg
        for r in warnings
    )

    # Check that this failure mode raises an exception.
    ctx.settings['filter']['regularisation']['regularise_or_fail'] = True
    with pytest.raises(FloatingPointError):
        post_regularise(ctx, px, new_px)


def test_reweight_failure(caplog):
    """
    Test that reweighting failures are handled as intended.
    """
    caplog.set_level(logging.DEBUG)

    instance = gaussian_walk_instance()
    ctx = instance.build_context(obs_tables={})
    ctx.settings['filter']['reweight_or_fail'] = False
    num_px = ctx.settings['filter']['particles']

    # Housekeeping performed by pfilter.run().
    start = 0.0
    until = 10.0
    ctx.settings['time']['sim_start'] = start
    ctx.settings['time']['sim_until'] = until
    sim_time = ctx.component['time']
    sim_time.set_period(start, until, ctx.settings['time']['steps_per_unit'])
    steps = sim_time.with_observation_tables(ctx, {})
    history = pypfilt.state.History(ctx)

    # Create a snapshot at the first time-step.
    (step_num, when, _obs) = next(steps)
    history.set_time_step(step_num, when)
    snapshot = history.snapshot(ctx)

    # Define the current particle weights; particle 0 has zero weight.
    snapshot.weights[0] = 0.0
    snapshot.weights[1:] = 1 / (num_px - 1)
    weights_in = snapshot.weights.copy()

    # Define the log-likelihoods so that particle 0 is the only one in
    # agreement with the observation.
    logs = np.zeros(num_px)
    logs[1:] = -1e20

    # Check that the weights were not modified.
    reweight_ensemble(ctx, snapshot, logs)
    assert np.array_equal(snapshot.weights, weights_in)

    # Check that this failure mode raises an exception.
    ctx.settings['filter']['reweight_or_fail'] = True
    with pytest.raises(ValueError, match='particle weights sum to zero'):
        reweight_ensemble(ctx, snapshot, logs)
