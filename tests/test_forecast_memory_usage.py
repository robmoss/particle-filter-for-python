"""
Test pypfilt's memory usage when running many forecasting passes with a single
call to :func:`pypfilt.forecast`.

See :func:`run_forecasts_and_discard_history` for an example greatly reducing
memory usage by discarding the particle history matrix and associated data
structures.
"""

import gc
import io
import numpy as np
import pypfilt
import tracemalloc


def test_forecast_memory_usage_keep_history():
    """
    Test the memory usage when running sets of forecasts and retaining all of
    the results, include the particle history matrices.
    """
    toml_str = memory_usage_scenario_str()
    mem_usage = run_sequence_of_forecasts(toml_str, keep=True)
    # Check that memory usage always exceeded 100 MB.
    assert all(mem_usage > 100)
    # Check pass 1 used <200 MB and subsequent passes used >200 MB.
    assert all(mem_usage[:1] < 200)
    assert all(mem_usage[1:] > 200)
    # Check passes 1 & 2 used <300 MB and subsequent passes used >300 MB.
    assert all(mem_usage[:2] < 300)
    assert all(mem_usage[2:] > 300)


def test_forecast_memory_usage_discard_history():
    """
    Test the memory usage when running sets of forecasts and discarding the
    particle history matrices and related data structures.
    """
    toml_str = memory_usage_scenario_str()
    mem_usage = run_sequence_of_forecasts(toml_str, keep=False)
    # Check that memory usage was always less than 10 MB.
    assert all(mem_usage < 10)


def test_forecast_memory_usage_dont_save_history():
    """
    Test the memory usage when running sets of forecasts and not retaining the
    particle history matrices.
    """
    toml_str = memory_usage_scenario_no_save_str()
    mem_usage = run_sequence_of_forecasts(toml_str, keep=False)
    # Check that memory usage was always less than 10 MB.
    assert all(mem_usage < 10)


def run_sequence_of_forecasts(toml_str, keep):
    """
    Run a sequence of forecasts and return an array of Python's memory usage
    (in megabytes) after each subset of forecasts.

    :param toml_str: The scenario configuration, provided as a TOML string.
    :type toml_str: str
    :param keep: Whether to keep the particle history matrices and related
        data structures.
    :type keep: bool
    """
    toml_io = io.StringIO(memory_usage_scenario_str())
    instances = list(pypfilt.load_instances(toml_io))
    assert len(instances) == 1
    instance = instances[0]

    fs_start = 1
    fs_count = 10
    fs_reps = 3
    fs_results = []
    mem_usage = []

    # NOTE: run the garbage collector to ensure unused memory is freed.
    collect = True
    tracemalloc.start()

    megabyte = 1024 * 1024

    for _ in range(fs_reps):
        fs_times = np.arange(fs_start, fs_start + fs_count)
        if keep:
            results = run_forecasts_and_keep_history(
                instance, fs_times, collect=collect
            )
        else:
            results = run_forecasts_and_discard_history(
                instance, fs_times, collect=collect
            )
        fs_results.append(results)

        # Record memory usage in MB.
        (current, _peak) = tracemalloc.get_traced_memory()
        mem_usage.append(current / megabyte)

        fs_start += fs_count

    return np.array(mem_usage)


def run_forecasts_and_keep_history(instance, fs_times, collect=False):
    """
    Run forecasts and return the results, retaining the particle history
    matrices and related data structures.

    :param instance: The scenario instance.
    :type instance: pypfilt.scenario.Instance
    :param fs_times: The times at which forecasts should be generated.
    :param collect: Whether to run the Python garbage collector after
        collecting all of the forecasting results.
    """
    context = instance.build_context()
    results = pypfilt.forecast(context, fs_times, filename=None)

    if collect:
        gc.collect()

    return results


def run_forecasts_and_discard_history(instance, fs_times, collect=False):
    """
    Run forecasts and return the results, discarding the particle history
    matrices and related data structures.

    :param instance: The scenario instance.
    :type instance: pypfilt.scenario.Instance
    :param fs_times: The times at which forecasts should be generated.
    :param collect: Whether to run the Python garbage collector after
        collecting all of the forecasting results.
    """
    context = instance.build_context()
    results = pypfilt.forecast(context, fs_times, filename=None)

    context.component['history'].matrix = None
    discard_history(results.estimation)
    for fs_time in results.forecasts:
        discard_history(results.forecasts[fs_time])

    if collect:
        gc.collect()

    return results


def discard_history(result):
    """
    Remove the history matrix and related data structures from the results of
    a particle filter pass.

    :param result: The results of a single particle filter pass.
    :type result: pypfilt.pfilter.Result
    """
    result.history.matrix = None
    result.settings['filter']['partition'] = {}


class MemoryUsageModel(pypfilt.model.Model):
    """
    A model to test pypfilt's memory usage when generating a large number of
    forecasts.
    """

    def field_types(self, ctx):
        return [('x', np.float64)]

    def init(self, ctx, vec):
        vec['x'] = ctx.data['prior']['x']

    def update(self, ctx, time_step, is_fs, prev, curr):
        curr['x'] = prev['x']


def memory_usage_scenario_str():
    """
    Define a forecast scenario for MemoryUsageModel that uses many particles,
    so that the particle history matrix uses a noticeable amount of memory.
    """
    return """
    [components]
    model = "test_forecast_memory_usage.MemoryUsageModel"
    time = "pypfilt.Scalar"
    sampler = "pypfilt.sampler.LatinHypercube"
    summary = "pypfilt.summary.HDF5"

    [time]
    start = 0.0
    until = 120.0
    steps_per_unit = 1

    [files]
    input_directory = "."
    output_directory = "."
    temp_directory = "."
    delete_cache_file_before_forecast = true
    delete_cache_file_after_forecast = true

    [filter]
    particles = 100000
    prng_seed = 3001
    history_window = 2
    resample.threshold = 0.5
    results.save_history = true

    [prior]
    x = { name = "randint", args.low = 0, args.high = 5 }

    [scenario.test]
    name = "Test Scenario"
    """


def memory_usage_scenario_no_save_str():
    """
    Define a forecast scenario for MemoryUsageModel that uses many particles,
    but does not return the particle history matrix.
    """
    return """
    [components]
    model = "test_forecast_memory_usage.MemoryUsageModel"
    time = "pypfilt.Scalar"
    sampler = "pypfilt.sampler.LatinHypercube"
    summary = "pypfilt.summary.HDF5"

    [time]
    start = 0.0
    until = 120.0
    steps_per_unit = 1

    [files]
    input_directory = "."
    output_directory = "."
    temp_directory = "."
    delete_cache_file_before_forecast = true
    delete_cache_file_after_forecast = true

    [filter]
    particles = 100000
    prng_seed = 3001
    history_window = 2
    resample.threshold = 0.5
    results.save_history = false

    [prior]
    x = { name = "randint", args.low = 0, args.high = 5 }

    [scenario.test]
    name = "Test Scenario"
    """
