"""Test the pypfilt.model.OdeModel class."""

import numpy as np
import os
import pytest

import pypfilt
import pypfilt.examples.lorenz
from pypfilt.model import OdeModel


class MultiDim(OdeModel):
    """
    A model that allows us to define the state vector structure.
    """

    def __init__(self, fields):
        self.fields = fields

    def field_types(self, ctx):
        return self.fields

    def d_dt(self, time, xt, ctx, is_forecast):
        pass


def test_ode_model_multi_dim_vector():
    """
    Test that OdeModel correctly handles a multi-dimensional state vector.
    """
    model = MultiDim(
        [
            ('x', float, (3, 5)),
            ('y', float),
            ('z', float, 2),
            ('a', float, (2, 2, 2, 2)),
        ]
    )

    # Check that OdeModel detects the correct number of float values.
    state_vec = np.zeros(10, dtype=model.field_types(ctx=None))
    num_fields = model._num_fields(state_vec)
    assert num_fields == 34
    # Check that we can view the state vector as a flat array.
    xt = state_vec.view((np.float64, num_fields)).reshape(-1)
    assert xt.shape == (340,)


def test_ode_model_invalid_field_type():
    """
    Test that OdeModel rejects a state vector with non-float fields.
    """
    model = MultiDim(
        [
            ('x', int, (3, 5)),
            ('y', float),
            ('z', float, 2),
            ('a', float, (2, 2, 2, 2)),
        ]
    )

    # Check that OdeModel detects an invalid field:
    state_vec = np.zeros(10, dtype=model.field_types(ctx=None))
    with pytest.raises(ValueError, match='Unsupported field type'):
        model._num_fields(state_vec)


def test_ode_model_invalid_nested_field():
    """
    Test that OdeModel rejects a state vector with nested fields.
    """
    model = MultiDim(
        [
            ('x', [('i', float), ('j', float)]),
            ('y', float),
            ('z', float, 2),
            ('a', float, (2, 2, 2, 2)),
        ]
    )

    # Check that OdeModel detects an invalid field:
    state_vec = np.zeros(10, dtype=model.field_types(ctx=None))
    with pytest.raises(ValueError, match='Unsupported field type'):
        model._num_fields(state_vec)


def test_ode_model_solver_options():
    """
    Test that OdeModel passes solver options to the solver.

    We test this by relaxing the absolute and relative solver tolerances, and
    verify that this yields different simulated observations.

    The default tolerances are ``rtol = 1e-3`` and ``atol = 1e-6``.
    """
    solver_options = {
        'rtol': 1e-1,
        'atol': 1e-4,
    }

    scenario_file = 'lorenz63_simulate.toml'
    with open(scenario_file, 'w') as f:
        f.write(pypfilt.examples.lorenz.lorenz63_simulate_toml())

    # Simulate observations for x(t), y(t), and z(t).
    instances = list(pypfilt.load_instances(scenario_file))
    instance = instances[0]
    obs_tables_orig = pypfilt.simulate_from_model(instance)

    # Simulate observations with reduced solver tolerances, and verify that
    # this yields different simulated observations.
    instances = list(pypfilt.load_instances(scenario_file))
    instance = instances[0]
    if 'model' not in instance.settings:
        instance.settings['model'] = {}
    instance.settings['model']['ode_solver_options'] = solver_options
    obs_tables_new = pypfilt.simulate_from_model(instance)

    # Ensure that the observed values are not identical, due to the change in
    # ODE solver options.
    assert obs_tables_orig.keys() == obs_tables_new.keys()
    for key, orig_table in obs_tables_orig.items():
        new_table = obs_tables_new[key]
        assert orig_table.shape == new_table.shape
        assert orig_table.dtype == new_table.dtype
        assert np.array_equal(orig_table['time'], new_table['time'])
        assert not np.allclose(orig_table['value'], new_table['value'])

    # Simulate observations with the default options for a second time, and
    # verify that this yields the original simulated observations.
    instances = list(pypfilt.load_instances(scenario_file))
    instance = instances[0]
    obs_tables_new = pypfilt.simulate_from_model(instance)

    # Ensure that the observed values are identical, due to using the same
    # (default) ODE solver options.
    assert obs_tables_orig.keys() == obs_tables_new.keys()
    for key, orig_table in obs_tables_orig.items():
        new_table = obs_tables_new[key]
        assert np.array_equal(orig_table, new_table)

    os.remove(scenario_file)
