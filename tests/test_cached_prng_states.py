"""Test that pypfile.cache correctly saves and restores PRNG states."""

import logging
import numpy as np
import os
import pypfilt
import pypfilt.examples.simple


def test_cached_prng_states(caplog):
    """Test that caching PRNG states has the desired effects."""

    # NOTE: The caplog fixture captures logging; see the pytest documentation:
    # https://docs.pytest.org/en/stable/logging.html
    caplog.set_level(logging.INFO)

    time_scale = pypfilt.Scalar()
    t0 = 0.0
    t1 = 15.0
    t2 = 30.0
    (results_1, results_2) = run_two_forecasts(caplog, time_scale, t0, t1, t2)

    # Extract the relevant summary tables for each forecast.
    cints_1 = results_1.forecasts[t1].tables['model_cints']
    cints_2 = results_2.forecasts[t1].tables['model_cints']
    sim_obs_1 = results_1.forecasts[t1].tables['sim_obs']
    sim_obs_2 = results_2.forecasts[t1].tables['sim_obs']

    # The state of each PRNG in `ctx.component['random']` is now cached, so
    # the model credible intervals should be identical.
    assert np.array_equal(cints_1, cints_2)

    # The SimulatedObs table now caches its PRNG states, so the simulated
    # observations should be identical.
    assert np.array_equal(sim_obs_1, sim_obs_2)


def test_cached_prng_states_created_prngs(caplog):
    """Test that additional PRNGs are cached as per the core PRNGs."""
    caplog.set_level(logging.INFO)

    # Ensure we start without a cache file.
    cache_file = 'test_cached_prng_states_created_prngs.hdf5'
    try:
        os.remove(cache_file)
    except FileNotFoundError:
        pass

    t0 = 0
    t1 = 6
    t2 = 10
    inst = pypfilt.examples.simple.gaussian_walk_instance()
    inst.settings['time']['start'] = t0
    inst.settings['time']['until'] = t2
    inst.settings['files']['cache_file'] = cache_file
    inst.settings['files']['delete_cache_file_before_forecast'] = False
    inst.settings['files']['delete_cache_file_after_forecast'] = False

    prng_name = 'new_prng'
    prng_seed = 20250114

    # NOTE: need to use 'new_prng' in some way during the forecast.
    def simulate_value_after_each_timestep(ctx):
        values = []

        def after_time_step(event):
            prng = event.ctx.component['random'][prng_name]
            values.append(prng.random())

        ctx.install_event_handler('AfterTimeStep', after_time_step)

        return values

    ctx_1 = inst.build_context()
    prng_1 = ctx_1.create_prng(prng_name, seed=prng_seed)
    rand_vals_1 = simulate_value_after_each_timestep(ctx_1)
    results_1 = pypfilt.forecast(ctx_1, [t1], filename=None)

    # Verify that the first forecast returned an estimation pass.
    assert results_1.estimation is not None
    assert isinstance(results_1.estimation, pypfilt.pfilter.Result)
    # Check that no cached state was used.
    assert 'No estimation pass needed' not in caplog.text
    # NOTE: this message has *two spaces* so that it aligns with
    # 'Forecasting from' messages.
    assert 'Estimating  from 0.0' in caplog.text

    ctx_2 = inst.build_context()
    prng_2 = ctx_2.create_prng(prng_name, seed=prng_seed)
    rand_vals_2 = simulate_value_after_each_timestep(ctx_2)
    results_2 = pypfilt.forecast(ctx_2, [t1], filename=None)

    # Verify that the second forecast did not return an estimation pass.
    assert results_2.estimation is None
    # Check that a cached state was used.
    assert 'No estimation pass needed' in caplog.text

    os.remove(cache_file)

    # Verify that we have the same random values from each forecast, noting
    # that the first set will include values generated during the initial
    # estimation pass.
    assert len(rand_vals_1) == t2
    assert len(rand_vals_2) == t2 - t1
    assert rand_vals_1[t1:] == rand_vals_2
    assert prng_1.bit_generator.state == prng_2.bit_generator.state


def run_forecast(time_scale, t0, t1, t2, cache_file, obs_tables):
    """Generate a GaussianWalk forecast."""
    inst = pypfilt.examples.simple.gaussian_walk_instance()

    inst.settings['time']['start'] = t0
    inst.settings['time']['until'] = t2
    inst.settings['files']['cache_file'] = cache_file
    inst.settings['files']['delete_cache_file_before_forecast'] = False
    inst.settings['files']['delete_cache_file_after_forecast'] = False

    context = inst.build_context(obs_tables=obs_tables)

    return pypfilt.forecast(context, [t1], filename=None)


def run_two_forecasts(caplog, time_scale, t0, t1, t2):
    """
    Run the same forecast twice; the first time with an empty cache, and the
    second resuming from a cached state.
    """
    # Ensure we start without a cache file.
    cache_file = 'test_cached_prng_states.hdf5'
    try:
        os.remove(cache_file)
    except FileNotFoundError:
        pass

    # Simulate observations from a single particle.
    obs_tables = simulate_from_model(time_scale, t0, t1)

    # Run a forecast to populate the cache, and then check that rerunning the
    # forecast from the cached state produces identical model dynamics.
    assert not os.path.isfile(cache_file)
    results_1 = run_forecast(time_scale, t0, t1, t2, cache_file, obs_tables)

    # Verify that the first forecast returned an estimation pass.
    assert results_1.estimation is not None
    assert isinstance(results_1.estimation, pypfilt.pfilter.Result)
    # Check that no cached state was used.
    assert 'No estimation pass needed' not in caplog.text
    # NOTE: this message has *two spaces* so that it aligns with
    # 'Forecasting from' messages.
    assert 'Estimating  from 0.0' in caplog.text

    assert os.path.isfile(cache_file)
    results_2 = run_forecast(time_scale, t0, t1, t2, cache_file, obs_tables)

    # Verify that the second forecast did not return an estimation pass.
    assert results_2.estimation is None
    # Check that a cached state was used.
    assert 'No estimation pass needed' in caplog.text

    os.remove(cache_file)

    return (results_1, results_2)


def simulate_from_model(time_scale, t0, t1):
    """Simulate observations from a single GaussianWalk particle."""
    inst = pypfilt.examples.simple.gaussian_walk_instance()

    inst.settings['time']['start'] = t0
    inst.settings['time']['until'] = t1
    inst.settings['prior']['x'] = {
        'name': 'constant',
        'args': {'value': 15.0},
    }

    return pypfilt.simulate_from_model(inst)
