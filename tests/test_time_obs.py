import datetime
import numpy as np
import pytest
import warnings

from pypfilt.examples.predation import (
    predation_datetime_instance,
    remove_example_files,
)


def test_predation_datetime():
    inst = predation_datetime_instance()
    inst.settings['summary']['metadata']['packages'] = {}
    ctx = inst.build_context()

    start = datetime.datetime(2017, 5, 1)
    until = datetime.datetime(2017, 5, 16)
    assert ctx.settings['time']['start'] == start
    assert ctx.settings['time']['until'] == until
    assert ctx.settings['time']['steps_per_unit'] == 1

    time_scale = ctx.component['time']
    time_scale.set_period(
        ctx.settings['time']['start'],
        ctx.settings['time']['until'],
        ctx.settings['time']['steps_per_unit'],
    )

    obs_tables = ctx.data['obs']

    time_steps = list(time_scale.with_observation_tables(ctx, obs_tables))
    assert len(time_steps) == 15
    for ix, time_step, obs_list in time_steps:
        assert len(obs_list) == 2
        for obs in obs_list:
            assert obs['time'] == time_step.end
        assert any(obs['unit'] == 'x' for obs in obs_list)
        assert any(obs['unit'] == 'y' for obs in obs_list)

        # Check that the next time-step with observations is correctly
        # identified.
        next_step = time_scale.time_step_of_next_observation(
            ctx, obs_tables, time_step.end
        )
        if time_step.end < until:
            assert next_step.step_number == ix + 1
            assert next_step.time_step.start == time_step.end
            assert next_step.time_step.end.hour == 0
            assert next_step.time_step.end.minute == 0
            assert next_step.time_step.end.day == time_step.end.day + 1
            for obs in next_step.observations:
                assert obs['time'] == next_step.time_step.end
        else:
            assert next_step is None

    start = ctx.settings['time']['start'] + datetime.timedelta(days=5)
    time_steps = list(
        time_scale.with_observation_tables(ctx, obs_tables, start)
    )
    assert len(time_steps) == 10
    for ix, time_step, obs_list in time_steps:
        assert len(obs_list) == 2
        for obs in obs_list:
            assert obs['time'] == time_step.end
        assert any(obs['unit'] == 'x' for obs in obs_list)
        assert any(obs['unit'] == 'y' for obs in obs_list)

        # Check that the next time-step with observations is correctly
        # identified.
        next_step = time_scale.time_step_of_next_observation(
            ctx, obs_tables, time_step.end
        )
        if time_step.end < until:
            assert next_step.step_number == ix + 1
            assert next_step.time_step.start == time_step.end
            assert next_step.time_step.end.hour == 0
            assert next_step.time_step.end.minute == 0
            assert next_step.time_step.end.day == time_step.end.day + 1
            for obs in next_step.observations:
                assert obs['time'] == next_step.time_step.end
        else:
            assert next_step is None

    remove_example_files()


def test_predation_datetime_smaller_steps():
    """
    Ensure that observations are only yielded at appropriate time-steps.
    """
    inst = predation_datetime_instance()
    steps_per_unit = 6
    num_days = 15
    inst.settings['summary']['metadata']['packages'] = {}
    inst.settings['time']['steps_per_unit'] = steps_per_unit
    ctx = inst.build_context()

    start = datetime.datetime(2017, 5, 1)
    until = datetime.datetime(2017, 5, 16)
    assert ctx.settings['time']['start'] == start
    assert ctx.settings['time']['until'] == until
    assert ctx.settings['time']['steps_per_unit'] == steps_per_unit

    time_scale = ctx.component['time']
    time_scale.set_period(
        ctx.settings['time']['start'],
        ctx.settings['time']['until'],
        ctx.settings['time']['steps_per_unit'],
    )

    obs_tables = ctx.data['obs']

    time_steps = list(time_scale.with_observation_tables(ctx, obs_tables))
    assert len(time_steps) == num_days * steps_per_unit

    obs_count = 0
    for _ix, time_step, obs_list in time_steps:
        if time_step.end.hour == 0 and time_step.end.minute == 0:
            assert len(obs_list) == 2
            obs_count += 2
            assert any(obs['unit'] == 'x' for obs in obs_list)
            assert any(obs['unit'] == 'y' for obs in obs_list)
        else:
            assert len(obs_list) == 0

        for obs in obs_list:
            assert obs['time'] == time_step.end

        # Check that the next time-step with observations is correctly
        # identified.
        next_step = time_scale.time_step_of_next_observation(
            ctx, obs_tables, time_step.end
        )
        if time_step.end < until:
            assert next_step.step_number % steps_per_unit == 0
            assert next_step.time_step.end.hour == 0
            assert next_step.time_step.end.minute == 0
            assert next_step.time_step.end.day == time_step.end.day + 1
            for obs in next_step.observations:
                assert obs['time'] == next_step.time_step.end
        else:
            assert next_step is None

    assert obs_count == num_days * 2

    remove_example_files()


def test_predation_datetime_large_step_warning():
    """
    Ensure that warnings are raised when time-steps are too large.
    """
    inst = predation_datetime_instance()
    inst.settings['summary']['metadata']['packages'] = {}
    ctx = inst.build_context()

    assert ctx.settings['time']['start'] == datetime.datetime(2017, 5, 1)
    assert ctx.settings['time']['until'] == datetime.datetime(2017, 5, 16)
    assert ctx.settings['time']['steps_per_unit'] == 1

    time_scale = ctx.component['time']
    time_scale.set_period(
        ctx.settings['time']['start'],
        ctx.settings['time']['until'],
        ctx.settings['time']['steps_per_unit'],
    )

    # Make the observations occur every 12 hours, not ever 24 hours.
    obs_tables = ctx.data['obs']
    delta = datetime.timedelta(hours=12)
    for unit in obs_tables:
        start = obs_tables[unit]['time'][0]
        count = len(obs_tables[unit])
        obs_tables[unit]['time'] = start + np.arange(count) * delta

    # Each observation stream has 15 observations, and after the first one
    # they should be returned as pairs. So each observation stream should
    # raise 7 warnings.
    # NOTE: the records returned by pytest.warn include all warnings, not only
    # those that match the provided pattern.
    warn_msg = 'consider using smaller time-steps'
    with pytest.warns(UserWarning, match=warn_msg) as records:
        time_steps = list(time_scale.with_observation_tables(ctx, obs_tables))
    time_warnings = [r for r in records if warn_msg in str(r.message)]
    assert len(time_warnings) == 14

    # Check that each warning is associated with this file.
    for record in time_warnings:
        assert record.filename == __file__

    assert len(time_steps) == 15
    for _ix, time_step, obs_list in time_steps:
        if time_step.end == obs_tables[unit]['time'][0]:
            assert len(obs_list) == 2
        elif time_step.end <= max(obs_tables[unit]['time']):
            assert len(obs_list) == 4
        else:
            assert len(obs_list) == 0

        for obs in obs_list:
            assert obs['time'] in [time_step.end, time_step.end - delta]

        if obs_list:
            assert any(obs['unit'] == 'x' for obs in obs_list)
            assert any(obs['unit'] == 'y' for obs in obs_list)

    remove_example_files()


def test_predation_datetime_small_step_no_warning():
    """
    Ensure that warnings are not raised when time-steps are small enough.
    """
    inst = predation_datetime_instance()
    inst.settings['summary']['metadata']['packages'] = {}
    inst.settings['time']['steps_per_unit'] = 2
    ctx = inst.build_context()

    time_scale = ctx.component['time']
    time_scale.set_period(
        ctx.settings['time']['start'],
        ctx.settings['time']['until'],
        ctx.settings['time']['steps_per_unit'],
    )

    # Make the observations occur every 12 hours, not ever 24 hours.
    obs_tables = ctx.data['obs']
    delta = datetime.timedelta(hours=12)
    for unit in obs_tables:
        start = obs_tables[unit]['time'][0]
        count = len(obs_tables[unit])
        obs_tables[unit]['time'] = start + np.arange(count) * delta

    # Ensure that no warnings are raised with an appropriate time-step size.
    with warnings.catch_warnings():
        warnings.simplefilter('error')
        time_steps = list(time_scale.with_observation_tables(ctx, obs_tables))
    assert len(time_steps) == 30

    for _ix, time_step, obs_list in time_steps:
        obs_begun = time_step.end >= min(obs_tables[unit]['time'])
        obs_remain = time_step.end <= max(obs_tables[unit]['time'])
        expect_obs = obs_begun and obs_remain
        if expect_obs:
            assert len(obs_list) == 2
        else:
            assert len(obs_list) == 0

        for obs in obs_list:
            assert obs['time'] == time_step.end

        if obs_list:
            assert any(obs['unit'] == 'x' for obs in obs_list)
            assert any(obs['unit'] == 'y' for obs in obs_list)

    remove_example_files()
