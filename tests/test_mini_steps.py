import io
import logging
import numpy as np
import pypfilt
import pytest


class TestModelSetting(pypfilt.model.Model):
    """
    A simple monotonic deterministic process, where the number of mini-steps
    must be defined in the scenario settings.
    """

    def field_types(self, ctx):
        return [('x', np.float64)]

    @pypfilt.model.ministeps()
    def update(self, ctx, time_step, is_fs, prev, curr):
        curr['x'] = prev['x'] + time_step.dt
        logger = logging.getLogger(__name__)
        logger.debug('Incrementing x by {}'.format(time_step.dt))


class TestModelPredef(pypfilt.model.Model):
    """
    A simple monotonic deterministic process, where the number of mini-steps
    is predefined by the decorator.
    """

    def field_types(self, ctx):
        return [('x', np.float64)]

    @pypfilt.model.ministeps(50)
    def update(self, ctx, time_step, is_fs, prev, curr):
        curr['x'] = prev['x'] + time_step.dt
        logger = logging.getLogger(__name__)
        logger.debug('Incrementing x by {}'.format(time_step.dt))


def test_mini_steps_via_decorator(caplog):
    """
    Test that defining mini-steps via the decorator produces the expected
    behaviour and model output.
    """
    caplog.set_level(logging.DEBUG)
    # Run a single estimation pass (without any observations).
    toml_io = io.StringIO(config_str())
    instances = list(pypfilt.load_instances(toml_io))
    assert len(instances) == 1
    instance = instances[0]
    model_name = 'test_mini_steps.TestModelPredef'
    instance.settings['components']['model'] = model_name
    context = instance.build_context()
    results = pypfilt.fit(context, filename=None)
    # Check that the update() method was called once for each mini-step.
    update_logs = [
        rec for rec in caplog.record_tuples if rec[0] == 'test_mini_steps'
    ]
    assert len(update_logs) == 300
    for rec in update_logs:
        assert rec[2] == 'Incrementing x by 0.02'
    # Check that the particle states evolved as expected.
    final_matrix = results.estimation.history.matrix
    svecs = final_matrix['state_vec']['x']
    num_px = svecs.shape[1]
    for px in range(num_px):
        xt = svecs[:, px]
        dx_dt = np.diff(xt)
        assert np.all(np.isclose(dx_dt, 1))


def test_mini_steps_via_settings(caplog):
    """
    Test that defining mini-steps via the scenario settings produces the
    expected behaviour and model output.
    """
    caplog.set_level(logging.DEBUG)
    # Run a single estimation pass (without any observations).
    toml_io = io.StringIO(config_str())
    instances = list(pypfilt.load_instances(toml_io))
    assert len(instances) == 1
    instance = instances[0]
    context = instance.build_context()
    # Define the number of mini-steps.
    context.settings['time']['mini_steps_per_step'] = 100
    results = pypfilt.fit(context, filename=None)
    # Check that the update() method was called once for each mini-step.
    update_logs = [
        rec for rec in caplog.record_tuples if rec[0] == 'test_mini_steps'
    ]
    assert len(update_logs) == 600
    for rec in update_logs:
        assert rec[2] == 'Incrementing x by 0.01'
    # Check that the particle states evolved as expected.
    final_matrix = results.estimation.history.matrix
    svecs = final_matrix['state_vec']['x']
    num_px = svecs.shape[1]
    for px in range(num_px):
        xt = svecs[:, px]
        dx_dt = np.diff(xt)
        assert np.all(np.isclose(dx_dt, 1))

    # Run a second fit with half as many mini-steps, and check that we obtain
    # equivalent particle states.
    instance.settings['time']['mini_steps_per_step'] = 50
    context = instance.build_context()
    results2 = pypfilt.fit(context, filename=None)
    final_matrix2 = results2.estimation.history.matrix
    svecs2 = final_matrix2['state_vec']['x']
    assert np.allclose(svecs, svecs2)

    # Run a second fit with twice as many time-steps, and check that we obtain
    # equivalent particle states.
    instance.settings['time']['mini_steps_per_step'] = 50
    instance.settings['time']['steps_per_unit'] = 2
    context = instance.build_context()
    results3 = pypfilt.fit(context, filename=None)
    final_matrix3 = results3.estimation.history.matrix
    svecs3 = final_matrix3['state_vec']['x']
    assert svecs3.shape != svecs.shape
    assert svecs3[::2].shape == svecs.shape
    assert np.allclose(svecs, svecs3[::2])


def test_mini_steps_undefined(caplog):
    """
    Test that failing to define the number of mini-steps raises an exception.
    """
    caplog.set_level(logging.DEBUG)
    # Run a single estimation pass (without any observations).
    toml_io = io.StringIO(config_str())
    instances = list(pypfilt.load_instances(toml_io))
    assert len(instances) == 1
    instance = instances[0]
    context = instance.build_context()
    with pytest.raises(ValueError, match='Must define setting'):
        pypfilt.fit(context, filename=None)


def config_str():
    """Define a forecast scenario for the TestModelSetting model."""
    return """
    [components]
    model = "test_mini_steps.TestModelSetting"
    time = "pypfilt.Scalar"
    sampler = "pypfilt.sampler.LatinHypercube"
    summary = "pypfilt.summary.HDF5"

    [time]
    start = 0.0
    until = 6.0
    steps_per_unit = 1

    [prior]
    x = { name = "randint", args.low = 0, args.high = 5 }

    [files]
    input_directory = "."
    output_directory = "."
    temp_directory = "."
    delete_cache_file_before_forecast = true
    delete_cache_file_after_forecast = true

    [filter]
    particles = 20
    prng_seed = 3001
    history_window = -1
    resample.threshold = 0.25
    regularisation.enabled = true
    results.save_history = true

    [summary]
    only_forecasts = false
    save_history = true

    [scenario.test]
    name = "Test Scenario"
    """
