"""
Test that ensemble partitions behave as expected.
"""

import io
import numpy as np
import pypfilt
import scipy.stats


class SimpleDet(pypfilt.model.Model):
    """
    A simple monotonic deterministic process.
    """

    def field_types(self, ctx):
        return [('x0', np.float64), ('x', np.float64), ('y', np.float64)]

    def can_smooth(self):
        return {'y'}

    def init(self, ctx, vec):
        vec['x0'][:5] = np.arange(5)
        vec['x0'][5:10] = np.arange(5)
        vec['x0'][10:] = np.tile(np.arange(5), 2)
        vec['x'] = vec['x0']
        vec['y'][:5] = np.linspace(0.4, 0.6, num=5)
        vec['y'][5:10] = np.linspace(0.9, 1.1, num=5)
        vec['y'][10:] = np.linspace(1.4, 1.6, num=10)

    def update(self, ctx, time_step, is_fs, prev, curr):
        curr['x0'] = prev['x0']
        curr['x'] = prev['x'] + time_step.dt * prev['y']
        curr['y'] = prev['y']


class SimpleDetObs(pypfilt.obs.Univariate):
    """
    A simple observation model for x(t).
    """

    def distribution(self, ctx, snapshot):
        expected = snapshot.state_vec['x']
        sdev = self.settings['parameters']['sdev']
        return scipy.stats.norm(loc=expected, scale=sdev)


def generate_obs():
    """
    Generate observations for ``x0 = 2.5`` and ``y = 1.0``.
    """
    fields = [pypfilt.io.time_field('time'), ('value', np.float64)]
    dtype = pypfilt.io.fields_dtype(pypfilt.time.Scalar(), fields)
    values = [
        (1, 3.5),
        (2, 4.5),
        (3, 5.5),
        (4, 6.5),
        (5, 7.5),
    ]
    return np.array(values, dtype=dtype)


def test_partitions_resampling():
    """
    Ensure that partitions are resampled independently and appropriately.
    """
    toml_io = io.StringIO(config_str())
    instances = list(pypfilt.load_instances(toml_io))
    assert len(instances) == 1
    instance = instances[0]

    # Provide the generated observations.
    obs_tables = {'x': generate_obs()}
    context = instance.build_context(obs_tables=obs_tables)

    # Check that the particle states evolve as expected.
    results = pypfilt.fit(context, filename=None)
    final_matrix = results.estimation.history.matrix

    # Extract the partition weights.
    partitions = context.settings['filter']['partition']
    assert len(partitions) == 3
    net_w1 = partitions[0]['weight']
    net_w2 = partitions[1]['weight']
    net_w3 = partitions[2]['weight']

    # Define a slice for each partition, to simplify indexing.
    ixs_1 = partitions[0]['slice']
    ixs_2 = partitions[1]['slice']
    ixs_3 = partitions[2]['slice']

    # Check various properties of the state matrix at each time-step.
    some_resampled = False
    for t in range(len(final_matrix)):
        hist = final_matrix[t]
        weights = hist['weight']
        resampled = hist['resampled']

        # Ensure that the partition weights are preserved.
        assert np.allclose(np.sum(weights[ixs_1]), 0.2)
        assert np.allclose(np.sum(weights[ixs_2]), 0.3)
        assert np.allclose(np.sum(weights[ixs_3]), 0.5)
        assert np.allclose(np.sum(weights), 1.0)

        # Ensure that resampling affects entire partitions.
        assert all(resampled[ixs_1] | ~resampled[ixs_1])
        assert all(resampled[ixs_2] | ~resampled[ixs_2])
        assert all(resampled[ixs_3] | ~resampled[ixs_3])

        # Ensure weights are identical at t=0 and after resampling.
        # Ensure weights are not identical at all other time-steps.
        if t == 0 or all(resampled[ixs_1]):
            assert all(weights[ixs_1] == weights[ixs_1][0])
        else:
            assert not all(weights[ixs_1] == weights[ixs_1][0])
        if t == 0 or all(resampled[ixs_2]):
            assert all(weights[ixs_2] == weights[ixs_2][0])
        else:
            assert not all(weights[ixs_2] == weights[ixs_2][0])
        if t == 0 or all(resampled[ixs_3]):
            assert all(weights[ixs_3] == weights[ixs_3][0])
        else:
            assert not all(weights[ixs_3] == weights[ixs_3][0])

        # Track whether some, but not all, partitions were resampled.
        some_resampled = some_resampled or (
            any(resampled) and not all(resampled)
        )

    # Ensure there was a time-step where some, but not all, partitions were
    # resampled.
    assert some_resampled

    # Inspect the final state of each partition.
    hist = final_matrix[-1]
    weights = hist['weight']
    final_x0 = hist['state_vec']['x0']
    final_y = hist['state_vec']['y']

    # In the first partition, the growth rate y is too small, so we expect it
    # to compensate by selecting a larger initial value x0 in response to the
    # first few observations, and selecting larger values of y.
    assert all(final_x0[ixs_1] >= 3)
    large_y = final_y[ixs_1] >= 0.55
    large_y_weight = np.sum(weights[ixs_1][large_y])
    assert large_y_weight >= 0.95 * net_w1

    # In the second partition, the growth rate y is close to the truth, so we
    # expect it to narrow in on the ground truth for x0 and y.
    assert all(final_x0[ixs_2] >= 2)
    assert all(final_x0[ixs_2] <= 3)
    mid_y = (final_y[ixs_2] >= 0.98) & (final_y[ixs_2] <= 1.05)
    mid_y_weight = np.sum(weights[ixs_2][mid_y])
    assert mid_y_weight >= 0.95 * net_w2

    # In the third partition, the growth rate y is too large, so we expect it
    # to compensate by selecting a smaller initial value x0 in response to the
    # first few observations, and selecting smaller values of y.
    assert all(final_x0[ixs_3] <= 2)
    # NOTE: not all particles will have selected smaller values of y, so we
    # instead check that the bulk of the partition's probability mass is
    # concentrated in those particles where y is small.
    small_y = final_y[ixs_3] < 1.5
    small_y_weight = np.sum(weights[ixs_3][small_y])
    assert small_y_weight >= 0.95 * net_w3


def test_partitions_reservoir_resampling():
    """
    Ensure that a reservoir partition is used as expected.
    """
    toml_io = io.StringIO(config_str())
    instances = list(pypfilt.load_instances(toml_io))
    assert len(instances) == 1
    instance = instances[0]

    # Define two partitions, which will have the following initial states:
    #
    # 1. The reservoir, where y ~ [0.4, 0.6]
    # 2. The ensemble, where y ~ [0.9, 1.1] | [1.4, 1.6]
    partitions = instance.settings['filter']['partition'] = [
        {
            'weight': 0.0,
            'particles': 5,
            'reservoir': True,
        },
        {
            'weight': 1.0,
            'particles': 15,
            'reservoir_partition': 1,
            'reservoir_fraction': 0.2,
        },
    ]

    # Provide the generated observations.
    obs_tables = {'x': generate_obs()}
    context = instance.build_context(obs_tables=obs_tables)

    # Check that the particle states evolve as expected.
    results = pypfilt.fit(context, filename=None)
    model_cints = results.estimation.tables['model_cints']
    final_matrix = results.estimation.history.matrix
    svecs = final_matrix['state_vec']  # shape is (7, 20)

    # Define a slice for each partition, to simplify indexing.
    partitions = context.settings['filter']['partition']
    assert len(partitions) == 2
    ixs_res = partitions[0]['slice']
    ixs_ens = partitions[1]['slice']

    # Check various properties of the state matrix at each time-step.
    some_resampled = False
    for t in range(len(final_matrix)):
        hist = final_matrix[t]
        weights = hist['weight']
        resampled = hist['resampled']

        # Ensure that the reservoir particles have weight zero, and that the
        # remaining particles have net weight 1.0.
        assert all(weights[ixs_res] == 0)
        assert np.allclose(np.sum(weights[ixs_ens]), 1.0)

        # Ensure that the reservoir particles were never resampled and that
        # their initial parameter values are preserved.
        assert not any(resampled[ixs_res])
        assert all(svecs[t]['x0'][ixs_res] == svecs[0]['x0'][ixs_res])
        assert all(svecs[t]['y'][ixs_res] == svecs[0]['y'][ixs_res])

        # Track whether a partition was resampled.
        some_resampled = some_resampled or any(resampled)

        # Check that resampling added three particles from the reservoir.
        if any(resampled):
            curr_svecs = svecs[t][ixs_ens]
            n_res = 3
            # Check that the final three particles are identical to particles
            # in the reservoir.
            # These particles should not have been subject to
            # post-regularisation.
            from_res = curr_svecs[-n_res:]
            assert all(from_res['y'] <= 0.6)
            for i in range(n_res):
                svec = from_res[i]
                assert any(svecs[t][ixs_res] == svec)

            # Check that no other particles in this partition are identical to
            # particles in the ensemble.
            # Any particles previously sampled from the reservoir will have
            # been subject to post-regularisation.
            for i in range(len(curr_svecs) - 3):
                svec = curr_svecs[i]
                assert not any(svecs[t][ixs_res] == svec)

    # Ensure that the non-reservoir partition was resampled at least once.
    assert some_resampled

    # Extract the 100% credible intervals for y(t).
    y_rows = (model_cints['name'] == 'y') & (model_cints['prob'] == 100)
    y_table = model_cints[y_rows]
    y_mins = y_table['ymin']
    # Ensure that the 100% CI for y(t=0) does not include reservoir values.
    assert y_mins[0] >= 0.9
    # Ensure that the 100% CI for y(t>0) does include reservoir values.
    assert all(y_mins[1:] <= 0.6)


def test_partitions_reservoir_no_resample():
    """
    Ensure that a reservoir partition can be preserved, but not contribute to
    resampling other partitions.
    """
    toml_io = io.StringIO(config_str())
    instances = list(pypfilt.load_instances(toml_io))
    assert len(instances) == 1
    instance = instances[0]

    # Define two partitions, which will have the following initial states:
    #
    # 1. The reservoir, where y ~ [0.4, 0.6]
    # 2. The ensemble, where y ~ [0.9, 1.1] | [1.4, 1.6]
    partitions = instance.settings['filter']['partition'] = [
        {
            'weight': 0.1,
            'particles': 5,
            'reservoir': True,
        },
        {
            'weight': 0.9,
            'particles': 15,
        },
    ]

    # Provide the generated observations.
    obs_tables = {'x': generate_obs()}
    context = instance.build_context(obs_tables=obs_tables)

    # Check that the particle states evolve as expected.
    results = pypfilt.fit(context, filename=None)
    model_cints = results.estimation.tables['model_cints']
    final_matrix = results.estimation.history.matrix
    svecs = final_matrix['state_vec']  # shape is (7, 20)

    # Define a slice for each partition, to simplify indexing.
    partitions = context.settings['filter']['partition']
    assert len(partitions) == 2
    ixs_res = partitions[0]['slice']
    ixs_ens = partitions[1]['slice']

    # Check various properties of the state matrix at each time-step.
    some_resampled = False
    for t in range(len(final_matrix)):
        hist = final_matrix[t]
        weights = hist['weight']
        resampled = hist['resampled']

        # Ensure that the partition weights are preserved.
        assert np.allclose(np.sum(weights[ixs_res]), 0.1)
        assert np.allclose(np.sum(weights[ixs_ens]), 0.9)

        # Ensure that the reservoir particles were never resampled and that
        # their initial parameter values are preserved.
        assert not any(resampled[ixs_res])
        assert all(svecs[t]['x0'][ixs_res] == svecs[0]['x0'][ixs_res])
        assert all(svecs[t]['y'][ixs_res] == svecs[0]['y'][ixs_res])

        # Track whether a partition was resampled.
        some_resampled = some_resampled or any(resampled)

        # Check that resampling did not add particles from the reservoir.
        if any(resampled):
            curr_svecs = svecs[t][ixs_ens]
            for row in curr_svecs:
                assert not any(row == svecs[t][ixs_res])

    # Ensure that the non-reservoir partition was resampled at least once.
    assert some_resampled

    # Extract the 100% credible intervals for y(t).
    y_rows = (model_cints['name'] == 'y') & (model_cints['prob'] == 100)
    y_table = model_cints[y_rows]
    y_mins = y_table['ymin']
    # Ensure that the 100% CI for y(t) includes reservoir values.
    assert all(y_mins <= 0.6)


def test_partitions_multiple_reservoirs():
    """
    Ensure that multiple reservoir partitions are used as expected.
    """
    toml_io = io.StringIO(config_str())
    instances = list(pypfilt.load_instances(toml_io))
    assert len(instances) == 1
    instance = instances[0]

    # Define four partitions, which will have the following initial states:
    #
    # 1. The first reservoir (zero weight), where y ~ [0.4, 0.6]
    # 2. The second reservoir (non-zero weight), where y ~ [0.9, 1.1]
    # 3. The first non-reservoir, where y ~ [1.4, 1.6]
    # 3. The second non-reservoir, where y ~ [1.4, 1.6]
    partitions = instance.settings['filter']['partition'] = [
        {
            'weight': 0.0,
            'particles': 5,
            'reservoir': True,
        },
        {
            'weight': 0.1,
            'particles': 5,
            'reservoir': True,
        },
        {
            'weight': 0.45,
            'particles': 5,
            'reservoir_partition': 1,
            'reservoir_fraction': 0.2,
        },
        {
            'weight': 0.45,
            'particles': 5,
            'reservoir_partition': 2,
            'reservoir_fraction': 0.2,
        },
    ]

    # Provide the generated observations.
    obs_tables = {'x': generate_obs()}
    context = instance.build_context(obs_tables=obs_tables)
    num_px = context.particle_count()
    assert num_px == 20

    # Check that the particle states evolve as expected.
    results = pypfilt.fit(context, filename=None)
    model_cints = results.estimation.tables['model_cints']
    part_fx = results.estimation.tables['part_fx']
    part_cints = results.estimation.tables['part_cints']
    final_matrix = results.estimation.history.matrix
    svecs = final_matrix['state_vec']  # shape is (7, 20)

    # Define a slice for each partition, to simplify indexing.
    partitions = context.settings['filter']['partition']
    assert len(partitions) == 4
    ixs_res1 = partitions[0]['slice']
    ixs_res2 = partitions[1]['slice']
    ixs_ens1 = partitions[2]['slice']
    ixs_ens2 = partitions[3]['slice']

    # Define a mask for the reservoir particles.
    mask_res = np.zeros(num_px, dtype=np.bool_)
    mask_res[partitions[0]['slice']] = True
    mask_res[partitions[1]['slice']] = True
    # Define a mask for the weighted particles.
    mask_wpx = np.zeros(num_px, dtype=np.bool_)
    mask_wpx[partitions[1]['slice']] = True
    mask_wpx[partitions[2]['slice']] = True
    mask_wpx[partitions[3]['slice']] = True

    # Check various properties of the state matrix at each time-step.
    some_resampled = False
    for t in range(len(final_matrix)):
        hist = final_matrix[t]
        weights = hist['weight']
        resampled = hist['resampled']

        # Ensure that the zero-weight reservoir particles have weight zero,
        # and that the remaining particles have net weight 1.0.
        assert all(weights[ixs_res1] == 0)
        assert np.allclose(np.sum(weights[mask_wpx]), 1.0)

        # Ensure that reservoir particles were never resampled and that
        # their initial parameter values are preserved.
        assert not any(resampled[mask_res])
        assert all(svecs[t]['x0'][mask_res] == svecs[0]['x0'][mask_res])
        assert all(svecs[t]['y'][mask_res] == svecs[0]['y'][mask_res])

        # Track whether a partition was resampled.
        some_resampled = some_resampled or any(resampled)

        # Resampling partition #3 should add one particle from reservoir #1.
        if all(resampled[ixs_ens1]):
            curr_svecs = svecs[t][ixs_ens1]
            res_svecs = svecs[t][ixs_res1]
            # Ensure the final particle is identical to a reservoir particle.
            assert sum(res_svecs == curr_svecs[-1]) == 1
            # Ensure no other particles are identical to reservoir particles.
            # Any particles previously sampled from the reservoir will have
            # been subject to post-regularisation.
            for row in curr_svecs[:-1]:
                assert not any(row == res_svecs)
            # Ensure no particles come from the other reservoir.
            for row in curr_svecs:
                assert not any(row == svecs[t][ixs_res2])

        # Resampling partition #4 should add one particle from reservoir #2.
        if all(resampled[ixs_ens2]):
            curr_svecs = svecs[t][ixs_ens2]
            res_svecs = svecs[t][ixs_res2]
            # Ensure the final particle is identical to a reservoir particle.
            assert sum(res_svecs == curr_svecs[-1]) == 1
            # Ensure no other particles are identical to reservoir particles.
            # Any particles previously sampled from the reservoir will have
            # been subject to post-regularisation.
            for row in curr_svecs[:-1]:
                assert not any(row == res_svecs)
            # Ensure no particles come from the other reservoir.
            for row in curr_svecs:
                assert not any(row == svecs[t][ixs_res1])

    # Ensure that the non-reservoir partition was resampled at least once.
    assert some_resampled

    # Extract the 100% credible intervals for y(t).
    y_rows = (model_cints['name'] == 'y') & (model_cints['prob'] == 100)
    y_table = model_cints[y_rows][['time', 'ymin', 'ymax']]
    y_mins = y_table['ymin']
    # Ensure the 100% CI for y(t=0) does not include values from the
    # zero-weight reservoir.
    assert y_mins[0] >= 0.9
    # Ensure the 100% CI for y(t=0) includes values from the non-zero-weight
    # reservoir.
    assert y_mins[0] < 1.0
    # Ensure the 100% CI for y(t>0) includes values from the zero-weight
    # reservoir.
    assert all(y_mins[1:] <= 0.6)

    # Now inspect per-partition summary tables.
    part_nums = [1, 2, 3, 4]

    # Inspect the 100% credible intervals for y(t) in each partition.
    assert set(part_cints['partition']) == set(part_nums)
    y_rows = (part_cints['name'] == 'y') & (part_cints['prob'] == 100)
    y_table = part_cints[y_rows][['time', 'partition', 'ymin', 'ymax']]
    y_bounds = {
        part: y_table[y_table['partition'] == part] for part in part_nums
    }

    # Check the first reservoir, where y ~ [0.4, 0.6]
    assert np.allclose(y_bounds[1]['ymin'], 0.4)
    assert np.allclose(y_bounds[1]['ymax'], 0.6)

    # Check the second reservoir, where y ~ [0.9, 1.1]
    assert np.allclose(y_bounds[2]['ymin'], 0.9)
    assert np.allclose(y_bounds[2]['ymax'], 1.1)

    # Check the first non-reservoir, where y ~ [1.4, 1.4888]
    assert np.allclose(y_bounds[3]['ymin'][0], 1.4)
    assert np.all(y_bounds[3]['ymin'][1:] <= 0.6)
    # NOTE: this partition is resampled at t=1 and t=4.
    # The first resampling only applies post-regularisation to the particles
    # that were already in the partition (y ~ [1.4, 1.4888]), but the second
    # resampling applies post-regularisation to those particles *and* the
    # particle from partition #1 that was added at t=1.
    # So the variance in y is greatly increased --- 4 values in [1.4, 1.5] and
    # 1 value in [0.4, 0.6] --- so post-regularisation will increase the
    # maximum value of y.
    assert np.all(y_bounds[3]['ymax'][:4] < 1.5)
    assert np.all(y_bounds[3]['ymax'][4:] > 1.5)

    # Check the second non-reservoir, where y ~ [1.5111, 1.6]
    assert np.all(y_bounds[4]['ymin'][0] >= 1.5)
    assert np.all(y_bounds[4]['ymin'][1:] <= 1.1)
    assert np.allclose(y_bounds[4]['ymax'][0], 1.6)
    assert np.all(y_bounds[4]['ymax'][1:] < 1.6)

    # Inspect the partition credible intervals for x observations.
    # Recall that these are CIs for a Gaussian observation model.
    assert set(part_fx['partition']) == set(part_nums)

    # At time t=0 the CIs should be the same for all partitions,
    # since x = [0, 1, 2, 3, 4] in each partition.
    cmp_cols = ['time', 'prob', 'ymin', 'ymax']
    for part in part_nums[1:]:
        part_rows = part_fx[
            (part_fx['time'] == 0) & (part_fx['partition'] == part)
        ]
        orig_rows = part_fx[
            (part_fx['time'] == 0) & (part_fx['partition'] == part_nums[0])
        ]
        # NOTE: cannot pass both arrays to np.allclose(), but instead need to
        # compare each column separately, due to the different column types.
        # See https://stackoverflow.com/a/41248649 for details.
        for col in cmp_cols:
            assert np.allclose(part_rows[col], orig_rows[col])

    # At times t>0 the CIs for each partition should differ, so we compare the
    # CIs for each pair of partitions at each time.
    for t in range(1, len(final_matrix)):
        for orig in part_nums:
            # Pick an origin partition, to compare against the others.
            orig_rows = part_fx[
                (part_fx['time'] == t) & (part_fx['partition'] == orig)
            ]
            orig_cis = np.stack((orig_rows['ymin'], orig_rows['ymax']))

            # Compare the CIs from each other partition, in turn.
            for part in part_nums:
                if part == orig:
                    continue

                part_rows = part_fx[
                    (part_fx['time'] == t) & (part_fx['partition'] == part)
                ]
                part_cis = np.stack((part_rows['ymin'], part_rows['ymax']))
                assert np.all(
                    part_rows[['time', 'prob']] == orig_rows[['time', 'prob']]
                )
                assert not np.allclose(orig_cis, part_cis)


def test_partitions_prev_ix():
    """
    Ensure that when partitions are resampled, the previous indices are
    recorded correctly.
    """
    toml_io = io.StringIO(config_str())
    instances = list(pypfilt.load_instances(toml_io))
    assert len(instances) == 1
    instance = instances[0]

    # Turn off post-regularisation, so that we can identify particles from
    # their 'x0' and 'y' values.
    instance.settings['filter']['regularisation']['enabled'] = False

    # Define the following three partitions:
    #
    # 1. Non-reservoir partition, where y ~ [0.4, 0.6];
    # 2. Reservoir partition, where y ~ [0.9, 11]; and
    # 3. Non-reservoir partition that uses the reservoir, y ~ [1.4, 1.6].
    partitions = instance.settings['filter']['partition'] = [
        {
            'weight': 0.1,
            'particles': 5,
            'reservoir': False,
        },
        {
            'weight': 0.1,
            'particles': 5,
            'reservoir': True,
        },
        {
            'weight': 0.8,
            'particles': 10,
            'reservoir_partition': 2,
            'reservoir_fraction': 0.2,
        },
    ]

    # Provide the generated observations.
    obs_tables = {'x': generate_obs()}
    context = instance.build_context(obs_tables=obs_tables)

    # Check that the particle states evolve as expected.
    results = pypfilt.fit(context, filename=None)
    final_matrix = results.estimation.history.matrix

    # Extract the partition slices.
    partitions = context.settings['filter']['partition']
    assert len(partitions) == 3
    # Define a slice for each partition, to simplify indexing.
    ixs_1 = partitions[0]['slice']
    ixs_2 = partitions[1]['slice']
    ixs_3 = partitions[2]['slice']

    resampled_1 = False
    resampled_3 = False
    for t in range(len(final_matrix)):
        hist = final_matrix[t]
        resampled = hist['resampled']

        # The reservoir partition should never be resampled.
        assert not np.any(resampled[ixs_2])
        # The previous indices should therefore be constant.
        assert np.array_equal(hist['prev_ix'][ixs_2], np.arange(5, 10))

        # When the first partition is resampled, all indices should be between
        # zero and five.
        if np.all(resampled[ixs_1]):
            resampled_1 = True
            assert np.all(hist['prev_ix'][ixs_1] < 5) & np.all(
                hist['prev_ix'][ixs_1] >= 0
            )

        # When the third partition is resampled, the first eight particles
        # should have been sampled from the third partition, and the final two
        # particles should have been sampled from the reservoir.
        if np.all(resampled[ixs_3]):
            resampled_3 = True
            assert np.all(hist['prev_ix'][ixs_3][:8] < 20) & np.all(
                hist['prev_ix'][ixs_3][:8] >= 10
            )
            assert np.all(hist['prev_ix'][ixs_3][8:] < 10) & np.all(
                hist['prev_ix'][ixs_3][8:] >= 5
            )

        # Ensure that the previous particles have identical parameters to the
        # current particles.
        if t > 1:
            prev_ixs = hist['prev_ix']
            curr_vec = hist['state_vec']
            prev_vec = final_matrix[t - 1]['state_vec'][prev_ixs]
            assert np.array_equal(
                curr_vec[['x0', 'y']], prev_vec[['x0', 'y']]
            )

    # Ensure that each non-reservoir partition was resampled at least once.
    assert resampled_1
    assert resampled_3


def config_str():
    """Define a forecast scenario for the SimpleDet model."""
    return """
    [components]
    model = "test_partitions.SimpleDet"
    time = "pypfilt.Scalar"
    sampler = "pypfilt.sampler.LatinHypercube"
    summary = "pypfilt.summary.HDF5"

    [time]
    start = 0.0
    until = 6.0
    steps_per_unit = 1

    [files]
    input_directory = "."
    output_directory = "."
    temp_directory = "."
    delete_cache_file_before_forecast = true
    delete_cache_file_after_forecast = true

    [filter]
    particles = 20
    prng_seed = 3001
    history_window = -1
    resample.threshold = 0.5
    regularisation.enabled = true
    results.save_history = true

    [filter.regularisation.bounds]
    y = { min = 0, max = 2 }

    [[filter.partition]]
    weight = 0.2
    particles = 5

    [[filter.partition]]
    weight = 0.3
    particles = 5

    [[filter.partition]]
    weight = 0.5
    particles = 10

    [summary]
    only_forecasts = false
    save_history = true

    [summary.tables]
    model_cints.component = "pypfilt.summary.ModelCIs"
    model_cints.credible_intervals = [ 50, 100 ]
    part_fx.component = "pypfilt.summary.PartitionPredictiveCIs"
    part_fx.credible_intervals = [ 0, 50, 95 ]
    part_cints.component = "pypfilt.summary.PartitionModelCIs"
    part_cints.credible_intervals = [ 50, 100 ]

    [observations.x]
    model = "test_partitions.SimpleDetObs"
    parameters.sdev = 0.5

    [scenario.test]
    name = "Test Scenario"
    """
