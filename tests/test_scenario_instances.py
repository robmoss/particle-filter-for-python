"""
Test that scenario instances are generated as expected from TOML content.
"""

import io
import pypfilt.scenario
import pytest


def test_scenario_empty_toml():
    """
    Test that a ValueError is raised when no scenarios are defined.
    """
    toml_input = """
    """
    source = io.StringIO(toml_input)
    with pytest.raises(ValueError, match='No scenarios defined'):
        _ = list(pypfilt.scenario.load_instances(source))


def test_scenario_single_instance():
    """
    Test that we obtain a single instance from this minimal TOML input.
    """
    toml_input = """
    global = true
    hello = "world"

    [scenario.test]
    global = false
    local = true
    """
    source = io.StringIO(toml_input)
    instances = list(pypfilt.scenario.load_instances(source))
    assert len(instances) == 1

    # Check that global and scenario-specific parameters have been applied.
    instance = instances[0]
    assert instance.scenario_id == 'test'
    assert instance.descriptor == ''

    assert len(instance.settings) == 3
    assert 'global' in instance.settings
    assert instance.settings['global'] is False
    assert 'hello' in instance.settings
    assert instance.settings['hello'] == 'world'
    assert 'local' in instance.settings
    assert instance.settings['local'] is True


def test_scenario_many_observation_models():
    """
    Test that we obtain multiple instances for a single scenario.
    """
    toml_input = """

    [scenario.test]

    [scenario.test.observations.x]
    parameters.bg_obs = 1
    parameters.pr_obs = [0.1, 0.2, 0.5]
    parameters.disp = 10
    descriptor.format = { bg_obs = "03.0f", pr_obs = "0.1f", disp = "03.0f" }
    descriptor.name = { bg_obs = "bg", pr_obs = "pr", disp = "disp" }

    [scenario.test.observations.y]
    parameters.bg_obs = 2
    parameters.pr_obs = 0.8
    parameters.disp = [100, 1000]
    descriptor.format = { bg_obs = "03.0f", pr_obs = "0.1f", disp = "04.0f" }
    descriptor.name = { bg_obs = "bg", pr_obs = "pr", disp = "disp" }
    """
    source = io.StringIO(toml_input)
    instances = list(pypfilt.scenario.load_instances(source))
    assert len(instances) == 6

    # Check that each instance has the correct scenario ID.
    assert all(inst.scenario_id == 'test' for inst in instances)

    # Check that each instance descriptor is unique.
    descriptors = set(inst.descriptor for inst in instances)
    assert len(descriptors) == len(instances)

    # Check that we have the expected number of instances for each of the
    # x and y observation model parameters.

    # Check the 'bg_obs' values are constant for each observation model.
    assert all(
        inst.settings['observations']['x']['parameters']['bg_obs'] == 1
        for inst in instances
    )
    assert all(
        inst.settings['observations']['y']['parameters']['bg_obs'] == 2
        for inst in instances
    )

    # Check the 'disp' values vary as expected.
    x_disp_10 = [
        inst
        for inst in instances
        if inst.settings['observations']['x']['parameters']['disp'] == 10
    ]
    assert len(x_disp_10) == len(instances)

    y_disp_100 = [
        inst
        for inst in instances
        if inst.settings['observations']['y']['parameters']['disp'] == 100
    ]
    assert len(y_disp_100) == 3

    y_disp_1000 = [
        inst
        for inst in instances
        if inst.settings['observations']['y']['parameters']['disp'] == 1000
    ]
    assert len(y_disp_1000) == 3

    # Check the 'pr_obs' values vary as expected.
    y_pr_08 = [
        inst
        for inst in instances
        if inst.settings['observations']['y']['parameters']['pr_obs'] == 0.8
    ]
    assert len(y_pr_08) == 6

    x_pr_01 = [
        inst
        for inst in instances
        if inst.settings['observations']['x']['parameters']['pr_obs'] == 0.1
    ]
    assert len(x_pr_01) == 2

    x_pr_02 = [
        inst
        for inst in instances
        if inst.settings['observations']['x']['parameters']['pr_obs'] == 0.2
    ]
    assert len(x_pr_02) == 2

    x_pr_05 = [
        inst
        for inst in instances
        if inst.settings['observations']['x']['parameters']['pr_obs'] == 0.5
    ]
    assert len(x_pr_05) == 2


def test_scenario_multiple_scenarios():
    """
    Test that we obtain a single instance for each scenario.
    """
    toml_input = """
    global = { foo = "hello", extra = "hi" }
    default = 1
    hello = "world"

    [scenario.foo]
    global = { foo = "goodbye" }
    default = { a = "yes", b = "no" }
    local = true

    [scenario.bar]
    global = { bar = "world" }
    local = true
    """
    source = io.StringIO(toml_input)
    instances = list(pypfilt.scenario.load_instances(source))
    assert len(instances) == 2

    foos = list(filter(lambda i: i.scenario_id == 'foo', instances))
    bars = list(filter(lambda i: i.scenario_id == 'bar', instances))
    assert len(foos) == 1
    assert len(bars) == 1
    foo = foos[0]
    bar = bars[0]

    # Check that global and scenario-specific parameters have been applied.
    assert foo.descriptor == ''
    assert bar.descriptor == ''

    assert len(foo.settings) == 4
    assert len(bar.settings) == 4
    assert foo.settings['hello'] == 'world'
    assert bar.settings['hello'] == 'world'
    assert foo.settings['default'] == {'a': 'yes', 'b': 'no'}
    assert bar.settings['default'] == 1
    assert foo.settings['local'] is True
    assert bar.settings['local'] is True
    assert foo.settings['global'] == {'foo': 'goodbye', 'extra': 'hi'}
    assert bar.settings['global'] == {
        'foo': 'hello',
        'bar': 'world',
        'extra': 'hi',
    }


def test_scenario_multiple_sources():
    """
    Test that we obtain instances from each source.
    """
    toml_input_a = """
    [scenario.foo]
    local = true
    """

    toml_input_b = """
    [scenario.bar]
    local = true
    """
    source_a = io.StringIO(toml_input_a)
    source_b = io.StringIO(toml_input_b)
    sources = [source_a, source_b]
    instances = list(pypfilt.scenario.load_instances(sources))
    assert len(instances) == 2

    foos = list(filter(lambda i: i.scenario_id == 'foo', instances))
    bars = list(filter(lambda i: i.scenario_id == 'bar', instances))
    assert len(foos) == 1
    assert len(bars) == 1
    foo = foos[0]
    bar = bars[0]

    # Check that global and scenario-specific parameters have been applied.
    assert foo.descriptor == ''
    assert bar.descriptor == ''

    assert len(foo.settings) == 1
    assert len(bar.settings) == 1
    assert foo.settings['local'] is True
    assert bar.settings['local'] is True


def test_scenario_many_scenarios_instances():
    """
    Test that we obtain multiple instances for each scenario.
    """
    toml_input = """

    [observations.x]
    parameters.bg_obs = 1
    parameters.pr_obs = [0.1, 0.2, 0.5]
    parameters.disp = 10
    descriptor.format = { bg_obs = "03.0f", pr_obs = "0.1f", disp = "03.0f" }
    descriptor.name = { bg_obs = "bg", pr_obs = "pr", disp = "disp" }

    [scenario.single]

    [scenario.multi]

    [scenario.multi.observations.y]
    parameters.bg_obs = 2
    parameters.pr_obs = 0.8
    parameters.disp = [100, 1000]
    descriptor.format = { bg_obs = "03.0f", pr_obs = "0.1f", disp = "04.0f" }
    descriptor.name = { bg_obs = "bg", pr_obs = "pr", disp = "disp" }

    """
    source = io.StringIO(toml_input)
    instances = list(pypfilt.scenario.load_instances(source))
    assert len(instances) == 9

    singles = list(filter(lambda i: i.scenario_id == 'single', instances))
    multis = list(filter(lambda i: i.scenario_id == 'multi', instances))
    assert len(singles) == 3
    assert len(multis) == 6

    for inst in singles:
        assert 'x' in inst.settings['observations']
        assert len(inst.settings['observations']) == 1

    for inst in multis:
        assert 'x' in inst.settings['observations']
        assert 'y' in inst.settings['observations']
        assert len(inst.settings['observations']) == 2

    # Check the 'pr_obs' values for 'x' vary as expected.
    for x_pr in [0.1, 0.2, 0.5]:
        single_matches = [
            inst
            for inst in singles
            if inst.settings['observations']['x']['parameters']['pr_obs']
            == x_pr
        ]
        assert len(single_matches) == 1

        multi_matches = [
            inst
            for inst in multis
            if inst.settings['observations']['x']['parameters']['pr_obs']
            == x_pr
        ]
        assert len(multi_matches) == 2

    # Check the 'disp' values for 'y' vary as expected.
    for y_disp in [100, 1000]:
        multi_matches = [
            inst
            for inst in multis
            if inst.settings['observations']['y']['parameters']['disp']
            == y_disp
        ]
        assert len(multi_matches) == 3


def test_scenario_no_descriptor_one_instance():
    """
    Test that we obtain a single instance with an empty descriptor string when
    no descriptor table is provided.
    """
    toml_input = """
    [observations.counts]
    parameters.scale = 1

    [scenario.test]
    name = "Test Scenario"
    """
    source = io.StringIO(toml_input)
    instances = list(pypfilt.scenario.load_instances(source))
    assert len(instances) == 1
    instance = instances[0]
    assert instance.scenario_id == 'test'
    assert instance.descriptor == ''
    obs_model = instance.settings['observations']['counts']
    assert obs_model['parameters']['scale'] == 1


def test_scenario_no_descriptor_two_instances():
    """
    Test that an exception is raised when no descriptor table is provided and
    a scenario defines more than one instance.
    """
    toml_input = """
    [observations.counts]
    parameters.scale = [1, 2]

    [scenario.test]
    name = "Test Scenario"
    """
    source = io.StringIO(toml_input)
    with pytest.raises(ValueError, match='has a duplicate descriptor'):
        _ = list(pypfilt.scenario.load_instances(source))
