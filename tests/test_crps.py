import numpy as np
import pypfilt.crps
import pypfilt.io
import pytest
import scipy.stats


def crps_unif(y):
    """
    Calculate CRPS for a continuous uniform distribution over the unit
    interval.
    """
    z = np.clip(y, 0, 1)
    return np.abs(y - z) + z**2 - z + 1 / 3


def test_crps_uniform_dist():
    """
    Test that CRPS values approach the exact value as the number of samples
    increases.
    """
    truth = 0.8
    crps_exact = crps_unif(truth)
    dist = scipy.stats.uniform()
    crps_errs = []
    for size in [100, 1000, 10000, 100000]:
        rng = np.random.default_rng(seed=12345)
        samples = dist.rvs(size=size, random_state=rng)
        crps_est = pypfilt.crps.crps_edf_scalar(truth, samples)
        crps_errs.append(np.abs(crps_est - crps_exact) / crps_exact)
    crps_errs = np.array(crps_errs)
    assert all(crps_errs >= 0)
    # Ensure that errors decrease as sample size grows.
    assert all(np.diff(crps_errs) < 0)
    # Check upper bounds as sample size grows.
    assert all(crps_errs < 0.5)
    assert all(crps_errs[1:] < 0.1)
    assert all(crps_errs[2:] < 0.05)
    assert all(crps_errs[3:] < 0.01)


def crps_norm(mean, sd, truth):
    """
    Calculate CRPS for a normal distribution.

    The formula comes from
    `Gneiting et al., 2005 <https://doi.org/10.1175/MWR2904.1>`.
    """
    y = (truth - mean) / sd
    unit_norm = scipy.stats.norm(loc=0, scale=1)
    return sd * (
        y * (2 * unit_norm.cdf(y) - 1)
        + 2 * unit_norm.pdf(y)
        - 1 / np.sqrt(np.pi)
    )


def test_crps_normal_dist():
    """
    Test that CRPS values approach the exact value as the number of samples
    increases.
    """
    mean = 100
    sd = 10
    y = 105
    crps_exact = crps_norm(mean, sd, y)
    dist = scipy.stats.norm(loc=mean, scale=sd)
    crps_errs = []
    for size in [10, 100, 1000, 10000]:
        rng = np.random.default_rng(seed=12345)
        samples = dist.rvs(size=size, random_state=rng)
        crps_est = pypfilt.crps.crps_edf_scalar(y, samples)
        crps_errs.append(np.abs(crps_est - crps_exact) / crps_exact)
    crps_errs = np.array(crps_errs)
    assert all(crps_errs >= 0)
    # Ensure that errors decrease as sample size grows.
    assert all(np.diff(crps_errs) < 0)
    # Check upper bounds as sample size grows.
    assert all(crps_errs < 1)
    assert all(crps_errs[1:] < 0.1)
    assert all(crps_errs[2:] < 0.05)
    assert all(crps_errs[3:] < 0.01)


def test_crps_simple():
    """
    Test that we obtain expected scores for a small number of samples, where
    we can calculate the scores by hand.
    """
    x1 = np.array([50, 51, 53, 49, 48, 50])
    x2 = np.array([72, 73, 78, 71, 68, 72])
    y1 = 50
    y2 = 75
    dat = np.vstack((x1, x2))
    y = np.array([y1, y2])
    crps_values = pypfilt.crps.crps_sample(y, dat)
    expected_values = np.array([0.30555556, 2.11111111])
    assert np.allclose(crps_values, expected_values)


def test_crps_normal_errors():
    """
    Test the relative values of CRPS scores for samples drawn from different
    normal distributions: biased or unbiased, and narrow or wide.
    """
    seed = 12345
    rnd = np.random.default_rng(seed=seed)
    samples = 100
    truth = 42
    bias = 2.5
    narrow_sd = 0.1
    wide_sd = 2.0
    unbiased_narrow = rnd.normal(loc=truth, scale=narrow_sd, size=samples)
    unbiased_wide = rnd.normal(loc=truth, scale=wide_sd, size=samples)
    biased_narrow = rnd.normal(
        loc=truth + bias, scale=narrow_sd, size=samples
    )
    biased_wide = rnd.normal(loc=truth + bias, scale=wide_sd, size=samples)
    score_ubn = pypfilt.crps.crps_edf_scalar(truth, unbiased_narrow)
    score_ubw = pypfilt.crps.crps_edf_scalar(truth, unbiased_wide)
    score_bn = pypfilt.crps.crps_edf_scalar(truth, biased_narrow)
    score_bw = pypfilt.crps.crps_edf_scalar(truth, biased_wide)
    assert 0 < score_ubn < score_ubw < score_bw
    assert 0 < score_ubn < score_ubw < score_bn

    # Check that we obtain the same CRPS scores from crps_sample().
    true_values = np.array([truth, truth, truth, truth])
    samples_table = np.stack(
        (unbiased_narrow, unbiased_wide, biased_narrow, biased_wide)
    )
    scores = pypfilt.crps.crps_sample(true_values, samples_table)
    assert np.array_equal(scores, [score_ubn, score_ubw, score_bn, score_bw])

    # Check that we obtain the same CRPS scores from simulated_obs_crps().
    truth_table = np.array(
        [(n, truth) for n in range(4)],
        dtype=[('time', np.float64), ('value', np.float64)],
    )
    sim_times = np.repeat(range(4), samples)
    sim_fstime = np.repeat(0.0, len(sim_times))
    sim_values = np.concatenate(
        (unbiased_narrow, unbiased_wide, biased_narrow, biased_wide)
    )
    sim_table = np.array(
        list(zip(sim_fstime, sim_times, sim_values)),
        dtype=[(col, np.float64) for col in ['fs_time', 'time', 'value']],
    )

    scores_table = pypfilt.crps.simulated_obs_crps(truth_table, sim_table)
    assert np.array_equal(scores_table['score'], scores)


def test_crps_invalid_samples():
    """
    Test that crps_sample() raises exceptions for invalid inputs.
    """
    x1 = np.array([50, 51, 53, 49, 48, 50])
    x2 = np.array([72, 73, 78, 71, 68, 72])
    y1 = 50
    y2 = 75
    dat = np.vstack((x1, x2))
    y = np.array([y1, y2])
    dat_bad_shape = dat.transpose()
    dat_bad_dims = np.concatenate((x1, x2))
    y_bad = np.array([[y1, y2]])

    with pytest.raises(ValueError, match='must be a 1-D array'):
        pypfilt.crps.crps_sample(y_bad, dat)

    with pytest.raises(ValueError, match='incompatible dimensions'):
        pypfilt.crps.crps_sample(y, dat_bad_shape)

    with pytest.raises(ValueError, match='must be a 2-D array'):
        pypfilt.crps.crps_sample(y, dat_bad_dims)


def test_crps_invalid_tables():
    """
    Test that simulated_obs_crps() raises exceptions for invalid inputs.
    """
    # Valid inputs.
    valid_truth = np.array(
        [(0.0, 10.0), (1.0, 20.0), (2.0, 30.0)],
        dtype=[('time', np.float64), ('value', np.float64)],
    )
    valid_sim = np.array(
        [(0.0, 0.0, 11.0), (0.0, 1.0, 12.0)],
        dtype=[
            ('fs_time', np.float64),
            ('time', np.float64),
            ('value', np.float64),
        ],
    )

    # Invalid field names.
    invalid_truth = np.array(
        [(0.0, 10.0), (1.0, 20.0), (2.0, 30.0)],
        dtype=[('not_time', np.float64), ('count', np.float64)],
    )
    # Multiple observations for the same time.
    duplicate_truth = np.array(
        [(0.0, 10.0), (0.0, 20.0), (2.0, 30.0)],
        dtype=[('time', np.float64), ('value', np.float64)],
    )

    # Invalid field names.
    invalid_sim_1 = np.array(
        [(0.0, 0.0, 11.0), (0.0, 1.0, 12.0)],
        dtype=[
            ('not_fs_time', np.float64),
            ('time', np.float64),
            ('value', np.float64),
        ],
    )
    invalid_sim_2 = np.array(
        [(0.0, 0.0, 11.0), (0.0, 1.0, 12.0)],
        dtype=[
            ('fs_time', np.float64),
            ('not_time', np.float64),
            ('value', np.float64),
        ],
    )
    invalid_sim_3 = np.array(
        [(0.0, 0.0, 11.0), (0.0, 1.0, 12.0)],
        dtype=[
            ('fs_time', np.float64),
            ('time', np.float64),
            ('count', np.float64),
        ],
    )

    with pytest.raises(ValueError, match='Column ".*" not found'):
        pypfilt.crps.simulated_obs_crps(invalid_truth, valid_sim)

    with pytest.raises(ValueError, match='Found .* true values'):
        pypfilt.crps.simulated_obs_crps(duplicate_truth, valid_sim)

    for sim in [invalid_sim_1, invalid_sim_2, invalid_sim_3]:
        with pytest.raises(ValueError, match='Column ".*" not found'):
            pypfilt.crps.simulated_obs_crps(valid_truth, sim)
