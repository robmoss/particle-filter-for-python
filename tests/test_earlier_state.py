"""
Test the behaviour of ``pypfilt.state.earlier_state()``.
"""

import numpy as np
import pypfilt.state


def test_earlier_state():
    dtype = [
        ('prev_ix', np.int64),
        ('resampled', np.bool_),
        ('id', np.float64),
    ]
    num_steps = 6
    num_px = 4
    same_order = np.arange(num_px)
    reverse_order = same_order[::-1]
    hist = np.zeros(shape=(num_steps, num_px), dtype=dtype)

    # No resampling, particles remain in the same order.
    hist['prev_ix'][:, :] = same_order[None, :]
    hist['id'][:, :] = same_order[None, :]

    for ix in [1, 3, 5]:
        for steps_back in [0, 1, 2, 3]:
            prev_state = pypfilt.state.earlier_states(hist, ix, steps_back)
            assert np.array_equal(prev_state['id'], same_order)

    # Reverse the particle ordering at step 2.
    resample_ix = 2
    hist['resampled'][resample_ix, :] = True
    hist['prev_ix'][resample_ix, :] = reverse_order
    hist['id'][resample_ix:, :] = reverse_order[None, :]

    # Starting at all time-steps prior to the resampling event, the particles
    # should be returned in their original order.
    for ix in range(resample_ix):
        for steps_back in range(num_steps):
            state = pypfilt.state.earlier_states(hist, ix, steps_back)
            ids = state['id']
            assert np.array_equal(ids, same_order)

    # Starting at all time-steps from the resampling event, the particles
    # should be returned in reverse order.
    for ix in range(2, num_steps):
        for steps_back in range(num_steps):
            state = pypfilt.state.earlier_states(hist, ix, steps_back)
            ids = state['id']
            assert np.array_equal(ids, reverse_order)


def test_snapshot_slices():
    """
    Test that slicing snapshots returns the expected subset of particles, and
    the expected subset of previous particle states.
    """
    dtype = [
        ('prev_ix', np.int64),
        ('resampled', np.bool_),
        ('state_vec', np.float64),
        ('weight', np.float64),
    ]
    num_steps = 6
    steps_per_unit = 1
    num_px = 4
    same_order = np.arange(num_px)
    reverse_order = same_order[::-1]
    subset_size = 2
    subset_slice = slice(1, 1 + subset_size)
    hist = np.zeros(shape=(num_steps, num_px), dtype=dtype)
    hist['weight'][:, :] = 1 / num_px

    # No resampling, particles remain in the same order.
    hist['prev_ix'][:, :] = same_order[None, :]
    hist['state_vec'][:, :] = same_order[None, :]

    for ix in [1, 3, 5]:
        snapshot = pypfilt.state.Snapshot(ix, steps_per_unit, hist, ix)
        assert len(snapshot) == num_px
        subset = snapshot[subset_slice]
        assert len(subset) == subset_size

        for steps_back in [0, 1, 2, 3]:
            prev_state = snapshot.back_n_steps(steps_back)
            assert prev_state.shape == (num_px,)
            assert np.array_equal(prev_state['state_vec'], same_order)

            prev_subset = subset.back_n_steps(steps_back)
            assert prev_subset.shape == (subset_size,)
            assert np.array_equal(
                prev_subset['state_vec'], same_order[subset_slice]
            )

    # Reverse the particle ordering at step 2.
    resample_ix = 2
    hist['resampled'][resample_ix, :] = True
    hist['prev_ix'][resample_ix, :] = reverse_order
    hist['state_vec'][resample_ix:, :] = reverse_order[None, :]

    # Starting at all time-steps prior to the resampling event, the particles
    # should be returned in their original order.
    for ix in range(resample_ix):
        snapshot = pypfilt.state.Snapshot(ix, steps_per_unit, hist, ix)
        assert len(snapshot) == num_px
        subset = snapshot[subset_slice]
        assert len(subset) == subset_size

        for steps_back in range(num_steps):
            prev_state = snapshot.back_n_steps(steps_back)
            ids = prev_state['state_vec']
            assert np.array_equal(ids, same_order)

            prev_subset = subset.back_n_steps(steps_back)
            ids = prev_subset['state_vec']
            assert np.array_equal(ids, same_order[subset_slice])

    # Starting at all time-steps from the resampling event, the particles
    # should be returned in reverse order.
    for ix in range(2, num_steps):
        snapshot = pypfilt.state.Snapshot(ix, steps_per_unit, hist, ix)
        assert len(snapshot) == num_px
        subset = snapshot[subset_slice]
        assert len(subset) == subset_size

        for steps_back in range(num_steps):
            prev_state = snapshot.back_n_steps(steps_back)
            ids = prev_state['state_vec']
            assert np.array_equal(ids, reverse_order)

            prev_subset = subset.back_n_steps(steps_back)
            ids = prev_subset['state_vec']
            assert np.array_equal(ids, reverse_order[subset_slice])
