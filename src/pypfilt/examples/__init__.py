from . import lorenz  # noqa: F401
from . import predation  # noqa: F401
from . import simple  # noqa: F401
from . import sir  # noqa: F401
from . import sirv  # noqa: F401
