.. _concepts:

Key Concepts
============

**TODO:** provide high-level explanations of the various components (e.g.,
simulation models, observation models, lookup tables and where they are
stored, summary tables) and how they all fit together.

Scenario definitions
--------------------

High-level overview of the TOML, how we allow for inheritance and overriding
on a per-scenario basis.

Simulation models
-----------------

Prior distributions
-------------------

Observations
------------

Observation models
------------------

Particle filter
---------------

**TODO:** explain how the particle filter combines all of the above to
estimate the posterior distribution.

.. _concept_events:

Particle filter events
----------------------

The particle filter triggers events as particles are updated, reweighted, and resampled.
Other components, such as simulation models and observation models, can respond to these events by registering event-handlers with the simulation context (see :func:`~pypfilt.build.Context.install_event_handler`).

During each time-step, the particle filter may trigger one or more of the following events, which are listed **in the order that they will occur**:

* ``'LogLikelihood'``
   This event is triggered when the likelihood of an observation has been calculated for each particle.
   It is triggered separately for each observation, so it will be triggered multiple times in a single time-step if there is more than one observation for that time-step.
   The event-handler receives a :class:`~pypfilt.event.LogLikelihood` argument.

* ``'AfterReweight'``
   This event is triggered after particle weights have been updated in response to one or more observations, but before particles are resampled (if required).
   The event-handler receives an :class:`~pypfilt.event.AfterReweight` argument.

* ``'BeforeResample'``
   This event is triggered before particles are resampled, but after their weights have been updated.
   It is triggered separately for each partition in the ensemble that requires resampling (if any).
   The event-handler receives a :class:`~pypfilt.event.BeforeResample` argument.

* ``'BeforeRegularisation'``
   This event is triggered before post-regularisation is applied, and only if post-regularisation is enabled.
   It is triggered separately for each partition in the ensemble that requires resampling (if any).
   The event-handler receives a :class:`~pypfilt.event.BeforeRegularisation` argument.

* ``'AfterRegularisation'``
   This event is triggered after post-regularisation is applied, and only if post-regularisation is enabled.
   It is triggered separately for each partition in the ensemble that requires resampling (if any).
   The event-handler receives an :class:`~pypfilt.event.AfterRegularisation` argument.

* ``'AfterResample'``
   This event is triggered after particles are resampled (including post-regularisation, if enabled).
   It is triggered separately for each partition in the ensemble that requires resampling (if any).
   The event-handler receives an :class:`~pypfilt.event.AfterResample` argument.

* ``'AfterTimeStep'``
   This event is triggered after the completion of each time-step, including updating particle weights and/or resampling as required.
   The event-handler receives an :class:`~pypfilt.event.AfterTimeStep` argument.

You can register event-handler functions with :func:`Context.install_event_handler() <pypfilt.build.Context.install_event_handler>` to be notified when these events occur.
For example, you could define the following event-handler to inspect the particle states after each time-step:

.. code-block:: python

   >>> import pypfilt
   >>> import pypfilt.examples.predation
   >>> def inspect_particles(event):
   ...     # Print a message to confirm that the event-handler is called.
   ...     if event.snapshot.time == event.ctx.end_time():
   ...         print('Reached the end')
   ...     # TODO: inspect the particle state vectors
   ...     state_vecs = event.snapshot.state_vec
   >>> pypfilt.examples.predation.write_example_files()
   >>> for instance in pypfilt.load_instances('predation.toml'):
   ...     context = instance.build_context()
   ...     context.install_event_handler('AfterTimeStep', inspect_particles)
   ...     results = pypfilt.fit(context, filename=None)
   Reached the end
   >>> # Remove the example files when they are no longer needed.
   >>> pypfilt.examples.predation.remove_example_files()

Time scales
-----------

Lookup (input) tables
---------------------

Summary (output) tables
-----------------------

Summary monitors
----------------

Particle filter settings
------------------------

Other simulation settings
-------------------------

Simulation contexts
-------------------

For every estimation and forecasting simulation, pypfilt_ builds a :class:`simulation context <pypfilt.build.Context>` from the :class:`scenario instance <pypfilt.scenario.Instance>`.
This context object contains all of the components, data tables, and scenario settings that define the simulation.

Common uses of a simulation context ``ctx`` include:

* Retrieving the unique scenario ID:

  .. code-block:: python

     print('The scenario ID is {}'.format(ctx.scenario_id))

* Retrieving a scenario setting with :meth:`ctx.get_setting() <pypfilt.build.Context.get_setting>`:

  .. code-block:: python

     particle_count = ctx.get_setting(['filter', 'particles'])
     sim_start = ctx.get_setting(['time', 'sim_start'])

* Retrieving sample values from the model prior distribution:

  .. code-block:: python

     x_init = ctx.data['prior']['x']

* Retrieving the simulation model component:

  .. code-block:: python

     model = ctx.component['model']

* Retrieving the simulation time-scale component:

  .. code-block:: python

     time_scale = ctx.component['time']

* Retrieving the simulation model PRNG:

  .. code-block:: python

     rnd = ctx.component['random']['model']

* Retrieving the observation model for some observation unit ``obs_unit``:

  .. code-block:: python

     obs_model = ctx.component['obs'][obs_unit]

* Retrieving time-varying inputs :ref:`from a lookup table <howto_using_lookup_tables>`:

  .. code-block:: python

     alpha = ctx.component['lookup']['alpha_table'].lookup(time)

* Adding and calling :meth:`event handlers <pypfilt.build.Context.call_event_handlers>`.


Generating reproducible results
-------------------------------

Plotting
--------

HDF5
----
