Editable installs
=================

Editable installs are supported with `pip >= 21.3 <https://pip.pypa.io/en/latest/news/#v21-3>`__.
Clone the pypfilt_ repository and install into a virtual environment:

.. code-block:: shell

   git clone https://bitbucket.org/robmoss/particle-filter-for-python.git
   python3 -m venv pypfilt_venv
   . ./pypfilt_venv/bin/activate
   pip install -e particle-filter-for-python/
