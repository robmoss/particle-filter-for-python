.. _development:

Development
===========

These page contain information about how pypfilt_ is developed, and how you can contribute to this project.

.. toctree::
   :maxdepth: 2
   :hidden:

   self
   contributing
   testing
   release-process
   unicode
   editable-installs
   changelog
