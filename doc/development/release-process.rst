Release process
===============

Feature development takes places on the "master" branch.
Periodically, a release is created by increasing the version number and
tagging the relevant commit with the new version number.

* Check that the release passes all of the tests:

  .. code-block:: shell

     nox

* Update the version number according to the
  `versioning scheme <https://www.python.org/dev/peps/pep-0440/>`__.

  * Update the version number in ``doc/conf.py``.
    The full version must **always** be updated, the short (X.Y) version **does not** need to be updated if the version number is being increased from X.Y.Z to X.Y.Z+1.

  * Update the version number in ``pyproject.toml``.

* Check whether the copyright year(s) need to be updated in:

  * ``LICENSE``

  * ``doc/conf.py``

* Describe the changes at the top of ``NEWS.rst`` under a heading of the form
  ``X.Y.Z (YYYY-MM-DD)``, which identifies the new version number and the
  date on which this version was released.

* Commit these changes; set the commit message to ``Release pypfilt X.Y.Z``.

  .. code-block:: shell

     git add LICENSE NEWS.rst doc/conf.py pyproject.toml
     git commit -m "Release pypfilt X.Y.Z"

* Tag this commit ``X.Y.Z``.

  .. code-block:: shell

     git tag -a X.Y.Z -m "pypfilt X.Y.Z"

* Push this commit **and** the new tag upstream.

  .. code-block:: shell

     git push --follow-tags

Publishing to PyPI
------------------

These instructions are based on the
`Packaging Python Projects tutorial <https://packaging.python.org/en/latest/tutorials/packaging-projects/>`__.

Ensure that all uncommitted changes are stashed, **or they will be packaged!**

.. code-block:: shell

   git stash

Build the wheel ``./.nox-build/pypfilt-X.Y.Z-py3-none-any.whl``:

.. code-block:: shell

   nox -s build

Upload this wheel to the PyPI **test** server, so that any problems can be
`identified <https://testpypi.python.org/pypi/pypfilt/>`__ and fixed:

.. code-block:: shell

   nox -s publish -- upload -r testpypi .nox-build/pypfilt-X.Y.Z-py3-none-any.whl

Then upload this wheel to PyPI:

.. code-block:: shell

   nox -s publish -- upload .nox-build/pypfilt-X.Y.Z-py3-none-any.whl

Citation snapshots
------------------

Create an archive of the new release, called ``pypfilt-X.Y.Z.zip``:

.. code-block:: shell

   git archive -o pypfilt-X.Y.Z.zip HEAD

Upload the archive to the `University of Melbourne research repository <https://figshare.unimelb.edu.au/>`_ (or an equivalent platform, such as `Zenodo <https://zenodo.org/>`_), and note the allocated DOI.

Also note the HEAD commit hash:

.. code-block:: shell

   git rev-parse HEAD

Update the following lines in ``CITATION.cff``:

.. code-block:: yaml

   version: X.Y.Z
   date-released: 'YYYY-MM-DD'
   commit: <HEAD commit hash>
   # ...
   identifiers:
     - description: 'The archived snapshot of pypfilt version X.Y.Z'
       type: doi
       value: <archive DOI>

Commit and push these changes:

.. code-block:: shell

   git add CITATION.cff
   git commit -m "Update CITATION.cff for pypfilt X.Y.Z"
   git push
