Testing with nox
================

The pypfilt_ testing suite uses the `pytest`_ framework and the `nox`_ automation tool.
The test cases are contained in the ``./tests`` directory, and can be run with:

.. code-block:: shell

   nox

This will also reproduce the images included in the online documentation.

Controlling how tests are run
-----------------------------

You can pass arguments to `pytest`_ by running the ``tests`` session and using ``--`` to separate `nox`_ arguments from `pytest`_ arguments.
For example:

* To run specific test cases whose names contain a specific string:

  .. code-block:: shell

     nox -s tests -- -k PATTERN

* To control the display of logging messages:

  .. code-block:: shell

     nox -s tests -- -o log_cli=true --log-cli-level=INFO

For further details, see the `pytest command-line documentation <https://docs.pytest.org/en/latest/reference/reference.html#command-line-flags>`__ or run:

.. code-block:: shell

   nox -s tests -- --help

The nox configuration
---------------------

The ``noxfile.py`` contents are shown below, and include targets that check whether the documentation in ``./doc`` builds correctly.

.. literalinclude:: ../../noxfile.py
   :language: python

.. _nox: https://nox.thea.codes/
.. _pytest: https://docs.pytest.org/
