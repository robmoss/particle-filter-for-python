.. _how_to:

How-to Guides
=============

**TODO:** provide worked examples of specific tasks.

.. _create_a_model:

Creating your own model
-----------------------

Create a new subclass of :class:`pypfilt.model.Model` and implement the following methods:

* :meth:`~pypfilt.model.Model.field_types`, which defines the structure of the particle state vector;
* :meth:`~pypfilt.model.Model.init`, which initialises the particle state vectors at the start of a simulation; and
* :meth:`~pypfilt.model.Model.update`, which updates the particle state vectors at each time-step.

If you want to allow any elements in the particle state vectors to be smoothed ("regularisation") the model must also implement the following method:

* :meth:`~pypfilt.model.Model.can_smooth`, which identifies the particle state vector elements that can be smoothed by the particle filter.

.. note:: If your model class contains any internal variables that are not stored in the particle state vectors, you should also implement the :meth:`~pypfilt.model.Model.resume_from_cache` method so that these variables can be properly initialised when beginning a simulation from a saved state.

          It is preferable to store all necessary model variables in the particle state vectors and avoid this complication.

For example, shown below is the code for the Gaussian random walk model (:class:`~pypfilt.examples.simple.GaussianWalk`) provided in the :mod:`pypfilt.examples.simple` module:

.. literalinclude:: ../../src/pypfilt/examples/simple.py
   :pyobject: GaussianWalk

Creating your own observation model
-----------------------------------

Inherit from the :class:`pypfilt.obs.Univariate` class to create observation models that are based on Scipy `probability distributions <https://docs.scipy.org/doc/scipy/reference/stats.html>`__.

For example, shown below is the code for the Gaussian random walk observation model provided in the :mod:`pypfilt.examples.simple` module:

.. literalinclude:: ../../src/pypfilt/examples/simple.py
   :pyobject: GaussianObs

.. note:: You can override any of the :class:`pypfilt.obs.Obs` methods.
    This can be useful if you want to support incomplete observations (override :meth:`~pypfilt.obs.Obs.log_llhd`), or need to support custom file formats when loading observations (override :meth:`~pypfilt.obs.Obs.from_file`).

Defining prior distributions
----------------------------

.. _external_prior_samples:

Reading prior samples from external data files
----------------------------------------------

Prior samples can be read from external data files.
This allows you to use arbitrarily-complex model prior distributions, which can incorporate features such as correlations between model parameters.

Samples can be read from **space-delimited text files** using :func:`pypfilt.io.read_table`:

.. code-block:: toml

   [prior]
   x = { external = true, table = "input-file.ssv", column = "x" }

Samples can also be read from **HDF5 datasets**, by specifying the file name, dataset path, and column name:

.. code-block:: toml

   [prior]
   x.external = true
   x.hdf5 = "prior-samples.hdf5"
   x.dataset = "data/prior-samples"
   x.column = "x"

Provide models with lookup tables
---------------------------------

Define partition-specific model parameters
------------------------------------------

There are a number of ways to define model parameters that take different values in each partition of the ensemble.
Here are a few examples:


* Define a unique value for each partition in the ``model`` table:

  .. code-block:: toml

     [model]
     values_alpha = [10, 50, 100]

  The model can use these values to initialise the appropriate field(s) of the state vectors:

  .. code-block:: python

     def init(self, ctx, vec):
         for (ix, partition) in ctx.get_setting(['filter', 'partition'], []):
             part_ixs = partition['slice']
             value = ctx.settings['model']['values_alpha'][ix]
             vec['alpha'][part_ixs] = value

* Define a probability distribution for each partition by defining a separate parameter for each partition in the ``prior`` table:

  .. code-block:: toml

     [prior]
     alpha_1 = { name = "uniform", args.loc = 0, args.scale = 1}
     alpha_2 = { name = "uniform", args.loc = 0, args.scale = 10}

  The model can use these values to initialise the appropriate field(s) of the state vectors:

  .. code-block:: python

     def init(self, ctx, vec):
         for (ix, partition) in ctx.get_setting(['filter', 'partition'], []):
             part_ixs = partition['slice']
             param_name = 'alpha_{}'.format(ix)
             values = ctx.data['prior'][param_name]
             count = len(vec[param_name][part_ixs])
             vec[param_name][part_ixs] = values[:count]

* Create a separate lookup table for each partition, and associate each table with a partition by adding a setting to the ``model`` table:

  .. code-block:: toml

     [model]
     partition_lookup_tables = ['first_partition', 'second_partition']

     [lookup_tables]
     # Define the 'first_partition' and 'second_partition' tables here.

  The model can sample values from each lookup table and assign them to the appropriate subsets of the ensemble:

  .. code-block:: python

     def init(self, ctx, vec):
         start = ctx.get_setting(['time', 'sim_start'])
         for (ix, partition) in ctx.get_setting(['filter', 'partition'], []):
             part_ixs = partition['slice']
             table_name = ctx.settings['model']['partition_lookup_tables'][ix]
             values = ctx.component['lookup'][table_name].lookup(start)
             count = len(vec['parameter'][part_ixs])
             vec['parameter'][part_ixs] = values[:count]

* Construct appropriate prior samples for these parameters that account for the partition sizes and ordering, and read these samples from external files (as described above);

.. _howto_provide_lookup_tables:

Provide observation models with lookup tables
---------------------------------------------

An observation model **may** allow some of its parameters to be defined in
lookup tables, rather than as fixed values.
This allows parameters to differ between particles and to vary over time.

.. note:: The observation model must support using lookup tables for parameter
   values.
   The example shown here uses the ``epifx.obs.PopnCounts`` observation model,
   which allows the observation probability to be defined in a lookup table.

To use a lookup table for an observation model parameter, the scenario must:

* Define the lookup table by giving it a name (in this example, ``"pr_obs"``)
  and identifying the data file to use (in this example, ``"pr-obs.ssv"``);
* Notify pypfilt_ that each particle should be associated with a column from
  this lookup table by setting ``sample_values = true`` (see the example
  below); and
* Notify the observation model that it should use this lookup table for the
  observation probability, by providing it as an argument to the observation
  model's constructor (in this example, the argument name is
  ``"pr_obs_lookup"``).

These steps are illustrated in the following TOML_ excerpt, which shows only
the relevant lines:

.. code:: toml

   [scenario.test.lookup_tables]
   pr_obs = { file = "pr-obs.ssv", sample_values = true }

   [scenario.test.observations.cases]
   model = "epifx.obs.PopnCounts"
   pr_obs_lookup = "pr_obs"

You can then retrieve values from the lookup table, and index them to select the appropriate value for each particle:

.. code:: python

   def get_values(ctx, snapshot, table_name):
       table = ctx.component['lookup'][table_name]
       ixs = snapshot.vec['lookup'][table_name]
       values = table.lookup(snapshot.time)[ixs]

.. _howto_using_lookup_tables:

Using lookup tables in your own models
--------------------------------------

Time-varying inputs can be provided in a lookup table, and retrieved by the simulation model when updating the particle state vectors at each time-step.
The example below shows how to retrieve the current value(s) for a parameter ``alpha`` from the lookup table called "alpha_table".

.. code-block:: python

   def update(self, ctx, step_date, dt, is_fs, prev, curr):
       """Perform a single time-step.""""
       # Retrieve the current value(s) for the parameter alpha.
       alpha = ctx.component['lookup']['alpha_table'].lookup(time)
       # NOTE: now update the particle state vectors.
       pass

This table must be defined in the scenario definition:

.. code-block:: toml

   [lookup_tables]
   alpha_table = "alpha-values.ssv"

.. note:: You may want to avoid hard-coding the table name, and make the table name a model setting.

          The scenario definition would need to include this new setting:

          .. code-block:: toml

             [model]
             alpha_lookup = "some_table_name"

          And the simulation model would use this setting in its ``update()`` method:

          .. code-block:: python

             def update(self, ctx, step_date, dt, is_fs, prev, curr):
                 """Perform a single time-step.""""
                 # Retrieve the current value(s) for the parameter alpha.
                 table_name = ctx.settings['model']['alpha_lookup']
                 alpha = ctx.component['lookup'][table_name].lookup(time)
                 # NOTE: now update the particle state vectors.
                 pass


Using lookup tables in your own observation models
--------------------------------------------------

Defining and using parameters
-----------------------------

Particle filter: resampling
---------------------------

Particle filter: post-regularisation
------------------------------------

Creating summary tables
-----------------------

Resampling before calculating summary statistics
------------------------------------------------

It can be useful to resample the particle ensemble before calculating summary statistics.
For example, the :class:`~pypfilt.summary.SimulatedObs` table resamples the particles each time that it generates simulated observations during the estimation pass.
In contrast, it only resamples the particles at the beginning of the forecasting pass, to ensure that the simulated observations reflect individual particle trajectories.
These resampling steps are performed by using :func:`pypfilt.resample.resample_weights` to return an array of resampled particle indices.

.. note:: This resampling **only affects** the simulated observations, and **does not affect the particle ensemble**.
   As a consequence, it does not perform steps such as post-regularisation.

You can use this same approach in your own summary tables:

* Create a pseudo-random number generator (PRNG) to use with :func:`~pypfilt.resample.resample_weights`;

* Ensure the PRNG state is saved to, and restored from, cache files by implementing the :meth:`~pypfilt.Table.save_state` and :meth:`~pypfilt.Table.load_state` methods;

* Resample the particles at each time during the estimation pass; and

* Resample the particles at the start of the forecasting pass and retain these indices for the entire forecasting pass (e.g., store them in ``self.__samples_ixs`` as shown below).

.. code-block:: python

   import numpy as np
   from pypfilt.cache import load_rng_states, save_rng_states
   from pypfilt.io import time_field
   from pypfilt.resample import resample_weights
   from pypfilt.summary import Table

   class MeanX(Table):
       """Record the mean value of ``x`` at each time unit."""

       def field_types(self, ctx, obs_list, name):
           # Create a PRNG for resampling the particles.
           prng_seed = ctx.settings['filter'].get('prng_seed')
           self.__rnd = np.random.default_rng(prng_seed)
           # Return the field types.
           return [time_field('fs_time'), time_field('time'), ('value', np.float64)]

       def n_rows(self, ctx, start_date, end_date, n_days, forecasting):
           # Reset the sample indices at the start of each simulation.
           self.__sample_ixs = None
           self.__forecasting = forecasting
           # Generate one row at each day.
           return n_days

       def load_state(self, ctx, group):
           """Restore the state of each PRNG from the cache."""
           load_rng_states(group, 'prng_states', {'resample': self.__rnd})

       def save_state(self, ctx, group):
           """Save the current state of each PRNG to the cache."""
           save_rng_states(group, 'prng_states', {'resample': self.__rnd})

       def add_rows(self, ctx, fs_time, window, insert_fn):
           """Record the mean value of ``x`` at each time unit."""
           for snapshot in window:
               if self.__sample_ixs is None:
                   (sample_ixs, _weight) = resample_weights(snapshot.weights, self.__rnd)
                   if self.__forecasting:
                       # Save these indices for use over the entire forecast.
                       self.__sample_ixs = sample_ixs
               else:
                   sample_ixs = self.__sample_ixs

               # Now summarise the snapshot, using `sample_ixs` for indexing.
               x_values = snapshot.state_vec['x'][sample_ixs]
               row = (fs_time, snapshot.time, np.mean(x_values))
               insert_fn(row)

Creating summary monitors
-------------------------

Load summary tables as Pandas data frames
-----------------------------------------

You can load summary tables with :func:`pypfilt.io.load_dataset`,  which returns these tables as NumPy `structured arrays <https://numpy.org/doc/stable/user/basics.rec.html>`__.
This can easily be extended to return summary tables as `Pandas <https://pandas.pydata.org/>`__ data frames:

.. code-block:: python

   import h5py
   import pandas as pd
   import pypfilt

   def load_dataframe(source, dataset, time_scale):
       """
       Load a pypfilt summary table and convert it into a Pandas dataframe.

       :param source: The path to the HDF5 file.
       :param dataset: The HDF5 path to the summary table.
       :param time_scale: The simulation time scale.
       """
       with h5py.File(source, 'r') as f:
           table = pypfilt.io.load_dataset(time_scale, f[dataset])

       return pd.DataFrame(table)

You can then use this function to read summary tables from simulation output files:

.. code-block:: python

   output_file = 'output.hdf5'
   dataset = '/tables/forecasts'
   time_scale = pypfilt.Datetime()
   forecasts = load_dataframe(output_file, dataset, time_scale)

Implement a continuous-time Markov chain (CTMC) model
-----------------------------------------------------

The :class:`pypfilt.examples.sir.SirCtmc` model implementation is shown below.

.. literalinclude:: ../../src/pypfilt/examples/sir.py
   :pyobject: SirCtmc

Implement a discrete-time Markov chain (DTMC) model
---------------------------------------------------

The :class:`pypfilt.examples.sir.SirDtmc` model implementation is shown below.

.. literalinclude:: ../../src/pypfilt/examples/sir.py
   :pyobject: SirDtmc

Implement an ordinary differential equation (ODE) model
-------------------------------------------------------

The :class:`pypfilt.examples.sir.SirOdeEuler` model implementation is shown below.
For simplicity, it uses the forward Euler method.

.. literalinclude:: ../../src/pypfilt/examples/sir.py
   :pyobject: SirOdeEuler

The :class:`pypfilt.examples.sir.SirOdeRk` model implementation is shown below.
It uses a SciPy initial value problem (IVP) solver for ODEs.

.. note:: The :class:`~pypfilt.examples.sir.SirOdeRk` model derives from the :class:`~pypfilt.model.OdeModel` class, and requires fewer time-steps (``time.steps_per_unit``) than the :class:`~pypfilt.examples.sir.SirOde` model, because the ODE solver can divide each time-step into as many smaller steps as required.

.. literalinclude:: ../../src/pypfilt/examples/sir.py
   :pyobject: SirOdeRk

Implement a stochastic differential equation (SDE) model
--------------------------------------------------------

The :class:`pypfilt.examples.sir.SirSde` model implementation is shown below.
It uses the Euler-Maruyama method.

.. literalinclude:: ../../src/pypfilt/examples/sir.py
   :pyobject: SirSde

Implement an observation model that relies on past state
--------------------------------------------------------

The :class:`pypfilt.examples.sir.SirObs` model implementation is shown below.
The expected value is derived from the decrease in susceptible individuals over the observation period :math:`\Delta`.

.. literalinclude:: ../../src/pypfilt/examples/sir.py
   :pyobject: SirObs

.. _performing-ministeps:

Record only a subset of simulation model time-steps
---------------------------------------------------

For simulation models that require very small time-steps, it may be desirable to avoid recording the particle states at each time-step.
This can be achieved by using a large time-step and dividing each time-step into a number of "mini-steps" that will not be recorded, by applying the :func:`pypfilt.model.ministeps` decorator to the simulation model's :meth:`~pypfilt.model.Model.update` method.

This decorator can be used in two ways.

1. Providing a default number of mini-steps, which can be overridden by the scenario settings:

   .. literalinclude:: ../../tests/test_mini_steps.py
      :pyobject: TestModelPredef
      :emphasize-lines: 10

2. Requiring the number of mini-steps to be defined in the simulation settings, by providing no default number of mini-steps:

   .. literalinclude:: ../../tests/test_mini_steps.py
      :pyobject: TestModelSetting
      :emphasize-lines: 10

In both cases, the number of mini-steps can be specified in the scenario definition:

.. code-block:: toml

   [time]
   mini_steps_per_step = 100

.. toctree::
   :hidden:

   self
   scenario-modelling
