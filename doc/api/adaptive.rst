pypfilt.adaptive
================

.. py:module:: pypfilt.adaptive

.. autofunction:: fit_effective_fraction

.. autofunction:: fixed_exponents
