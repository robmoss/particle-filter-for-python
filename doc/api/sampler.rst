pypfilt.sampler
===============

.. py:module:: pypfilt.sampler

The :mod:`pypfilt.sampler` module provides a `Latin hypercube <https://en.wikipedia.org/wiki/Latin_hypercube_sampling>`__ sampler.
This sampler ensures that the samples provide **a good representation of the real variability**, and supports **dependent parameters** whose distributions depend on other parameters.

.. autoclass:: LatinHypercube

To use this sampler, add the following to your scenario files:

.. code-block:: toml

   [components]
   sampler = "pypfilt.sampler.LatinHypercube"

   [sampler]
   dependent_distributions_function = "beta_distribution"

   [prior]
   # Sample alpha uniformly over the interval [5, 10].
   alpha = { name = "uniform", args.loc = 5, args.scale = 5}
   # Identify beta as a dependent parameter.
   beta = { dependent = true }

You then need to define a function that constructs the sampling distribution for each dependent parameter:

.. code-block:: python

   def beta_distribution(indep_values, dep_params):
       """Define beta ~ N(alpha + 1, 1)."""
       return {
           'beta': {
               'name': 'norm',
               'args': {'loc': indep_values['alpha'] + 1, 'scale': 1},
           },
       }

.. note::

   See the `lhs documentation <https://lhs.readthedocs.io/en/latest/>`__ for examples of how to define distributions for independent and dependent parameters.

Defining a custom sampler
-------------------------

A custom sampler should derive the following base class:

.. autoclass:: Base
   :members:
