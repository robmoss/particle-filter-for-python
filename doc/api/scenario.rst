pypfilt.scenario
================

.. py:module:: pypfilt.scenario

The :mod:`pypfilt.scenario` module reads simulation scenarios from plain-text TOML_ inputs.

The purpose of this module is to allow users to define and run simulations **without writing any Python code**, and instead define all of the necessary settings in TOML_ files.

.. note:: A scenario will have a separate :py:class:`Instance` for each combination of observation model parameter values.

Loading scenarios
-----------------

.. autofunction:: load_instances

.. autoclass:: Instance
   :members: build_context, time_scale

Internal types
--------------

.. autoclass:: Specification

.. autoclass:: Scenario

.. autoclass:: ObsModelParams

Internal functions
------------------

.. autofunction:: load_toml

.. autofunction:: load_specifications

.. autofunction:: scenarios

.. autofunction:: instances

.. autofunction:: observation_model_parameter_combinations

.. autofunction:: scenario_observation_model_combinations

.. autofunction:: scenario_observation_model_parameters

.. autofunction:: override_dict

.. autoclass:: DELETE_KEY

.. autofunction:: as_list
