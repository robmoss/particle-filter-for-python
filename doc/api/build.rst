pypfilt.build
=============

.. py:module:: pypfilt.build

The :mod:`pypfilt.build` module builds simulation contexts for simulation instances, in preparation for running estimation and forecasting simulations.

Building simulation contexts
----------------------------

.. autofunction:: build_context

.. autoclass:: Context
   :members: get_setting, override_settings, install_event_handler, call_event_handlers, prior_table, external_prior_samples, particle_count, start_time, end_time, time_steps, time_units, summary_times, summary_count, create_prng

Internal functions
------------------

.. autofunction:: get_chained

.. autofunction:: set_chained

.. autofunction:: set_chained_default
