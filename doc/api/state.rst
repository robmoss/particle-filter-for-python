pypfilt.state
=============

.. py:module:: pypfilt.state

.. autoclass:: History
   :members: set_time_step, reached_window_end, shift_window_back, set_resampled, snapshot, summary_window, create_backcast

.. autoclass:: Snapshot
   :members:

.. autofunction:: history_matrix

.. autofunction:: earlier_states

.. autofunction:: is_history_matrix

.. autofunction:: require_history_matrix

.. autofunction:: is_state_vec_matrix

.. autofunction:: require_state_vec_matrix

.. autofunction:: repack
