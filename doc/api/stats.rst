pypfilt.stats
=============

.. py:module:: pypfilt.stats

Note that there are `9 common definitions of sample quantiles <https://blogs.sas.com/content/iml/2017/05/24/definitions-sample-quantiles.html>`_.
Weighted versions of these 9 definitions are presented in
`this article <https://aakinshin.net/posts/weighted-quantiles/>`_.
The :func:`qtl_wt` function currently implements **type 2** weighted quantiles.
However, the differences between these definitions is typically small when there are many values (i.e., particles).

.. autofunction:: cov_wt

.. autofunction:: avg_var_wt

.. autofunction:: qtl_wt

.. autofunction:: cred_wt
