pypfilt.event
=============

.. py:module:: pypfilt.event

This module defines a data structure for each :ref:`particle filter event <concept_events>`.

.. autoclass:: LogLikelihood

.. autoclass:: AfterReweight

.. autoclass:: BeforeResample

.. autoclass:: BeforeRegularisation

.. autoclass:: AfterRegularisation

.. autoclass:: AfterResample

.. autoclass:: AfterTimeStep
