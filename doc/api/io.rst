pypfilt.io
==========

.. py:module:: pypfilt.io

The :mod:`pypfilt.io` module provides functions for reading tabular data from
data files.

Reading tabular data
--------------------

Use :func:`~pypfilt.io.read_fields` to read tabular data (such as observations) from plain-text files.

.. autofunction:: pypfilt.io.read_fields

.. autofunction:: pypfilt.io.string_field

.. autofunction:: pypfilt.io.time_field

.. autofunction:: pypfilt.io.read_table

.. autofunction:: pypfilt.io.date_column

.. autofunction:: pypfilt.io.datetime_column

Writing tabular data
--------------------

Use :func:`write_table` to save summary tables to plain-text files.

.. autofunction:: write_table

Lookup tables
-------------

The :mod:`pypfilt.io` module also provides lookup tables, which are used to
retrieve time-indexed values (e.g., time-varying model inputs).

.. note:: Define one or more lookup tables in the scenario definition (see :ref:`Lookup table settings <settings_lookup>`).
   You can then retrieve the corresponding :class:`Lookup` component from the simulation context:

   .. code-block:: python

      table_name = 'some_name'
      lookup_time = 1.0
      table = context.component['lookup'][table_name]
      values = table.lookup(lookup_time)

.. autoclass:: Lookup

.. autofunction:: pypfilt.io.read_lookup_table

.. autofunction:: pypfilt.io.lookup_values_count

.. autofunction:: pypfilt.io.lookup_times

.. autofunction:: pypfilt.io.lookup

Summary tables
--------------

The :mod:`pypfilt.io` module provides convenience functions for preserving time values when saving and loading summary tables.

.. note::

   When implementing a summary table, simply ensure that the :func:`~pypfilt.summary.Table.field_types` method uses :func:`pypfilt.io.time_field` to identify each field that will contain time values, and :func:`pypfilt.io.string_field` to identify each field that will contain string values.

   You can then use :func:`load_summary_table` and :func:`pypfilt.io.load_dataset` to retrieve saved tables and ensure that all time values are in the expected format.

.. autofunction:: load_summary_table

.. autofunction:: load_dataset

The :class:`pypfilt.summary.HDF5` class ensures that all summary tables are constructed with the appropriate data type and are saved with the necessary metadata, so you should never need to use the following functions.

.. autofunction:: fields_dtype

.. autofunction:: save_dataset

.. _scalar: https://numpy.org/doc/stable/reference/arrays.scalars.html

.. _numpy.genfromtxt: https://numpy.org/doc/stable/reference/generated/numpy.genfromtxt.html
