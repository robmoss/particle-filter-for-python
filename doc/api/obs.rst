pypfilt.obs
===========

.. module:: pypfilt.obs

While all observations models should derive from the :class:`Obs` base class, the :class:`Univariate` class provides a simple way to define observation models in terms of Scipy `probability distributions <https://docs.scipy.org/doc/scipy/reference/stats.html>`__.

.. autoclass:: pypfilt.obs.Obs
   :members:

.. autoclass:: pypfilt.obs.Univariate
   :members: distribution, quantiles_tolerance, from_file, row_into_obs, obs_into_row, simulated_field_types, simulated_obs

.. autofunction:: pypfilt.obs.expect

.. autofunction:: pypfilt.obs.log_llhd

.. autofunction:: pypfilt.obs.bisect_cdf

.. autofunction:: pypfilt.obs.simulate
