pypfilt.crps
============

.. py:module:: pypfilt.crps

This module provides functions for calculating `Continuous Ranked Probability Scores <https://otexts.com/fpp3/distaccuracy.html>`__.
CRPS is a measure of how well a forecast distribution predicts the true values.

You can evaluate forecast performance against future observations by:

1. Simulating observations for each particle, using the :class:`~pypfilt.summary.SimulatedObs` table;

2. Loading the simulated observations with :func:`~pypfilt.io.load_dataset`;

3. Loading the future observations (once available) with the observation model's :meth:`~pypfilt.obs.Obs.from_file` method; and

4. Calculating the CRPS scores with :func:`simulated_obs_crps`.

.. autofunction:: simulated_obs_crps

.. autofunction:: crps_edf_scalar

.. autofunction:: crps_sample
