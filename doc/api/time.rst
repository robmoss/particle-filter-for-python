pypfilt.time
============

.. py:module:: pypfilt.time

Two pre-defined simulation time scales are provided.

.. autoclass:: pypfilt.time.Scalar
   :no-members:
   :members: __init__, set_period, with_observation_tables,
      time_step_of_next_observation

.. autoclass:: pypfilt.time.Datetime
   :no-members:
   :members: __init__, set_period, with_observation_tables,
      time_step_of_next_observation, custom_format_strings

.. autoclass:: TimeStep

.. autoclass:: TimeStepWithObservations

Custom time scales
------------------

If neither of the above time scales is suitable, you can define a custom time
scale, which should derive the following base class and define the methods
listed here:

.. autoclass:: pypfilt.time.Time
   :no-members:
   :members: dtype, native_dtype, is_instance, to_dtype, from_dtype,
             to_unicode, from_unicode, column, steps, step_count, step_of,
             add_scalar, time_of_obs
