Particle filters for Python
===========================

Welcome to the pypfilt_ documentation.
This package implements several particle filter methods that can be used for recursive Bayesian estimation and forecasting.

.. admonition:: December 2024
   :class: tip

   pypfilt_ was a joint winner of the `2024 Venables Award <https://statsocaus.github.io/venables-award/>`__ for new developers of open source software for data analytics (sponsored by the `ARDC <http://ardc.edu.au/>`__)!
   The other winner was `whereabouts <https://whereabouts.readthedocs.io/en/latest/>`__.

If there is a system or process that can be:

* Described (modelled) with mathematical equations; and
* Measured repeatedly in some (noisy) way.

Then you can use pypfilt_ to estimate the state and/or parameters of this system, and generate probabilistic forecasts.

This package also supports other workflows for simulation models, such as :ref:`scenario modelling <scenario_modelling>`.

.. figure:: getting-started/lorenz63_forecast_regularised.png
   :width: 100%
   :name: example forecasts

   Forecasts for :math:`x(t)`, :math:`y(t)`, and :math:`z(t)` of the :class:`~pypfilt.examples.lorenz.Lorenz63` system at time :math:`t=20`.

Contents
--------

Depending on your preferred learning style, start with any of the following:

The :doc:`getting-started/index` tutorial
   Shows how to build a simulation model, generate simulated observations from this model, fit the model to these observations, and generate the `example forecasts`_, above.

The :doc:`concepts/index` guide
   Introduces the various pypfilt_ components and how they fit together.

   .. note:: This guide is currently incomplete.

The :doc:`how-to/index`
   Demonstrates how to solve particular problems.

   .. note:: These guides are currently incomplete.

The :doc:`settings/index` guide
   Explains the settings that can be defined in a scenario file.

The :doc:`api/index`
   Presents the details of each pypfilt_ component and is likely only of interest if you need to develop your own components.

The :doc:`development/index` guide
   Explains how pypfilt_ is developed and how to contribute to this project.

License
-------

The code is distributed under the terms of the BSD 3-Clause license (see ``LICENSE``), and the documentation is distributed under the terms of the `Creative Commons BY-SA 4.0 license <http://creativecommons.org/licenses/by-sa/4.0/>`_.

Citation
--------

If you use ``pypfilt``, please cite our `JOSS article <https://doi.org/10.21105/joss.06276>`_ and an archived release of the software (see ``CITATION.cff``).

.. code-block:: bibtex

   @article{pypfilt,
     author = {Moss, Robert},
     title = {pypfilt: a particle filter for {Python}},
     journal = {Journal of Open Source Software},
     volume = {9},
     issue = {96},
     pages = {6276},
     year = {2024},
     doi = {10.21105/joss.06276},
     note = {Please cite this article and an archived release (see CITATION.cff)},
   }

.. toctree::
   :maxdepth: 2
   :hidden:

   getting-started/index

.. toctree::
   :maxdepth: 2
   :hidden:

   concepts/index

.. toctree::
   :maxdepth: 2
   :hidden:

   how-to/index

.. toctree::
   :maxdepth: 2
   :hidden:

   settings/index

.. toctree::
   :maxdepth: 2
   :hidden:

   api/index

.. toctree::
   :maxdepth: 2
   :hidden:

   development/index
