.. _gs-conclusion:

Conclusion
==========

You've seen how to define simulation models, observation models, and simulation scenarios, and how to use these scenarios to simulate observations and generate forecasts.

You can now try modifying the observation files, adjusting the prior distributions, etc, and see how this affects the forecast predictions.
Save the :func:`example scenarios <pypfilt.examples.lorenz.lorenz63_all_scenarios_toml>` and try modifying the included scenarios, or adding your own scenarios.

To learn how to use pypfilt_ with your own models and data, see :ref:`concepts` and the :ref:`how_to`.
