.. _lorenz63-reg:

Regularisation
==============

We can maintain particle diversity with the **post-regularised particle filter**, where resampling draws particles from a continuous distribution, rather than directly from the particle ensemble.
The continuous distribution is constructed using a multi-variate Gaussian kernel, whose covariance matrix is the normalised particle covariance.
This means that when the particles are resampled, we will obtain many particles that are **similar, but not identical** to the particles with the largest weights (i.e., those that best describe the past observations).

As demonstrated in the figure below, the post-regularised particle filter greatly improves the model fit and forecasts.
However, due to the chaotic nature of the system, the forecasts for :math:`x(t)` and :math:`y(t)` become quite uncertain by :math:`t=25`.

.. figure:: lorenz63_forecast_regularised.png
   :width: 100%

   Forecasts for :math:`x(t)`, :math:`y(t)`, and :math:`z(t)` of the :class:`~pypfilt.examples.lorenz.Lorenz63` system at time :math:`t=20`, using the post-regularisation particle filter.

In order to use the post-regularised particle filter, we need to:

#. Identify which fields in the particle state vector can be smoothed by the filter.
   This is defined by the simulation model's :meth:`~pypfilt.model.Model.can_smooth` method.

#. Enable regularisation by setting ``filter.regularisation.enabled = true`` in the scenario definition.

#. Define the lower and upper bounds for each field that should be smoothed (``filter.regularisation.bounds.FIELD``) in the scenario definition.

.. note:: The post-regularised particle filter will only smooth fields that are returned by the :meth:`~pypfilt.model.Model.can_smooth` method **and** for which lower and/or upper bounds have been defined.

   You can omit the lower or upper bounds, if they are not appropriate.
   To smooth a field without imposing any bounds, define an empty table for that field.
   See the bounds for ``z`` in the scenario definition below.

.. literalinclude:: ../../src/pypfilt/examples/lorenz.py
   :pyobject: Lorenz63
   :lines: 1,16-
   :emphasize-lines: 30-32
   :caption: The :class:`~pypfilt.examples.lorenz.Lorenz63` simulation model allows all state vector fields to be smoothed (**see highlighted lines**).

.. literalinclude:: lorenz63_forecast_regularised.toml
   :language: toml
   :linenos:
   :emphasize-lines: 46, 48-51
   :name: lorenz63-forecast-regularised-toml
   :caption: An example scenario for the Lorenz-63 system that uses the post-regularisation particle filter (**see highlighted lines**).

.. note:: Call :func:`~pypfilt.examples.lorenz.save_lorenz63_scenario_files` to save this scenario file (and the others used in this tutorial) in the working directory.

.. literalinclude:: ../../tests/test_lorenz.py
   :pyobject: run_lorenz63_forecast_regularised
