.. _lorenz63-crps:

Forecast performance
====================

It was visually evident from the previous figures that the post-regularised particle filter produced much better forecasts than the bootstrap particle filter.

However, if there wasn't such a clear difference between the forecasts, or if we were interested in evaluating forecast performance over a large number of forecasts, visually inspecting the results would not be a suitable approach.

Instead, we can use a **proper scoring rule** such as the `Continuous Ranked Probability Score <https://otexts.com/fpp3/distaccuracy.html>`__ to evaluate each forecast distribution against the true observations.

We can then measure how much post-regularisation improves the forecast performance by calculating a CRPS skill score **relative to the original forecast**:

.. math::

   \mathrm{Skill} = \frac{\operatorname{CRPS}_{\mathrm{Original}} - \operatorname{CRPS}_{\mathrm{Regularised}}}{\operatorname{CRPS}_{\mathrm{Original}}}

.. note::

   Depending on the nature of the data and your model, it may be useful to **transform the data** before calculating CRPS values (e.g., computing scores on the log scale).
   See `Scoring epidemiological forecasts on transformed scales (Bosse et al., 2023) <https://doi.org/10.1371/journal.pcbi.1011393>`__ for further details.

Shown below are the CPRS values for each :math:`z(t)` forecast.
As displayed in the figure legend, the forecast with post-regularisation is **76.7% better** than the original forecast.

.. figure:: lorenz63_crps_comparison.png
   :width: 100%

   Comparison of CRPS values for the original :math:`z(t)` forecasts, and for the :math:`z(t)` forecasts with regularisation.

We can calculate CPRS values by taking the following steps:

1. Record simulated :math:`z(t)` observations for each particle with the :class:`~pypfilt.summary.SimulatedObs` summary table;

2. Save the forecast results to HDF5 files;

3. Load the simulated :math:`z(t)` observations with :func:`~pypfilt.io.load_summary_table`;

4. Load the true future :math:`z(t)` observations with :func:`~pypfilt.io.read_table`; and

5. Calculate CRPS values with :func:`~pypfilt.crps.simulated_obs_crps`.

.. literalinclude:: ../../tests/test_lorenz.py
   :pyobject: score_lorenz63_forecasts
