.. _lorenz63-scenario:

Defining a scenario
===================

Scenarios are defined in TOML_ files, as illustrated in the example below.

#. The key components are defined in the ``[components]`` table:

   * The simulation model (``model``);
   * The time scale (``time``, either :class:`scalar time <pypfilt.time.Scalar>` or :class:`date-time <pypfilt.time.Datetime>`);
   * The prior distribution sampler (``sampler``, see :mod:`pypfilt.sampler`); and
   * The output recorder (``summary``).

#. The simulation period and time-step settings are defined in the ``[time]`` table.

#. The model prior distribution is defined in the ``[prior]`` table.
   Here, we have defined all the parameters and state variables to have **fixed values**, so that we can simulate observations from a known ground truth.

#. Each observation model is defined in an ``[observations.UNIT]`` table, where ``UNIT`` is the **observation unit** (see :ref:`lorenz63-obs` for details).
   Here, we have defined three observations models, with observation units ``'x'``, ``'y'``, and ``'z'``.

#. Particle filter settings are defined in the ``[filter]`` table, including:

   * The number of particles (``particles``); and
   * The seed for the pseudo-random number generator (``prng_seed``).

#. Each scenario is defined in a ``[scenario.ID]`` table, where ``ID`` is a unique identifier for the scenario.
   In :ref:`lorenz63-multi` we will see how to define multiple scenarios and scenario-specific settings.

.. literalinclude:: lorenz63_simulate.toml
   :language: toml
   :linenos:
   :emphasize-lines: 23, 26, 29, 38
   :name: lorenz63-simulate-toml
   :caption: An example scenario for simulating observations from the Lorenz-63 system.

.. note:: Call :func:`~pypfilt.examples.lorenz.save_lorenz63_scenario_files` to save this scenario file (and the others used in this tutorial) in the working directory.

Once we have defined one or more scenarios in a TOML_ file, we can iterate over the scenario instances with :func:`pypfilt.load_instances`:

.. literalinclude:: ../../tests/test_lorenz.py
   :pyobject: check_lorenz63_instances
