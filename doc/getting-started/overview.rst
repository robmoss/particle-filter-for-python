.. _gs-overview:

Overview
========

In order to use pypfilt_ you will need to define the following things:

* A **simulation model** that describes the behaviour of the system;

  * See :ref:`create_a_model` for an example of defining a simulation model.

* **Prior distribution(s)** and parameter bounds;

  * Specify **independent priors** for each parameter (see the :class:`~pypfilt.sampler.LatinHypercube` sampler, which supports all of the `scipy.stats`_ distributions and `several other distributions <https://lhs.readthedocs.io/en/latest/distributions.html>`__); and/or
  * Read **correlated priors** for multiple parameters from an external file (see :ref:`external_prior_samples`).

* Some **observations** of the system (either real or simulated from the
  simulation model);
* An **observation model** that describes how the simulation model and the
  observations are related;

  * Use the :class:`~pypfilt.obs.Univariate` base class to define observation models in terms of any univariate `scipy.stats`_ distribution.

* :ref:`Simulation and particle filter settings <settings>`, such as:

  * Choose a time scale (either :class:`~pypfilt.time.Scalar` or :class:`~pypfilt.time.Datetime`);
  * Particle filter settings, such as the resampling threshold and whether to
    use post-regularisation;
  * Select appropriate summary tables to record things of interest; and
  * A seed for the pseudo-random number generator, so that the results are
    reproducible.

All of this information is collected into a single TOML_ file, which pypfilt_
can then use to generate forecasts.

.. note:: TOML_ is a simple, easy to read configuration file format, similar
   to JSON and YAML.
