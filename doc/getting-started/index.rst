.. _getting_started:

Getting Started
===============

This guide assumes that you have already installed pypfilt_:

.. code-block:: shell

   # Install pypfilt with plotting support.
   pip install pypfilt[plot]

See the :ref:`installation instructions <install>` for further details.

This guide shows how to build a simulation model, generate simulated observations from this model, fit the model to these observations, and generate the forecasts shown below.

.. figure:: lorenz63_forecast_regularised.png
   :width: 100%

   Forecasts for :math:`x(t)`, :math:`y(t)`, and :math:`z(t)` of the :class:`~pypfilt.examples.lorenz.Lorenz63` system at time :math:`t=20`.

.. toctree::
   :hidden:

   self
   install
   overview
   system
   model
   observations
   scenario
   simulating
   forecasts
   plotting
   regularisation
   scoring
   multiple
   caching
   conclusion
