.. _lorenz63-plot:

Plotting the results
====================

The :mod:`pypfilt.plot` module provides functions for plotting observations
and credible intervals, and classes for constructing figures with sub-plots.
For example, we can use the :class:`~pypfilt.plot.Wrap` class to plot the simulation model fits and forecasts against the simulated observations for :math:`x(t)`, :math:`y(t)`, and :math:`z(t)`:

.. figure:: lorenz63_forecast.png
   :width: 100%

   Forecasts for :math:`x(t)`, :math:`y(t)`, and :math:`z(t)` of the :class:`~pypfilt.examples.lorenz.Lorenz63` system at time :math:`t=20`.

.. note::

   You may prefer to use a dedicated plotting package such as `plotnine <https://plotnine.readthedocs.io/>`__.

We can see that between :math:`t=15` and :math:`t=20`, the simulation model is no longer able to fit the simulated observations (black points), and the forecasts do not characterise the future observations (hollow points).
This is due to **particle degeneracy**, a situation where very few (if any) particles are consistent with the available observations.

In the next section we will see one way to mitigate this issue.

.. note::
   Whenever the particles are resampled, the particles with the largest weights (i.e., those that best describe the past observations) will be **duplicated**.
   Because the simulation model is **deterministic**, these duplicate particles will produce identical trajectories, and so the particle ensemble will comprise fewer and fewer unique trajectories, representing a smaller and smaller subset of the original samples drawn from the model prior distribution.

.. literalinclude:: ../../tests/test_lorenz.py
   :pyobject: plot_lorenz63_forecast
