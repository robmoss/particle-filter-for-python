.. _lorenz63-eqns:

The Lorenz-63 system
====================

The `Lorenz-63 system <https://en.wikipedia.org/wiki/Lorenz_system>`__ is a system of ordinary differential equations that has chaotic solutions for some parameter values and initial states.

.. math::

   \frac{dx}{dt} &= \sigma (y - x) \\
   \frac{dy}{dt} &= x (\rho - z) - y \\
   \frac{dz}{dt} &= xy - \beta z \\

In this guide, we will focus on the system dynamics obtained with the following parameter values:

.. math::

   \sigma &= 10 \\
   \rho &= 28 \\
   \beta &= \frac{8}{3}
