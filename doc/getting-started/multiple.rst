.. _lorenz63-multi:

Defining multiple scenarios
===========================

We can define multiple scenarios in a single TOML_ file.
For example, shown below is a single TOML_ file that defines all of the scenarios presented in this :ref:`getting_started` guide.
The settings that are shared by all three scenarios are defined as per usual, and the scenario-specific settings are defined inside each scenario's table (indicated by the highlighted lines).

.. literalinclude:: lorenz63_all.toml
   :language: toml
   :linenos:
   :emphasize-lines: 36-
   :name: lorenz63-all-toml
   :caption: An example of defining multiple scenarios in a single file, where common settings are shared between all scenarios, and scenario-specific settings are defined inside each scenario table.

.. note:: Call :func:`~pypfilt.examples.lorenz.save_lorenz63_scenario_files` to save this scenario file (and the others used in this tutorial) in the working directory.

We can load all of these scenarios with :func:`pypfilt.load_instances` and use them to simulate observations and generate forecasts:

.. literalinclude:: ../../tests/test_lorenz.py
   :pyobject: run_all_lorenz63_scenarios
