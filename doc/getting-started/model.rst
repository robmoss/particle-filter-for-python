.. _lorenz63-model:

The simulation model
====================

To construct a Lorenz-63 simulation model for pypfilt_, we need to create a :class:`~pypfilt.model.Model` subclass that defines the state vector structure and the state update rule.
Because this model is a system of ordinary differential equations, we can derive from the :class:`~pypfilt.model.OdeModel` class, which provides a convenient wrapper around `scipy.integrate.solve_ivp() <https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.solve_ivp.html>`__.

Here, we define the state vector to contain six floating-point values:

.. math::

   x_t = [\sigma, \rho, \beta, x, y, z]^T

* The **state vector structure** is defined by the :meth:`~pypfilt.model.Model.field_types` method, which returns a list of ``(name, type)`` pairs.

* The **right-hand side** is defined by the :meth:`~pypfilt.model.OdeModel.d_dt` method, which keeps the parameters :math:`\sigma`, :math:`\rho`, and :math:`\beta` fixed, and calculates the rate of change for :math:`x(t)`, :math:`y(t)`, and :math:`z(t)`.

.. literalinclude:: ../../src/pypfilt/examples/lorenz.py
   :pyobject: Lorenz63
   :lines: 1, 16-42
