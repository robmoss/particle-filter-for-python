.. _lorenz63-fs:

Running forecasts
=================

In order to fit the simulation model to observations and generate forecasts, we need to:

#. Choose prior distributions for :math:`x(t)`, :math:`y(t)`, and :math:`z(t)`;

#. Define an input file for each observation model; and

#. Record summary statistics such as :class:`predictive credible intervals <pypfilt.summary.PredictiveCIs>` for each observation model, and :class:`simulated observations <pypfilt.summary.SimulatedObs>` for :math:`z(t)`.

These changes are indicated by the highlighted lines in the following scenario definition:

.. literalinclude:: lorenz63_forecast.toml
   :language: toml
   :linenos:
   :emphasize-lines: 19-21, 25, 29, 33, 35-39
   :name: lorenz63-forecast-toml
   :caption: An example scenario for generating forecasts for the Lorenz-63 system.

.. note:: Call :func:`~pypfilt.examples.lorenz.save_lorenz63_scenario_files` to save this scenario file (and the others used in this tutorial) in the working directory.

Observations will be read from these files when generating forecasts with :func:`pypfilt.forecast`:

.. literalinclude:: ../../tests/test_lorenz.py
   :pyobject: run_lorenz63_forecast

If you pass a filename to :func:`pypfilt.forecast`, all of the summary tables for the estimation pass and each forecasting pass will be saved to that file, as HDF5 data sets.

.. note:: `HDF5 <http://hdfgroup.org/>`__ is a file format that allows you to
   store lots of data tables and related metadata in a single file, and to
   load these data tables as if they were NumPy arrays.
   All of the summary tables recorded by pypfilt_ are NumPy
   `structured arrays <https://numpy.org/doc/stable/user/basics.rec.html>`__.
   You can explore HDF5 files with the `h5py <https://www.h5py.org/>`__
   package, which makes it easy to load and store data tables.
