.. _lorenz63-sim:

Simulating observations
=======================

Once a scenario has been defined, we can simulate observations from each observation model with :func:`pypfilt.simulate_from_model`:

.. literalinclude:: ../../tests/test_lorenz.py
   :pyobject: simulate_lorenz63_observations

.. note:: Recall that for :ref:`this scenario <lorenz63-scenario>` we specified **fixed values** for each parameter and state variable, so that we can simulate observations from a known ground truth.

.. hlist::
   :columns: 2

   - .. literalinclude:: lorenz63-x.ssv
        :name: lorenz63-obs-x
        :lines: 1-4
        :caption: Example simulated observations for :math:`x(t)`.

   - .. literalinclude:: lorenz63-y.ssv
        :name: lorenz63-obs-y
        :lines: 1-4
        :caption: Example simulated observations for :math:`y(t)`.

We can now use these simulated observations to fit the simulation model and generate forecasts for the future observations.
