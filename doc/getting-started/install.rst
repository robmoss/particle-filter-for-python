.. _install:

Installation
============

You must have Python 3.8, or later, installed.
On Windows, the simplest option is to use `Anaconda <https://docs.continuum.io/anaconda/>`__.
By default, it will install all of the required packages except `tomli <https://pypi.org/project/tomli/>`__ and `tomli-w <https://pypi.org/project/tomli-w/>`__.

You can install pypfilt_ with `pip <https://pip.pypa.io/>`__.
This is best done in a `virtual environment <http://docs.python-guide.org/en/latest/dev/virtualenvs/>`__.
It will also install any required packages that are not currently installed.

Install pypfilt without plotting support:
   .. code-block:: shell

      pip install pypfilt

Install pypfilt with plotting support:
   .. code-block:: shell

      pip install pypfilt[plot]
